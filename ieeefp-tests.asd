(asdf:defsystem ieeefp-tests
    :depends-on (rt split-sequence)
    :components ((:file "package")
		 (:module "ieee754"
		  :depends-on ("package")
		  :components
		  (
		   (:file "ieee754-sbcl"
		    :if-feature :sbcl)
		   (:file "ieee754-cmucl"
		    :if-feature :cmucl)
		   (:file "ieee754-abcl"
		    :if-feature :abcl)))
		 (:file "ieeefp-tests" :depends-on ("ieee754"))))

(cl:defmethod asdf:perform ((o asdf:test-op)
			    (c (cl:eql (asdf:find-system :ieeefp-tests))))
  (cl:mapcar (cl:lambda (x) 
	       (cl:format cl:*trace-output* "; loading ~S~%" (namestring x))
	       (cl:load x :verbose nil))
	     (cl:reverse 
	      (cl:symbol-value (cl:intern "*TEST-FILES*"
					  (cl:find-package "IEEEFP-TESTS")))))
  (cl:funcall (cl:intern "DO-TESTS" (cl:find-package "RT"))))
