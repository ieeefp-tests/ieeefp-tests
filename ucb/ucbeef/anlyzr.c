/*
 * Copyright 1/5/95 Zhishun Alex Liu.  All Rights Reserved.
 *
 ***************************
 * WORK IN PROGRESS
 ***************************
 *
 * Written by Zhishun Alex Liu under direction of Professor William Kahan.
 * The author's current electronic mail address as of December 1994 is
 * Alex.Liu@Eng.Sun.COM.
 *
 * Use of this software is granted with the understanding that all recipients
 * should regard themselves as participants in an ongoing research project and
 * hence should feel obligated to report their experiences (good or bad) with
 * this elementary functions test suite to the author.
 */
#ifndef lint
static char sccsid[] = "@(#)anlyzr.c	1.1 (BeEF) 1/5/95";
#endif
/*
 * INPUTs:
 *	fid: an integer which identifies the function under test
 *	rid: an integer which identifies the region under test
 *	ndiv: an integer indicating the number of subregions over [bgn, end)
 *	narg: number of random arguments per subregion desired
 *	bgn: starting value of the region
 *	end: ending value of the region
 *
 * DESCRIPTION:
 * anlyzr(.) subdivides [bgn, end) into "ndiv" subregions and for each
 * subregion [s.l, s.r) it invokes grand(.) "narg-1" times and collects
 * statistics then sends them to report(.) for printing.
 *
 * extern GENERIC grand(.);
 * extern void nearby(.), report(.), er_dfl(.), er_log(.);
 */
#include "sys.h"

static struct summary s;

void
#ifdef __STDC__
anlyzr(int fid, int rid, int ndiv, int narg, GENERIC *_bgn, GENERIC *_end)
#else
anlyzr(fid, rid, ndiv, narg, _bgn, _end)
int fid, rid, ndiv, narg;
GENERIC *_bgn, *_end;			/* input arguments */
#endif
{
	int i, j;
	GENERIC	bgn = *_bgn, end = *_end, width, xs[3];
#ifdef __STDC__
	void (*rtp)(int, int, GENERIC *, struct summary *);
#else
	void (*rtp)( );
#endif

#if TEST_LOG
	if (fid == CASE_LOG)
		rtp = er_log;
	else
#endif
		rtp = er_dfl;
	width = (end - bgn) / (GENERIC) ndiv;	/* compute width */
	s.l = bgn;				/* initialize s.l */
	s.r = s.l + width;			/* initialize s.r */
	for (i = 1; i <= ndiv; i++) {	/* loop thru "ndiv" subregions... */
		s.scnt = s.mcnt = s.nn = s.np = s.nm = 0;
		s.en = s.ep = ZERO;
		xs[1] = s.xn = s.xp = s.l;
		nearby(xs);
		(*rtp)(fid, rid, xs, &s);	/* error info for left end */
		for (j = 1; j < narg; j++) {	/* narg-1 of them */
			xs[1] = s.l + width * grand( );
			nearby(xs);
			(*rtp)(fid, rid, xs, &s);
		}
		report(i, &s);
		s.l = s.r;
		s.r += width;
	}
}
