/*
 * Copyright 1/5/95 Zhishun Alex Liu.  All Rights Reserved.
 *
 ***************************
 * WORK IN PROGRESS
 ***************************
 *
 * Written by Zhishun Alex Liu under direction of Professor William Kahan.
 * The author's current electronic mail address as of December 1994 is
 * Alex.Liu@Eng.Sun.COM.
 *
 * Use of this software is granted with the understanding that all recipients
 * should regard themselves as participants in an ongoing research project and
 * hence should feel obligated to report their experiences (good or bad) with
 * this elementary functions test suite to the author.
 *
 * static char Sccsid[] = "@(#)sys.h	1.1 (BeEF) 1/5/95";
 */
#ifdef __STDC__
#define	PROTO(p)	p
#else
#define	PROTO(p)	()
#endif

#include "local.h"

#define BITS_2M	(1 << (BITS_M))		/* BITS_M defined in "local.h" */

#undef FLOAT
#define	FLOAT	float
#ifndef	NLINES
#define	NLINES	16
#endif
#ifndef RANDMAX
#define	RANDMAX	2147483647L		/* 2^31-1 */
#endif

#undef ZERO
#undef ONE
#undef TWO
#undef FOUR

#define	ZERO	((GENERIC) 0)
#define	ONE	((GENERIC) 1)
#define	TWO	((GENERIC) 2)
#define	THREE	((GENERIC) 3)
#define	FOUR	((GENERIC) 4)
#define FIVE	((GENERIC) 5)
#define	SIX	((GENERIC) 6)
#define NINE	((GENERIC) 9)
#define	HALF	((GENERIC) 0.5)

#define	GABS(x)	((x) >= ZERO ? (x) : -(x))

#define	CASE_SIN	1
#define	CASE_COS	2
#define	CASE_ATN	3
#define	CASE_EXP	4
#define	CASE_EM1	5
#define	CASE_LOG	6
#define	CASE_L1P	7

typedef struct rational {
	int p, q;
} RATIONAL;

typedef struct summary {
	GENERIC l,			/* left end of a subregion */
		r,			/* right end of the subregion */
		en,			/* maximum negative error */
		ep,			/* maximum positive error */
		xn,			/* location of en in the subregion */
		xp,			/* location of ep in the subregion */
		xm;			/* left end of monotonicity failure */
	int	mcnt,			/* monotonicity failure count */
		scnt,			/* symmetry failure cnt sin/cos/atan */
		nn,			/* LOG: power of 2 of true xn */
		np,			/* LOG: power of 2 of true xp */
		nm;			/* LOG: power of 2 of true xm */
} SUMMARY;

extern int distd PROTO((int, GENERIC *));
extern void anlyzr PROTO((int, int, int, int, GENERIC *, GENERIC *));
extern void dbc_atn PROTO((void));
extern void bgn_atn PROTO((void));
extern GENERIC cf_atn PROTO((GENERIC *));
extern GENERIC er_atn PROTO((int, GENERIC *, GENERIC *));
extern void er_dfl PROTO((int, int, GENERIC *, struct summary *));
extern void dbc_em1 PROTO((void));
extern void bgn_em1 PROTO((void));
extern void bgn_exp PROTO((void));
extern GENERIC er_em1 PROTO((int, GENERIC *, GENERIC *));
extern GENERIC er_exp PROTO((int, GENERIC *, GENERIC *));
extern void cf_tan2 PROTO((GENERIC *, GENERIC *, GENERIC *));
extern void dbc_l1p PROTO((void));
extern GENERIC er_l1p PROTO((int, GENERIC *, GENERIC *));
extern void dbc_log PROTO((void));
extern void bgn_log PROTO((void));
extern void er_log PROTO((int, int, GENERIC *, struct summary *));
extern void dbc_sin PROTO((void));
extern void bgn_sin PROTO((void));
extern GENERIC er_sin PROTO((int, GENERIC *, GENERIC *, int *));
extern GENERIC er_cos PROTO((int, GENERIC *, GENERIC *, int *));
extern void gsrand PROTO((unsigned));
extern GENERIC grand PROTO((void));
extern GENERIC ld_2_n PROTO((int));
extern GENERIC getulp PROTO((GENERIC *, GENERIC *));
extern void nearby PROTO((GENERIC *));
extern void unpack PROTO((int, int, int, LONG *, GENERIC *, GENERIC *));
extern void update PROTO((int, int, int, GENERIC *, GENERIC *, struct summary *));
extern void double_check PROTO((int, char *, struct rational *, FLOAT *));
extern void bgn_all PROTO((void));
extern long randm PROTO((void));
extern void srandm PROTO((unsigned));
#ifdef notdef
extern char *ini_st PROTO((unsigned, char *, int));
extern char *set_st PROTO((char *));
#endif	/* defined(notdef) */
extern void report PROTO((int, struct summary *));
extern void title PROTO((char *, int));
extern void subtitle PROTO((PRTYPE *, PRTYPE *, int));
