/*	conversion from ucbtest macros to Sun macros	*/

#ifdef FLOAT
#undef FLOAT
#define SP
#endif

#ifdef DOUBLE
#undef DOUBLE
#define DP
#endif

#ifdef LONGDOUBLE
#undef LONGDOUBLE
#define QP
#endif

#ifdef sgi
#define	MACHINE_PI	1
#define MPI2_P  0x1921fb, 0x54442, 0xd1800      /* 50(->53)-bit */
#endif

#if defined(linux) || defined(_AUX)
#define	MACHINE_PI	1
#define MPI2_P  0x1921fb, 0x54442, 0xd1846, 0x98000     /* 66-bit pi/2 */
#endif

#if defined(__hpux) && defined(SP)
#define	MACHINE_PI	1
#define MPI2_P  0x1921fb, 0x54443       /* 41(->42)-bit */
#endif

#if defined(__hpux) && defined(DP)
#define	MACHINE_PI	1
#define MPI2_P  0x1921fb, 0x54442, 0xd1846, 0x98980     /* 74(->76)-bit */
#endif

/*	special definitions for Sun test harness	*/

#if defined(sunos_version)

#if (sunos_version/100 >= 5)
#define	POSIX_TIMES	1
#define	BSD_RUSAGE	0
#else
#define	POSIX_TIMES	0
#define	BSD_RUSAGE	1
#endif

#if (sunos_version/100 >= 5) || (sunpro_version >= 100)
#define	MACHINE_PI	0
#else
#define	MACHINE_PI	1
#define MPI2_P  0x1921fb, 0x54442, 0xd1846, 0x98000     /* 66-bit pi/2 */
#endif

#endif /* defined(sunos_version) */

#ifndef	MACHINE_PI
#define	MACHINE_PI	0
#endif
#ifndef	POSIX_TIMES
#define	POSIX_TIMES	1
#endif
#ifndef	BSD_RUSAGE
#define	BSD_RUSAGE	0
#endif

#ifdef NREGIONS
#define NDIV NREGIONS
#else
#define NDIV 64
#endif

#ifdef NTESTS
#define NARG NTESTS/(10*NDIV)
#else
#define NARG 2500
#endif

#ifndef	LONG
#define	LONG	long
#endif

#ifndef	GUARD_AGAINST_OVERFLOW
#define	GUARD_AGAINST_OVERFLOW	0
#endif

#define	TEST_SIN	0
#define	TEST_COS	0
#define	TEST_ATN	0
#define	TEST_EXP	0
#define	TEST_LOG	0
#define	TEST_EM1	0
#define	TEST_L1P	0
#ifdef	TEST_sin
#undef  TEST_SIN
#define	TEST_SIN	1
#endif
#ifdef	TEST_cos
#undef  TEST_COS
#define	TEST_COS	1
#endif
#ifdef	TEST_atan
#undef  TEST_ATN
#define	TEST_ATN	1
#endif
#ifdef	TEST_exp
#undef  TEST_EXP
#define	TEST_EXP	1
#endif
#ifdef	TEST_log
#undef  TEST_LOG
#define	TEST_LOG	1
#endif
#ifdef	TEST_expm1
#undef  TEST_EM1
#define	TEST_EM1	1
#endif
#ifdef	TEST_log1p
#undef  TEST_L1P
#define	TEST_L1P	1
#endif

#ifdef SP

#ifndef	GENERIC
#define	GENERIC	float
#endif
#ifndef	PRTYPE
#define	PRTYPE	double
#endif
#ifndef	PRFFMT
#define	PRFFMT(w, p, x)	(void) printf("%*.*f", w, p, x);
#endif
#ifndef	PREFMT
#define	PREFMT(w, p, x)	(void) printf("%*.*e", w, p, x);
#endif
#ifndef	BITS_M
#define	BITS_M	7
#endif

#define TESTSIN testsins_
#define TESTCOS testcoss_
#define TESTATN testatans_
#define TESTEXP testexps_
#define TESTEM1 testexpm1s_
#define TESTLOG testlogs_
#define TESTL1P testlog1ps_

#endif /* SP */

#ifdef DP

#ifndef	GENERIC
#define	GENERIC	double
#endif
#ifndef	PRTYPE
#define	PRTYPE	double
#endif
#ifndef	PRFFMT
#define	PRFFMT(w, p, x)	(void) printf("%*.*f", w, p, x);
#endif
#ifndef	PREFMT
#define	PREFMT(w, p, x)	(void) printf("%*.*e", w, p, x);
#endif
#ifndef	BITS_M
#define	BITS_M	10
#endif

#define TESTSIN testsind_
#define TESTCOS testcosd_
#define TESTATN testatand_
#define TESTEXP testexpd_
#define TESTEM1 testexpm1d_
#define TESTLOG testlogd_
#define TESTL1P testlog1pd_

#endif /* DP */

#ifdef QP

#ifndef	GENERIC
#define	GENERIC	long double
#endif
#ifndef	PRTYPE
#define	PRTYPE	long double
#endif
#ifndef	PRFFMT
#define	PRFFMT(w, p, x)	(void) printf("%*.*Lf", w, p, x);
#endif
#ifndef	PREFMT
#define	PREFMT(w, p, x)	(void) printf("%*.*Le", w, p, x);
#endif
#ifndef	BITS_M
#define	BITS_M	14
#endif

#define TESTSIN testsinx_
#define TESTCOS testcosx_
#define TESTATN testatanx_
#define TESTEXP testexpx_
#define TESTEM1 testexpm1x_
#define TESTLOG testlogx_
#define TESTL1P testlog1px_

#endif /* QP */

#ifdef __STDC__
#define ANSI_PROTOTYPES
#define PROTO(p)  p
#else
#undef ANSI_PROTOTYPES
#define PROTO(p)  ()
#endif
 
#define	FUNC_SIN(y, x)	TESTSIN( &y, &x);
#define	DECL_SIN	extern void TESTSIN PROTO((GENERIC *, GENERIC *));
DECL_SIN

#define	FUNC_COS(y, x)	TESTCOS(&y, &x);
#define	DECL_COS	extern void TESTCOS PROTO((GENERIC *, GENERIC *));
DECL_COS

#define	FUNC_ATN(y, x)	TESTATN(&y, &x);
#define	DECL_ATN	extern void TESTATN PROTO((GENERIC *, GENERIC *));
DECL_ATN

#define	FUNC_EXP(y, x)	TESTEXP(&y, &x);
#define	DECL_EXP	extern void TESTEXP PROTO((GENERIC *, GENERIC *));
DECL_EXP

#define	FUNC_EM1(y, x)	TESTEM1(&y, &x);
#define	DECL_EM1	extern void TESTEM1 PROTO((GENERIC *, GENERIC *));
DECL_EM1

#define	FUNC_LOG(y, x)	TESTLOG(&y, &x);
#define	DECL_LOG	extern void TESTLOG PROTO((GENERIC *, GENERIC *));
DECL_LOG

#define	FUNC_L1P(y, x)	TESTL1P(&y, &x);
#define	DECL_L1P	extern void TESTL1P PROTO((GENERIC *, GENERIC *));
DECL_L1P
