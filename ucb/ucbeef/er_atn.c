/*
 * Copyright 1/5/95 Zhishun Alex Liu.  All Rights Reserved.
 *
 ***************************
 * WORK IN PROGRESS
 ***************************
 *
 * Written by Zhishun Alex Liu under direction of Professor William Kahan.
 * The author's current electronic mail address as of December 1994 is
 * Alex.Liu@Eng.Sun.COM.
 *
 * Use of this software is granted with the understanding that all recipients
 * should regard themselves as participants in an ongoing research project and
 * hence should feel obligated to report their experiences (good or bad) with
 * this elementary functions test suite to the author.
 */
#ifndef lint
static char sccsid[] = "@(#)er_atn.c	1.1 (BeEF) 1/5/95";
#endif
/*
 * er_atn(.) computes the error in ULPs of the arc tangent under test.
 * With a large table (24 entries), an error of less than 0.05 ULPs is
 * guaranteed. (Bound proved: 0.048 ULPs).
 * extern GENERIC ld_2_n(.), getulp(.);
 * extern void unpack(.), double_check(.);
 */
#include "sys.h"
extern int sig_bits;			/* # of significant bits of GENERIC */
#if MACHINE_PI
					/* here we're computing ... 	*/
					/* Atan(x) := PI / pi atan(x)	*/
					/* = atan(x) - mpi_mu * atan(x)	*/
extern GENERIC mpi_mu;
#endif

#define	TRM_ATN 16			/* maximum # of allocated A[m] & B[m] */
static GENERIC off_atn;			/* least positive number such that    */
					/* atan(x) - x will differ from 0     */
static int trm_atn;			/* # of terms needed ... */
					/* for continued fractions */
static GENERIC A_atn[TRM_ATN],		/* USED BY ac_log(.) indirectly thru */
	B_atn[TRM_ATN];			/* its invocation of cf_atn() */

static FLOAT x_atn[ ] = {	/* The centers between break points */
	 2.373046875e-01,	/* C: 243/1024 */
	 2.529296875e-01,	/* C: 259/1024 */
	 2.773437500e-01,	/* C:  71/ 256 */
	 3.105468750e-01,	/* C: 159/ 512 */
	 3.437500000e-01,	/* C:  11/  32 */
	 3.779296875e-01,	/* C: 387/1024 */
	 4.130859375e-01,	/* C: 423/1024 */
	 4.492187500e-01,	/* C: 115/ 256 */
	 4.863281250e-01,	/* C: 249/ 512 */
	 5.234375000e-01,	/* C:  67/ 128 */
	 5.625000000e-01,	/* C:   9/  16 */
	 6.464843750e-01,	/* C: 331/ 512 */
	 7.382812500e-01,	/* C: 189/ 256 */
	 8.378906250e-01,	/* C: 429/ 512 */
	 9.472656250e-01,	/* C: 485/ 512 */
	 1.050781250e+00,	/* C: 269/ 256 */
	 1.148437500e+00,	/* C: 147/ 128 */
	 1.273437500e+00,	/* C: 163/ 128 */
	 1.437500000e+00,	/* C:  23/  16 */
	 1.605468750e+00,	/* C: 411/ 256 */
	 2.093750000e+00,	/* C:  67/  32 */
	 2.812500000e+00,	/* C:  45/  16 */
	 3.914062500e+00,	/* C: 501/ 128 */
	 6.218750000e+00,	/* C: 199/  32 */
};
#define	CTR_ATN (sizeof(x_atn) / sizeof(x_atn[0]))	/* # of centers */

static struct rational xpq_atn[ ] = {
	{  243, 1024},		/*  0:  2.3730468750e-01 */
	{  259, 1024},		/*  1:  2.5292968750e-01 */
	{  284, 1024},		/*  2:  2.7734375000e-01 */
	{  318, 1024},		/*  3:  3.1054687500e-01 */
	{  352, 1024},		/*  4:  3.4375000000e-01 */
	{  387, 1024},		/*  5:  3.7792968750e-01 */
	{  423, 1024},		/*  6:  4.1308593750e-01 */
	{  460, 1024},		/*  7:  4.4921875000e-01 */
	{  498, 1024},		/*  8:  4.8632812500e-01 */
	{  536, 1024},		/*  9:  5.2343750000e-01 */
	{  576, 1024},		/* 10:  5.6250000000e-01 */
	{  662, 1024},		/* 11:  6.4648437500e-01 */
	{  756, 1024},		/* 12:  7.3828125000e-01 */
	{  858, 1024},		/* 13:  8.3789062500e-01 */
	{  970, 1024},		/* 14:  9.4726562500e-01 */
	{ 1076, 1024},		/* 15:  1.0507812500e+00 */
	{ 1176, 1024},		/* 16:  1.1484375000e+00 */
	{ 1304, 1024},		/* 17:  1.2734375000e+00 */
	{ 1472, 1024},		/* 18:  1.4375000000e+00 */
	{ 1644, 1024},		/* 19:  1.6054687500e+00 */
	{ 2144, 1024},		/* 20:  2.0937500000e+00 */
	{ 2880, 1024},		/* 21:  2.8125000000e+00 */
	{ 4008, 1024},		/* 22:  3.9140625000e+00 */
	{ 6368, 1024},		/* 23:  6.2187500000e+00 */
};

static GENERIC	y_at_l[CTR_ATN + 1],	/* lowpart  of atan(x_atn) & pi / 2 */
		y_at_h[CTR_ATN + 1];	/* highpart of atan(x_atn) & pi / 2 */
/*
 * accurate atan values of the centers. stored in 20-bit chunks...
 *
 * for each i := 0, 1, ..., CTR_ATN:
 * atan(x_atn[i]) = sign * (1 + alpha) * 2**N where 0 <= alpha < 1
 * let j := PCS_ATN * i + 1, then
 * y_at_p[j - 1] = sign(atan(x_atn[i]))
 * y_at_p[j + 0] = sign(N) * 2**|N|
 * y_at_p[j + 1] = 1 + 1st 20 bits of alpha
 * y_at_p[j + 2] =     2nd 20 bits of alpha
 * y_at_p[j + 3] =     3rd 20 bits of alpha
 * y_at_p[j + 4] =     4th 20 bits of alpha
 * y_at_p[j + 5] =     5th 20 bits of alpha
 * ...
 */
static LONG y_at_p[] = {			/* MUST be of type LONG */
     1,  -8, 0x1dd2c6, 0xf45db, 0x8b9c9, 0xbec03, 0xbf27a,
              0xe608d, 0x172e2, 0xa99d0, 0x63f59, 0x7ee87,
     1,  -8, 0x1fb5c0, 0x55893, 0x475a0, 0x7aba7, 0x97db3,
              0xb0407, 0x66ed8, 0xdac27, 0xbbc40, 0xb462b,
     1,  -4, 0x115097, 0x3a9ce, 0x546a1, 0xa5160, 0x18d73,
              0x61842, 0x2d2fc, 0x5c9b4, 0x00b6f, 0xb8cfe,
     1,  -4, 0x13454b, 0xe5720, 0xa003b, 0xdfffc, 0xa144b,
              0x3ec7b, 0xd9da0, 0xf3811, 0x3b509, 0xb4fce,
     1,  -4, 0x1530ad, 0x9951c, 0xd49db, 0x5336f, 0xeef7e,
              0xfb3d1, 0x82425, 0x873a6, 0x3de9a, 0xfa744,
     1,  -4, 0x172023, 0xe88ea, 0x0a13d, 0xe5f2f, 0xd2ab9,
              0xd5aae, 0x1d797, 0x29190, 0xff855, 0x1546b,
     1,  -4, 0x191234, 0xc0bf7, 0x1368a, 0x81880, 0x74f63,
              0x1bc4f, 0x64840, 0x976f2, 0x1d0aa, 0xbc71a,
     1,  -4, 0x1b0564, 0x20ae9, 0x3439b, 0x3b1ae, 0x7272e,
              0xaa9ae, 0xbbfcd, 0x6abc9, 0x35ed2, 0x469bc,
     1,  -4, 0x1cf839, 0x6bc7f, 0xc8df6, 0x6c7d4, 0x52684,
              0xb5192, 0x207f1, 0x50560, 0x89632, 0x9a59c,
     1,  -4, 0x1edcb6, 0xd43f8, 0x434e0, 0x3689c, 0xcf77b,
              0x1c4c9, 0xb8b02, 0x17518, 0x6cb46, 0x88b20,
     1,  -2, 0x10657e, 0x94db3, 0x0cfc5, 0x496d4, 0x1396c,
              0x34a2b, 0x81e22, 0xab9b0, 0xee9bb, 0xf78ab,
     1,  -2, 0x125d63, 0x21466, 0x46f52, 0xaf45b, 0x34b3f,
              0x07a5b, 0xfe468, 0xe1936, 0x8b554, 0xfa6ff,
     1,  -2, 0x1459c6, 0x52bad, 0xc7f46, 0x65a63, 0xa384d,
              0x6f512, 0x7a7ea, 0xdaaeb, 0xd979b, 0xe14aa,
     1,  -2, 0x165147, 0x8826e, 0x4c87c, 0x44d71, 0x098f9,
              0x7a769, 0x790d5, 0x1dd99, 0x14c83, 0x32723,
     1,  -2, 0x18442f, 0xb8fc6, 0x7d2c7, 0xc70b8, 0x24b90,
              0x0e330, 0x38741, 0x08033, 0x4f0cc, 0xc5b27,
     1,  -2, 0x19ecca, 0x32969, 0x5e07a, 0x270e5, 0x0a9e1,
              0x94a28, 0xcde69, 0x19685, 0xcf1bc, 0x462d5,
     1,  -2, 0x1b5713, 0x92769, 0x134be, 0x824ca, 0x47366,
              0x18765, 0x9aad8, 0xb85ee, 0x4cc77, 0xe7c70,
     1,  -2, 0x1cf690, 0x462a5, 0xd2740, 0x225cc, 0x4caac,
              0x03288, 0x81752, 0x42401, 0xfbaf1, 0xaa7ce,
     1,  -2, 0x1ed0d9, 0x7c904, 0x1c8f6, 0xceb0e, 0x2512d,
              0xb7f4d, 0x344f1, 0x5d499, 0x61be9, 0xacf4c,
     1,   1, 0x10383c, 0x54504, 0x2e95d, 0x0ee64, 0x52553,
              0x66a06, 0x1698b, 0x5e33c, 0x97da8, 0xd5742,
     1,   1, 0x1200e5, 0xae0dd, 0x61d37, 0xda79b, 0xb203c,
              0x2f38f, 0xa7c42, 0x81c63, 0x4570d, 0x85134,
     1,   1, 0x13aab9, 0x8641f, 0x26ac4, 0x796c6, 0x7d148,
              0x72c07, 0x2dff7, 0x1fbeb, 0x3ca29, 0xab078,
     1,   1, 0x15216a, 0x87790, 0x2ee45, 0xe06f7, 0x1acdb,
              0x25899, 0x83f01, 0xd87f1, 0xf122f, 0xeba06,
     1,   1, 0x1694eb, 0x4cd16, 0x1d800, 0xe8c63, 0xf1687,
              0x2cc6b, 0x1cb94, 0x8380f, 0x0918c, 0xef2a0,
     1,   1, 0x1921fb, 0x54442, 0xd1846, 0x9898c, 0xc5170,
              0x1b839, 0xa2520, 0x49c11, 0x14cf9, 0x8e804,
};					/* # entries per atan packed value */
#define	PCS_ATN ((sizeof(y_at_p) / sizeof(y_at_p[0])) / (CTR_ATN + 1))

void
#ifdef __STDC__
dbc_atn(void)
#else
dbc_atn( )
#endif
{
	int i;

	for (i = 0; i < CTR_ATN; i++)
		double_check(i, "x_atn", &xpq_atn[i], &x_atn[i]);
}

static GENERIC
#ifdef __STDC__
_A_atn(int m)
#else
_A_atn(m)
int m;
#endif
{
	LONG num, denum, t;			/* MUST be of type LONG */

	t = m * (m + m - 1);
	num = 12 * t - 3;
	t = 4 * m + 1;
	denum = (t - 4) * t;
	return (GENERIC) num / (GENERIC) denum;
}

static GENERIC
#ifdef __STDC__
_B_atn(int m)
#else
_B_atn(m)
int m;
#endif
{
	LONG num, denum, t;			/* MUST be of type LONG */

	t = m * (m + m + 1);
	num = 36 * t * t;
	t = 4 * m + 1;
	t *= t;
	denum = (t - 4) * t;
	return (GENERIC) num / (GENERIC) denum;
}

void
#ifdef __STDC__
bgn_atn(void)
#else
bgn_atn( )
#endif
{
	int i, pieces;

	off_atn = ld_2_n(-sig_bits / 2 - 8);
	trm_atn = (sig_bits + 5) / 10 + 1;	/* # terms to be used in C.F. */
	pieces = (sig_bits + 15) / 20 + 1;	/* 16 bits more accurate */

	for (i = 1; i <= trm_atn; i++) {	/* generate C.F. coefficients */
		A_atn[i] = _A_atn(i);
		B_atn[i] = _B_atn(i);
	}
	unpack(pieces, (int) PCS_ATN, (int) CTR_ATN, y_at_p, y_at_l, y_at_h);
}

#if TEST_ATN || TEST_LOG || TEST_L1P
GENERIC
#ifdef __STDC__
cf_atn(GENERIC *_u)
#else
cf_atn(_u)				/* also USED BY ac_log(.) */
GENERIC *_u;				/* input argument */
#endif
{
	int m;
	GENERIC u = *_u, y = u, gtmp;

	for (m = trm_atn; m >= 1; m--) {
		gtmp = A_atn[m] - B_atn[m] / y;
		y = u + gtmp;
	}
	return y;
}
#endif	/* TEST_ATN || TEST_LOG || TEST_L1P */

#if TEST_ATN
/*
 * ALGORITHM...
 *
 * for x in the primary interval [0, .2294),
 *
 * atan(x) := x - sml_atn(x)
 *        = x - x / cf_atn(u)	with u := 3 / x^2
 *
 * where
 *						B_atn[1]
 * cf_atn(u) := u + A_atn[1] - ------------------------------------------------
 *							B_atn[2]
 *				 u + A_atn[2] - -------------------------------
 *								B_atn[3]
 *				  		u + A_atn[3] - ----------------
 *					   			u + .
 *						        		.
 *						          		    .
 * and
 *
 *		12m(2m - 1) - 3
 * A_atn[m] := -----------------,		m = 1, 2, ..., trm_atn
 *		(4m - 3)(4m + 1)
 *
 *			36[m(2m + 1)]^2
 * B_atn[m] := -----------------------------, 	m = 1, 2, ..., trm_atn
 *		[(4m + 1)^2 - 4][(4m + 1)^2]
 *
 *
 * for X not in the primary interval, we use the addition formula
 *
 *				X - Xo
 * atan(X) = atan(Xo) + atan ------------
 *			      1 + Xo * X
 *
 *			x - x_atn[i]
 * i.e. compute rx := ------------------ for x <= 10.125
 *			1 + x_atn[i] * x
 *
 *			-1
 *		rx := ------		if x >  10.125
 *			 x
 *
 * atan(x) := y_at_h[i] + atan(rx) + y_at_l[i]
 *
 *	    = y_at_h[i] + rx - sml_atn(rx) + y_at_l[i]
 *
 * In summary,
 *
 * for x in the primary interval,
 *
 * error = (y - x) + sml_atn(x)
 *
 * otherwise
 *
 * error = (((y - y_at_h[i]) - rx) + sml_atn(rx)) - y_at_l[i]
 *
 */

static GENERIC
#ifdef __STDC__
sml_atn(GENERIC *_x)
#else
sml_atn(_x)				/* returns x - atan(x) */
GENERIC *_x;				/* input argument */
#endif
{
	GENERIC x = *_x, gtmp;

	if (x <= off_atn && x >= -off_atn)	/* test to see if x too tiny */
		return ZERO;
	else {
		gtmp = THREE / (x * x);
		return x / cf_atn(&gtmp);
	}
}

GENERIC
#ifdef __STDC__
er_atn(int rid, GENERIC *_x, GENERIC *_y)
#else
er_atn(rid, _x, _y)
int rid;
GENERIC *_x, *_y;		/* input arguments */
#endif
{
	int i, sign;
	GENERIC x = *_x, y = *_y, rx, scale, ulp, error;

	i = rid;
	sign = 1;
	if (rid < 0) {			/* if negative argument */
		i = -i;				/* negate index */
		x = -x;				/* negate x */
		y = -y;				/* negate y */
		sign = -1;			/* record it */
	}
	if (i == 1) {
		error = y - x; error += sml_atn(&x);
#if MACHINE_PI
		error += mpi_mu * x;
#endif
	}
	else {
		i = i - 2;
		if (i < CTR_ATN)
			rx = (x - (GENERIC) x_atn[i]) /
				(ONE + (GENERIC) x_atn[i] * x);
		else
			rx = -ONE / x;
		error = (y - y_at_h[i]) - rx;
		error += sml_atn(&rx);
		error -= y_at_l[i];
#if MACHINE_PI
		error += mpi_mu * y_at_h[i];
		error += mpi_mu * rx;
#endif
	}

	ulp = y - error; ulp = getulp(&ulp, &scale);
	error *= scale;
	error /= ulp;
	return sign > 0 ? error : -error;
}
#endif	/* TEST_ATN */
