#ifndef lint
static char sccsid[] = "@(#)mpwglue.c	1.1 (BeEF) 1/5/95";
#endif
/*
 * MPW C 2.02 doesn't provide us with this most elementary time() call,
 * in addition to not providing perror() and isatty()...
 */
#ifdef macintosh
#include <OSUtils.h>
typedef long time_t;

time_t
time(tloc)
time_t *tloc;
{
	time_t clock;

	GetDateTime(&clock);
	if (tloc != (time_t *) 0)
		*tloc = clock;
	return clock;
}
#endif	/* macintosh */
