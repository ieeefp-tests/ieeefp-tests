/*
 * Copyright 1/5/95 Zhishun Alex Liu.  All Rights Reserved.
 *
 ***************************
 * WORK IN PROGRESS
 ***************************
 *
 * Written by Zhishun Alex Liu under direction of Professor William Kahan.
 * The author's current electronic mail address as of December 1994 is
 * Alex.Liu@Eng.Sun.COM.
 *
 * Use of this software is granted with the understanding that all recipients
 * should regard themselves as participants in an ongoing research project and
 * hence should feel obligated to report their experiences (good or bad) with
 * this elementary functions test suite to the author.
 */
#ifndef lint
static char sccsid[] = "@(#)report.c	1.1 (BeEF) 1/5/95";
#endif
#include <stdio.h>
#include "sys.h"
extern int sig_bits, sig_decimals;
#if 0
#if BEEF_QA
"    From         N.M.E. ...location of N.M.E.     ...in hexadecimal        "
"     to          P.M.E. ...location of P.M.E.     ...notation              "
"  0.0000000e+00 -0.00057  5.35479764034030840e-02 0x551f3e5b aa3e76b6      "
"  5.4687500e-02  0.00070  5.12857575543042080e-02 0x11033e52 2208b724      "
"       %15.7e  %8.5f           %25.17e                 %-25.17e            "
"----1234567----B---12345----12345678901234567----B---12345678901234567---- "
" -x.xxxxxxxe-xxB-x.xxxxx -x.xxxxxxxxxxxxxxxxxe-xxB-x.xxxxxxxxxxxxxxxxxe-xx "
#else
"    From         N.M.E. ...location of N.M.E.     non-monotonicity count   "
"     to          P.M.E. ...location of P.M.E.     sample loc. of failures  "
"  0.0000000e+00  -0.489   2.17987857023772250e-02 0                        "
"  5.4687500e-02   0.494   5.22826153890207030e-02                          "
"       %15.7e  %7.3f           %25.17e                 %1d                 "
"       %15.7e  %7.3f           %25.17e                 %-25.17e            "
"----1234567----B----123B----12345678901234567----B---12345678901234567---- "
" -x.xxxxxxxe-xxB-xx.xxxB -x.xxxxxxxxxxxxxxxxxe-xxB-x.xxxxxxxxxxxxxxxxxe-xx "
#endif
#endif	/* 0 */
void
#ifdef __STDC__
report(int lid, struct summary *s)
#else
report(lid, s)
int lid;
struct summary *s;
#endif
{
	PRTYPE l = (PRTYPE) s->l,
		r = (PRTYPE) s->r,
		en = (PRTYPE) s->en,
		ep = (PRTYPE) s->ep;

	if ((lid%NLINES) == 1) {	/* print column heading */
		(void) printf(
			"--> %c.M.E. := %s Maximum Error observed in ULPs\n",
			'N', "Negative");
		(void) printf(
			"--> %c.M.E. := %s Maximum Error observed in ULPs\n",
			'P', "Positive");
		(void) printf("\n");
		(void) printf("    From         N.M.E. %-*.*s %-s\n",
			sig_decimals + 8, sig_decimals + 8,
			"...location of N.M.E.",
#if BEEF_QA
			"...in hexadecimal");
#else
			"non-monotonicity count");
#endif
		(void) printf("     to          P.M.E. %-*.*s %-s\n\n",
			sig_decimals + 8, sig_decimals + 8,
			"...location of P.M.E.",
#if BEEF_QA
			"...notation");
#else
			"sample loc. of failures");
#endif
	}
	PREFMT(15, 7, l)			/* first row */
	(void) printf(" ");
	if (en < (PRTYPE) 0) {
		if (en > -(PRTYPE) 100) {
#if BEEF_QA
			PRFFMT(7, 4, en)
#else
			PRFFMT(7, 3, en)
#endif
			(void) printf(" ");
		}
		else if (en > -(PRTYPE) 10000) {	/* for MC68881/2 */
			PRFFMT(7, 1, en)
			(void) printf(" ");
		}
		else {
			PREFMT(7, 0, en)
			(void) printf(" ");
		}
		PREFMT(sig_decimals + 8, sig_decimals, (PRTYPE) s->xn)
		if (s->nn)
			(void) printf("*2^%d", s->nn);
		(void) printf(" ");
	}
	else
		(void) printf("  ----- %*.*s ",
			sig_decimals + 8, sig_decimals + 8,
			"  -------------------------------------------------");
#if BEEF_QA
#ifndef lint
	if (en < (PRTYPE) 0)
		(void) printf("0x%08.8x %08.8x", (PRTYPE) s->xn);
#endif	/* !defined(lint) */
#else
	(void) printf("%1d", s->mcnt);
#endif
	(void) printf("\n");
	PREFMT(15, 7, r)			/* second row */
	(void) printf(" ");
	if (ep > (PRTYPE) 0) {
		if (ep < (PRTYPE) 100) {
#if BEEF_QA
			PRFFMT(7, 4, ep)
#else
			PRFFMT(7, 3, ep)
#endif
			(void) printf(" ");
		}
		else if (ep < (PRTYPE) 10000) {		/* for MC68881/2 */
			PRFFMT(7, 1, ep)
			(void) printf(" ");
		}
		else {
			PREFMT(7, 0, ep)
			(void) printf(" ");
		}
		PREFMT(sig_decimals + 8, sig_decimals, (PRTYPE) s->xp)
		if (s->np)
			(void) printf("*2^%d", s->np);
		(void) printf(" ");
	}
	else
		(void) printf("  ----- %*.*s ",
			sig_decimals + 8, sig_decimals + 8,
			"  -------------------------------------------------");
#if BEEF_QA
#ifndef lint
	if (ep > (PRTYPE) 0)				/* ARGUSED */
		(void) printf("0x%08.8x %08.8x", (PRTYPE) s->xp);
#endif	/* !defined(lint) */
#else
	if (s->mcnt) {
		PREFMT(sig_decimals + 8, sig_decimals, (PRTYPE) s->xm)
		if (s->nm)
			(void) printf("*2^%d", s->nm);
	}
#endif
	if (s->scnt)			/* sin/cos/atan */
		(void) printf(" {%d}", s->scnt);
	(void) printf("\n");
	if (lid%NLINES)			/* skip to a new line */
		(void) printf("\n");
	else				/* skip to a new page */
		(void) printf("\f");
	(void) fflush(stdout);
}

void
#ifdef __STDC__
title(char *fct_name, int narg)
#else
title(fct_name, narg)
char *fct_name;
int narg;
#endif
{
	(void) printf(">> Elementary functions' accuracy test for %s <<\n\n",
		fct_name);
	(void) printf("Sampling rate: %d random arguments per subregion\n\f",
		narg);
}

void
#ifdef __STDC__
subtitle(PRTYPE *bgn, PRTYPE *End, int ndiv)
#else
subtitle(bgn, End, ndiv)
PRTYPE *bgn, *End;			/* input arguments */
int ndiv;
#endif
{
	(void) printf("Testing range: [");	/* ] */
	PRFFMT(10, 7, *bgn)
	(void) printf(", ");
	PRFFMT(10, 7, *End)
/* ( */	(void) printf(")\tNumber of subregions: %d\n\n", ndiv);
}
