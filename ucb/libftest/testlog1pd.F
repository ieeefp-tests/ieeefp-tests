#include "libftest.h"

#define NAME testlog1pd
#define TYPE DOUBLE

#ifdef SUN_LIBS

#define FUNC d_log1p
#include "testfunc1.F"

#else

#include "testlog1p.F"

#endif	
