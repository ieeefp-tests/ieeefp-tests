#include "libftest.h"

#define NAME testhypotx
#define TYPE EXTENDED

#ifdef SUN_LIBS

#define FUNC hypotl
#include "testfunc2c.F"

#else

	subroutine NAME UNDERSCORE ( z, x, y)
	TYPE z, x, y

#ifdef __hpux
        z = sqrt(x*x+y*y)
#else
	z = abs(qcmplx(x,y))
#endif
	return
	end	

#endif	
