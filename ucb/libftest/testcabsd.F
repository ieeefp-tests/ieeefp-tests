#include "libftest.h"

#define NAME testcabsd
#define TYPE DOUBLE
#define TYPECOMPLEX DOUBLECOMPLEX

	subroutine NAME UNDERSCORE ( z, x)
	TYPE z 
	TYPECOMPLEX x

	z = abs(x)
	return
	end	
