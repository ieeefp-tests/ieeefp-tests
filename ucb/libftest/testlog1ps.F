#include "libftest.h"

#define NAME testlog1ps
#define TYPE SINGLE

#ifdef SUN_LIBS

#define FUNC r_log1p
#include "testfunc1.F"

#else

#include "testlog1p.F"

#endif	
