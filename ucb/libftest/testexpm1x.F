#include "libftest.h"

#define NAME testexpm1x
#define TYPE EXTENDED

#ifdef SUN_LIBS

#define FUNC expm1l
#include "testfunc1c.F"

#else

#include "testexpm1.F"

#endif	
