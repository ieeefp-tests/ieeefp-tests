#ifndef SUN_LIBS
#if (sunos_version/100 == 4) || (sunpro_version >= 100)
#define SUN_LIBS
#endif
#endif

#ifndef UNDERSCORE
#if defined(__hpux) || defined(_AIX)
#define UNDERSCORE _
#else
#define UNDERSCORE  
#endif
#endif

#define SINGLE	real*4
#define DOUBLE	real*8
#define EXTENDED real*16

#define SINGLECOMPLEX	complex*8
#define DOUBLECOMPLEX	complex*16
#define EXTENDEDCOMPLEX complex*32
