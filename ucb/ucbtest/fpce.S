#if (defined(sparc) || defined(__sparc) || defined(__sparc)) && (sunos_version >= 500)

	.file	"fpce.sparc.c"
gcc2_compiled.:
.section	".text"
	.align 4
	.global fesetround
	.type	 fesetround,#function
	.proc	04
fesetround:
	!#PROLOGUE# 0
	save %sp,-120,%sp
	!#PROLOGUE# 1
	st %i0,[%fp+68]
	st %fsr,[%fp-20]
	ld [%fp-20],%o0
	sethi %hi(1073741823),%o2
	or %o2,%lo(1073741823),%o1
	and %o0,%o1,%o0
	ld [%fp+68],%o2
	sll %o2,30,%o1
	or %o0,%o1,%o0
	st %o0,[%fp-20]
	ld [%fp-20],%fsr
	nop
	nop
	nop
.LL1:
	ret
	restore
.LLfe1:
	.size	 fesetround,.LLfe1-fesetround
	.ident	"GCC: (GNU) 2.6.2"
#endif

#if defined(i386) || defined(i486) || defined(i86pc)
	.file	"fpce.s"
	.version	"01.01"
gcc2_compiled.:
.text
	.align 8
.globl fesetround
	.type	 fesetround,@function
fesetround:
	pushl %ebp
	movl %esp,%ebp
	subl $4,%esp
	fstcw -2(%ebp)
	movw -2(%ebp),%ax
	andb $243,%ah
	movw 8(%ebp),%dx
	salw $10,%dx
	movl %eax,%ecx
	orw %dx,%cx
	movw %cx,-2(%ebp)
	fldcw -2(%ebp)
.L1:
	movl %ebp,%esp
	popl %ebp
	ret
.Lfe1:
	.size	 fesetround,.Lfe1-fesetround
	.align 8
.globl fesetprec
	.type	 fesetprec,@function
fesetprec:
	pushl %ebp
	movl %esp,%ebp
	subl $4,%esp
	fstcw -2(%ebp)
	movw -2(%ebp),%ax
	andb $252,%ah
	movw 8(%ebp),%dx
	salw $8,%dx
	movl %eax,%ecx
	orw %dx,%cx
	movw %cx,-2(%ebp)
	fldcw -2(%ebp)
.L2:
	movl %ebp,%esp
	popl %ebp
	ret
.Lfe2:
	.size	 fesetprec,.Lfe2-fesetprec
	.ident	"GCC: (GNU) 2.6.3"
#endif

#if defined(m68k) && defined(_AUX)
	data	1
	text
	global	fesetround
fesetround:
	link.l	%fp,&F%1
	movm.l	&M%1,(F%1+4,%fp)
	fmovm	&FPM%1,FPO%1(%fp)
	fmov.l	%fpcr,%d2
	mov.l	&-49,%d0
	and.l	%d2,%d0
	mov.l	(8,%fp),%d1
	lsl.l	&4,%d1
	or.l	%d1,%d0
	mov.l	%d0,%d2
	fmov.l	%d2,%fpcr
	movm.l	(F%1+4,%fp),&M%1
	fmovm	FPO%1(%fp),&FPM%1
	unlk	%fp
	rts
	set	S%1,0
	set	FPO%1,0
	set	FPM%1,0x0000
	set	M%1,0x0004
	set	F%1,-8

	data	1
	text
	global	fesetprec
fesetprec:
	link.l	%fp,&F%2
	movm.l	&M%2,(F%2+4,%fp)
	fmovm	&FPM%2,FPO%2(%fp)
	fmov.l	%fpcr,%d2
	mov.l	%d2,%d0
	and.l	&-193,%d0
	mov.l	(8,%fp),%d1
	lsl.l	&6,%d1
	or.l	%d1,%d0
	mov.l	%d0,%d2
	fmov.l	%d2,%fpcr
	movm.l	(F%2+4,%fp),&M%2
	fmovm	FPO%2(%fp),&FPM%2
	unlk	%fp
	rts
	set	S%2,0
	set	FPO%2,0
	set	FPM%2,0x0000
	set	M%2,0x0004
	set	F%2,-8
	data	1
#endif

#if !defined(_AUX) && (defined(m68k) || defined(mc68000))
	.text
        .even
.globl _fesetround
_fesetround:
        link a6,#0
        movel d2,sp@-
        movel a6@(8),d0
        fmovel fpcr,d1
        moveq #-49,d2
        andl d2,d1
        asll #4,d0
        orl d0,d1
        fmovel d1,fpcr
        movel a6@(-4),d2
        unlk a6
        rts
        .even
.globl _fesetprec
_fesetprec:
        link a6,#0
        movel a6@(8),d0
        fmovel fpcr,d1
        andw #65343,d1
        asll #6,d0
        orl d0,d1
        fmovel d1,fpcr
        unlk a6
        rts
#endif

#ifdef __alpha			/* empty file to please gcc on alpha */
        .verstamp 3 11 2 6 4
        .set noreorder
        .set volatile
        .set noat
        .file   1 "null.c"
gcc2_compiled.:
__gnu_compiled_c:
#endif
