/* This is m68k.c.  */
/*   csr   x
     0x00  0 = TONEAREST
     0x10  1 = TOWARDZERO
     0x20  2 = DOWNWARD
     0x30  3 = UPWARD      */

void 
fesetround (x)
     int x;
{
  register int csr;

  __asm ("fmove%.l fpcr,%0"
         : "=ro" (csr)
         : /* no input */ );

  csr = (csr & ~0x30) | (x << 4);

  __asm ("fmove%.l %0,fpcr"
         : /* no output */
         : "ro" (csr));
}


/*   csr   x
     0x00  0 = EXTENDED
     0x40  1 = SINGLE
     0x80  2 = DOUBLE     */

void 
fesetprec (x)
     int x;
{
  register int csr;

  __asm ("fmove%.l fpcr,%0"
         : "=ro" (csr)
         : /* no input */ );

  csr = (csr & ~0xc0) | (x << 6);

  __asm ("fmove%.l %0,fpcr"
         : /* no output */
         : "ro" (csr));
}

