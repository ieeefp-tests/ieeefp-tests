#include "ucbtest.h"
#include "../libctest/libctest.h"

#if (sunpro_version >= 300) && !defined(SUN_MATH)
#define SUN_MATH
#endif

#ifdef _PLAIN_UNIX
#ifdef DP
extern int
#ifdef __STDC__
k_toextd(),
#endif
k_tointd(), k_tosingled(), k_todoubled(),
k_addd(), k_subd(), k_muld(), k_divd(),
k_acosd(), k_asind(), k_atand(), k_atan2d(),
k_cabsd(), k_ceild(), k_cosd(), k_coshd(), k_expd(), 
k_fabsd(), k_floord(), k_fmodd(), k_hypotd(), k_logd(), k_log10d(), k_powd(),
k_sind(), k_sinhd(), k_sqrtd(), k_tand(), k_tanhd()
;
#endif
#ifdef SP
extern int
#ifdef __STDC__
k_toexts(),
#endif
k_toints(), k_tosingles(), k_todoubles(),
k_adds(), k_subs(), k_muls(), k_divs(),
k_acoss(), k_asins(), k_atans(), k_atan2s(),
k_cabss(), k_ceils(), k_coss(), k_coshs(), k_exps(), 
k_fabss(), k_floors(), k_fmods(), k_hypots(), k_logs(), k_log10s(), k_pows(),
k_sins(), k_sinhs(), k_sqrts(), k_tans(), k_tanhs()
;
#endif
#ifdef QP
extern int
k_tointq(), k_tosingleq(), k_todoubleq(), k_toextq(),
k_addq(), k_subq(), k_mulq(), k_divq(),
k_acosq(), k_asinq(), k_atanq(), k_atan2q(),
k_cabsq(), k_ceilq(), k_cosq(), k_coshq(), k_expq(), 
k_fabsq(), k_floorq(), k_fmodq(), k_hypotq(), k_logq(), k_log10q(), k_powq(),
k_sinq(), k_sinhq(), k_sqrtq(), k_tanq(), k_tanhq()
;
#endif
#else
extern int
k_acos(),	k_acosf(),	k_acosh(),	k_acoshf(),
k_acospi(),	k_acospif(),	k_add(),
k_aint(),	k_aintf(),	k_anint(),	k_anintf(),
k_annuity(),	k_annuityf(),	k_asin(),	k_asinf(),
k_asinh(),	k_asinhf(),	k_asinpi(),	k_asinpif(),
k_atan(),	k_atanf(),	k_atan2(),	k_atan2f(),
k_atan2pi(),	k_atan2pif(),
k_atanh(),	k_atanhf(),	k_atanpi(),	k_atanpif(),
k_cabs(),	k_cbrt(),	k_cbrtf(),
k_ceil(),	k_ceilf(),	k_fp_class(),	k_fp_classf(),
k_compound(),	k_compoundf(),	k_copysign(),	k_copysignf(),
k_cos(),	k_cosf(),	k_cosh(),	k_coshf(),
k_cospi(),	k_cospif(),	k_div(),
k_erf(),	k_erff(),	k_erfc(),	k_erfcf(),
k_exp(),	k_expf(),	k_exp10(),	k_exp10f(),
k_exp2(),	k_exp2f(),	k_expm1(),	k_expm1f(),
k_fabs(),	k_fabsf(),	k_finite(),	k_finitef(),
k_floor(),	k_floorf(),	k_fmod(),	k_fmodf(),
k_hypot(),	k_hypotf(),	k_ilogb(),	k_ilogbf(),
k_irint(),	k_irintf(),	k_isinf(),	k_isinff(),
k_isnan(),	k_isnanf(),	k_isnormal(),	k_isnormalf(),
k_issubnormal(),k_issubnormalf(),k_iszero(),	k_iszerof(),
k_j0(),		k_j0f(),	k_j1(),		k_j1f(),
k_jn(),		k_jnf(),	k_lgamma(),
k_log(),	k_logf(),	k_log10(),	k_log10f(),
k_log1p(),	k_log1pf(),	k_log2(),	k_log2f(),
k_logb(),	k_mul(),
k_nextafter(),	k_nextafterf(),	k_nint(),	k_nintf(),
k_pow(),	k_powf(),
#ifdef UCBFLIBTEST
k_pow_di(),	k_pow_ri(),
#endif
k_remainder(),	k_remainderf(),	k_rint(),	k_rintf(),
k_scalb(),	k_scalbn(),	k_scalbnf(),
k_signbit(),	k_signbitf(),	k_sin(),	k_sinf(),
k_sincos_c(),	k_sincos_s(),	k_sincosf_c(),	k_sincosf_s(),
k_sincospi_c(),	k_sincospi_s(),	k_sincospif_c(),k_sincospif_s(),
k_sinh(),	k_sinhf(),	k_sinpi(),	k_sinpif(),
k_sqrt(),	k_sqrtf(),	k_sub(),
k_tan(),	k_tanf(),	k_tanh(),	k_tanhf(),
k_tanpi(),	k_tanpif(),	k_y0(),		k_y0f(),
k_y1(),		k_y1f(),	k_yn(),		k_ynf()
#ifdef SUN_MATH
,k_sind(),	k_sindf(),	k_cosd(),	k_cosdf(),
k_tand(),	k_tandf(),	k_asind(),	k_asindf()
,k_acosd(),	k_acosdf(),	k_atand(),	k_atandf(),
k_atan2d(),	k_atan2df()
#endif
, k_sincosd_s(),	k_sincosd_c()
;
#endif

extern int sizeoffun;

#ifdef QP
#define NQ (sizeof(long double)/sizeof(long))
#endif
