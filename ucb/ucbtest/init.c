#include "ucbtest.h"

UINT32 significand_length = 0L;
UINT32 ntests = (UINT32) NTESTS;
UINT32 nregions = 1L;

#ifdef GENERIC

UINT32 get_significand PROTO((void))

{				/* get number of significant bits */
FINT significand_length=0;

#ifdef SP
                testsignifs_(&significand_length);
#endif
#ifdef DP
                testsignifd_(&significand_length);
#endif
#ifdef QP
                testsignifx_(&significand_length);
#endif

return(significand_length);
}

#else
#define GENERIC_STRING "generic"
#endif

void
ucbstart(file, line)
	char           *file;
	int             line;
{

	printf(" ucbtest START   in %s at line %d for %s \n",
		file, line, GENERIC_STRING);
#ifdef GENERIC		/* mainly for beefscan */
	generic_precision();
	significand_length = get_significand();
	if (significand_length > 64) ntests /= 100L; /* software quad is slow */
	printf(" %ld tests on %ld regions with %ld significant bits \n",
		ntests, nregions, significand_length);
#endif
}

void
ucbpass(file, line)
	char           *file;
	int             line;
{

	retrospective();

	printf(" ucbtest UCBPASS in %s at line %d for %s \n",
	       file, line, GENERIC_STRING);
	exit(0);
}

void 
ucbfail(file, line)
	char           *file;
	int             line;
{

	printf(" ucbtest UCBFAIL in %s at line %d for %s \n",
	       file, line, GENERIC_STRING);
#ifdef NO_FAIL
	exit(0);
#else
	exit(42);
#endif
}
