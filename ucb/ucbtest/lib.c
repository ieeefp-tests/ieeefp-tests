#define _PLAIN_UNIX

/*
Copyright (C) 1988-1994 Sun Microsystems, Inc. 2550 Garcia Avenue
Mountain View, California  94043 All rights reserved.

Any person is hereby authorized to download, copy, use, create bug fixes, 
and distribute, subject to the following conditions:

	1.  the software may not be redistributed for a fee except as
	    reasonable to cover media costs;
	2.  any copy of the software must include this notice, as well as 
	    any other embedded copyright notices; and 
	3.  any distribution of this software or derivative works thereof 
	    must comply with all applicable U.S. export control laws.

THE SOFTWARE IS MADE AVAILABLE "AS IS" AND WITHOUT EXPRESS OR IMPLIED
WARRANTY OF ANY KIND, INCLUDING BUT NOT LIMITED TO THE IMPLIED
WARRANTIES OF DESIGN, MERCHANTIBILITY, FITNESS FOR A PARTICULAR
PURPOSE, NON-INFRINGEMENT, PERFORMANCE OR CONFORMANCE TO
SPECIFICATIONS.  

BY DOWNLOADING AND/OR USING THIS SOFTWARE, THE USER WAIVES ALL CLAIMS
AGAINST SUN MICROSYSTEMS, INC. AND ITS AFFILIATED COMPANIES IN ANY
JURISDICTION, INCLUDING BUT NOT LIMITED TO CLAIMS FOR DAMAGES OR
EQUITABLE RELIEF BASED ON LOSS OF DATA, AND SPECIFICALLY WAIVES EVEN
UNKNOWN OR UNANTICIPATED CLAIMS OR LOSSES, PRESENT AND FUTURE.

IN NO EVENT WILL SUN MICROSYSTEMS, INC. OR ANY OF ITS AFFILIATED
COMPANIES BE LIABLE FOR ANY LOST REVENUE OR PROFITS OR OTHER SPECIAL,
INDIRECT AND CONSEQUENTIAL DAMAGES, EVEN IF IT HAS BEEN ADVISED OF THE
POSSIBILITY OF SUCH DAMAGES.

This file is provided with no support and without any obligation on the
part of Sun Microsystems, Inc. ("Sun") or any of its affiliated
companies to assist in its use, correction, modification or
enhancement.  Nevertheless, and without creating any obligation on its
part, Sun welcomes your comments concerning the software and requests
that they be sent to fdlibm-comments@sunpro.sun.com.
*/

#include "lib.h"

struct fun {
	char	*name;
	int	(*address)();
	int	in;
	int	out;
	char	*prec;
	int	x_mask;
} kfun[] = {
	/* double function */
#ifdef DP
#ifdef __STDC__
"toextd",    k_toextd,	2,2,	"extended",	0,
#endif
"tointd",    k_tointd,	2,2,	"extended",	0,
"tosingled", k_tosingled,2,2,	"extended",	0,
"todoubled", k_todoubled,2,2,	"extended",	0,
"addd",      k_addd,	4,2,	"double",	0,
"subd",      k_subd,	4,2,	"double",	0,
"muld",      k_muld,	4,2,	"double",	0,
"divd",      k_divd,	4,2,	"double",	0,
"acosd",     k_acosd,	2,2,	"extended",	0,
"asind",     k_asind,	2,2,	"extended",	0,
"atand",     k_atand,	2,2,	"extended",	0,
"atan2d",    k_atan2d,	4,2,	"extended",	0,
"cabsd",     k_cabsd,	4,2,	"extended",	0,
"ceild",     k_ceild,	2,2,	"extended",	0,
"cosd",	     k_cosd,	2,2,	"extended",	0,
"coshd",     k_coshd,	2,2,	"extended",	0,
"expd",	     k_expd,	2,2,	"extended",	0,
"fabsd",     k_fabsd,	2,2,	"extended",	0,
"floord",    k_floord,	2,2,	"extended",	0,
"fmodd",     k_fmodd,	4,2,	"extended",	0,
"hypotd",    k_hypotd,	4,2,	"extended",	0,
"logd",	     k_logd,	2,2,	"extended",	0,
"log10d",    k_log10d,	2,2,	"extended",	0,
"powd",	     k_powd,	4,2,	"extended",	0,
"sind",	     k_sind,	2,2,	"extended",	0,
"sinhd",     k_sinhd,	2,2,	"extended",	0,
"sqrtd",     k_sqrtd,	2,2,	"double",	0,
"tand",	     k_tand,	2,2,	"extended",	0,
"tanhd",     k_tanhd,	2,2,	"extended",	0,
#endif
#ifdef SP
#ifdef __STDC__
"toexts",    k_toexts,	1,1,	"extended",	0,
#endif
"toints",    k_toints,	1,1,	"extended",	0,
"tosingles", k_tosingles,1,1,	"extended",	0,
"todoubles", k_todoubles,1,1,	"extended",	0,
"adds",      k_adds,	2,1,	"single",	0,
"subs",      k_subs,	2,1,	"single",	0,
"muls",      k_muls,	2,1,	"single",	0,
"divs",      k_divs,	2,1,	"single",	0,
"acoss",     k_acoss,	1,1,	"extended",	0,
"asins",     k_asins,	1,1,	"extended",	0,
"atans",     k_atans,	1,1,	"extended",	0,
"atan2s",    k_atan2s,	2,1,	"extended",	0,
"cabss",     k_cabss,	2,1,	"extended",	0,
"ceils",     k_ceils,	1,1,	"extended",	0,
"coss",	     k_coss,	1,1,	"extended",	0,
"coshs",     k_coshs,	1,1,	"extended",	0,
"exps",	     k_exps,	1,1,	"extended",	0,
"fabss",     k_fabss,	1,1,	"extended",	0,
"floors",    k_floors,	1,1,	"extended",	0,
"fmods",     k_fmods,	2,1,	"extended",	0,
"hypots",    k_hypots,	2,1,	"extended",	0,
"logs",	     k_logs,	1,1,	"extended",	0,
"log10s",    k_log10s,	1,1,	"extended",	0,
"pows",	     k_pows,	2,1,	"extended",	0,
"sins",	     k_sins,	1,1,	"extended",	0,
"sinhs",     k_sinhs,	1,1,	"extended",	0,
"sqrts",     k_sqrts,	1,1,	"single",	0,
"tans",	     k_tans,	1,1,	"extended",	0,
"tanhs",     k_tanhs,	1,1,	"extended",	0,
#endif
#ifdef QP
"toextq",    k_toextq,	NQ,NQ,	"extended",	0,
"tointq",    k_tointq,	NQ,NQ,	"extended",	0,
"tosingleq", k_tosingleq,NQ,NQ,	"extended",	0,
"todoubleq", k_todoubleq,NQ,NQ,	"extended",	0,
"addq",      k_addq,	2*NQ,NQ,"extended",	0,
"subq",      k_subq,	2*NQ,NQ,"extended",	0,
"mulq",      k_mulq,	2*NQ,NQ,"extended",	0,
"divq",      k_divq,	2*NQ,NQ,"extended",	0,
"acosq",     k_acosq,	NQ,NQ,	"extended",	0,
"asinq",     k_asinq,	NQ,NQ,	"extended",	0,
"atanq",     k_atanq,	NQ,NQ,	"extended",	0,
"atan2q",    k_atan2q,	2*NQ,NQ,"extended",	0,
"cabsq",     k_cabsq,	2*NQ,NQ,"extended",	0,
"ceilq",     k_ceilq,	NQ,NQ,	"extended",	0,
"cosq",	     k_cosq,	NQ,NQ,	"extended",	0,
"coshq",     k_coshq,	NQ,NQ,	"extended",	0,
"expq",	     k_expq,	NQ,NQ,	"extended",	0,
"fabsq",     k_fabsq,	NQ,NQ,	"extended",	0,
"floorq",    k_floorq,	NQ,NQ,	"extended",	0,
"fmodq",     k_fmodq,	2*NQ,NQ,"extended",	0,
"hypotq",    k_hypotq,	2*NQ,NQ,"extended",	0,
"logq",	     k_logq,	NQ,NQ,	"extended",	0,
"log10q",    k_log10q,	NQ,NQ,	"extended",	0,
"powq",	     k_powq,	2*NQ,NQ,"extended",	0,
"sinq",	     k_sinq,	NQ,NQ,	"extended",	0,
"sinhq",     k_sinhq,	NQ,NQ,	"extended",	0,
"sqrtq",     k_sqrtq,	NQ,NQ,	"extended",	0,
"tanq",	     k_tanq,	NQ,NQ,	"extended",	0,
"tanhq",     k_tanhq,	NQ,NQ,	"extended",	0,
#endif

#ifndef _PLAIN_UNIX
#ifdef SUN_MATH
"k_acosd",	     k_acosd,	2,2,	"extended",	0,
"k_asind",	     k_asind,	2,2,	"extended",	0,
"k_cosd",	     k_cosd,	2,2,	"extended",	0,
"k_sind",	     k_sind,	2,2,	"extended",	0,
"k_sincosd_c",	     k_sincosd_c,2,2,	"extended",	0,
"k_sincosd_s",	     k_sincosd_s,2,2,	"extended",	0,
"k_atand",	     k_atand,	2,2,	"extended",	1,
"k_tand",	     k_tand,	2,2,	"extended",	0,
"k_atan2d",	     k_atan2d,	4,2,	"extended",	0,
#endif
"k_acosh",	     k_acosh,	2,2,	"extended",	0,
"k_acospi",	     k_acospi,	2,2,	"extended",	0,
"k_add",	     k_add,	4,2,	"double",	0,
"k_aint",	     k_aint,	2,2,	"extended",	0,
"k_anint",	     k_anint,	2,2,	"extended",	0,
"k_annuity",	     k_annuity,	4,2,	"extended",	0,
"k_asinh",	     k_asinh,	2,2,	"extended",	0,
"k_asinpi",	     k_asinpi,	2,2,	"extended",	0,
"k_atanh",	     k_atanh,	2,2,	"extended",	1,
"k_atanpi",	     k_atanpi,	2,2,	"extended",	0,
"k_atan2pi",	     k_atan2pi,	4,2,	"extended",	0,
"k_cbrt",	     k_cbrt,	2,2,	"extended",	0,
"k_compound",	     k_compound,4,2,	"extended",	0,
"k_copysign",	     k_copysign,4,2,	"extended",	0,
"k_cospi",	     k_cospi,	2,2,	"extended",	0,
"k_div",	     k_div,	4,2,	"double",	0,
"k_erf",	     k_erf,	2,2,	"extended",	0,
"k_erfc",	     k_erfc,	2,2,	"extended",	0,
"k_exp10",	     k_exp10,	2,2,	"extended",	0,
"k_exp2",	     k_exp2,	2,2,	"extended",	0,
"k_expm1",	     k_expm1,	2,2,	"extended",	0,
"k_finite",	     k_finite,	2,1,	"extended",	0,
"k_fp_class",	     k_fp_class,2,1,	"extended",	0,
"k_ilogb",	     k_ilogb,	2,1,	"extended",	0,
"k_irint",	     k_irint,	2,1,	"extended",	0,
"k_isinf",	     k_isinf,	2,1,	"extended",	0,
"k_isnan",	     k_isnan,	2,1,	"extended",	0,
"k_isnormal",	     k_isnormal,2,1,	"extended",	0,
"k_issubnormal",     k_issubnormal,2,1,	"extended",	0,
"k_iszero",	     k_iszero,	2,1,	"extended",	0,
"k_j0",		     k_j0,	2,2,	"extended",	0,
"k_j1",		     k_j1,	2,2,	"extended",	0,
"k_jn",		     k_jn,	3,2,	"extended",	0,
"k_lgamma",	     k_lgamma,	2,2,	"extended",	0,
"k_log1p",	     k_log1p,	2,2,	"extended",	0,
"k_log2",	     k_log2,	2,2,	"extended",	0,
"k_logb",	     k_logb,	2,2,	"extended",	0,
"k_mul",	     k_mul,	4,2,	"double",	0,
"k_nextafter",	     k_nextafter,4,2,	"extended",	0,
"k_nint",	     k_nint,	2,1,	"extended",	0,
#ifdef UCBFLIBTEST
"k_pow_di",	     k_pow_di,	3,2,	"extended",	0,
#endif
"k_remainder",	     k_remainder,4,2,	"extended",	0,
"k_rint",	     k_rint,	2,2,	"extended",	0,
"k_scalb",	     k_scalb,	4,2,	"extended",	0,
"k_scalbn",	     k_scalbn,	3,2,	"extended",	0,
"k_signbit",	     k_signbit,	2,1,	"extended",	0,
"k_sincos_c",	     k_sincos_c,2,2,	"extended",	0,
"k_sincos_s",	     k_sincos_s,2,2,	"extended",	0,
"k_sincospi_c",	     k_sincospi_c,2,2,	"extended",	0,
"k_sincospi_s",	     k_sincospi_s,2,2,	"extended",	0,
"k_sinpi",	     k_sinpi,	2,2,	"extended",	0,
"k_sub",	     k_sub,	4,2,	"extended",	0,
"k_tanpi",	     k_tanpi,	2,2,	"extended",	0,
"k_y0",		     k_y0,	2,2,	"extended",	0,
"k_y1",		     k_y1,	2,2,	"extended",	0,
"k_yn",		     k_yn,	3,2,	"extended",	0,

	/* float function */
#ifdef SUN_MATH
"k_acosdf",	     k_acosdf,	1,1,	"extended",	0,
"k_asindf",	     k_asindf,	1,1,	"extended",	0,
"k_atandf",	     k_atandf,	1,1,	"extended",	0,
"k_cosdf",	     k_cosdf,	1,1,	"extended",	0,
"k_sindf",	     k_sindf,	1,1,	"extended",	0,
"k_tandf",	     k_tandf,	1,1,	"extended",	0,
"k_atan2df",	     k_atan2df,2,1,	"extended",	0,
#endif
"k_acosf",	     k_acosf,	1,1,	"extended",	0,
"k_acoshf",	     k_acoshf,	1,1,	"extended",	0,
"k_acospif",	     k_acospif,	1,1,	"extended",	0,
"k_aintf",	     k_aintf,	1,1,	"extended",	0,
"k_anintf",	     k_anintf,	1,1,	"extended",	0,
"k_annuityf",	     k_annuityf,2,1,	"extended",	0,
"k_asinf",	     k_asinf,	1,1,	"extended",	0,
"k_asinhf",	     k_asinhf,	1,1,	"extended",	0,
"k_asinpif",	     k_asinpif,	1,1,	"extended",	0,
"k_atanf",	     k_atanf,	1,1,	"extended",	0,
"k_atan2f",	     k_atan2f,	2,1,	"extended",	0,
"k_atan2pif",	     k_atan2pif,2,1,	"extended",	0,
"k_atanhf",	     k_atanhf,	1,1,	"extended",	0,
"k_atanpif",	     k_atanpif,	1,1,	"extended",	0,
"k_cbrtf",	     k_cbrtf,	1,1,	"extended",	0,
"k_ceilf",	     k_ceilf,	1,1,	"extended",	0,
"k_compoundf",	     k_compoundf,2,1,	"extended",	0,
"k_copysignf",	     k_copysignf,2,1,	"extended",	0,
"k_cosf",	     k_cosf,	1,1,	"extended",	0,
"k_coshf",	     k_coshf,	1,1,	"extended",	0,
"k_cospif",	     k_cospif,	1,1,	"extended",	0,
"k_erff",	     k_erff,	1,1,	"extended",	0,
"k_erfcf",	     k_erfcf,	1,1,	"extended",	0,
"k_expf",	     k_expf,	1,1,	"extended",	0,
"k_exp10f",	     k_exp10f,	1,1,	"extended",	0,
"k_exp2f",	     k_exp2f,	1,1,	"extended",	0,
"k_expm1f",	     k_expm1f,	1,1,	"extended",	0,
"k_fabsf",	     k_fabsf,	1,1,	"extended",	0,
"k_finitef",	     k_finitef,	1,1,	"extended",	0,
"k_floorf",	     k_floorf,	1,1,	"extended",	0,
"k_fmodf",	     k_fmodf,	2,1,	"extended",	0,
"k_fp_classf",	     k_fp_classf,1,1,	"extended",	0,
"k_hypotf",	     k_hypotf,	2,1,	"extended",	0,
"k_ilogbf",	     k_ilogbf,	1,1,	"extended",	0,
"k_irintf",	     k_irintf,	1,1,	"extended",	0,
"k_isinff",	     k_isinff,	1,1,	"extended",	0,
"k_isnanf",	     k_isnanf,	1,1,	"extended",	0,
"k_isnormalf",	     k_isnormalf,1,1,	"extended",	0,
"k_issubnormalf",     k_issubnormalf,1,1,"extended",	0,
"k_iszerof",	     k_iszerof,	1,1,	"extended",	0,
"k_j0f",	     k_j0f,	1,1,	"extended",	0,
"k_j1f",	     k_j1f,	1,1,	"extended",	0,
"k_jnf",	     k_jnf,	2,1,	"extended",	0,
"k_logf",	     k_logf,	1,1,	"extended",	0,
"k_log10f",	     k_log10f,	1,1,	"extended",	0,
"k_log1pf",	     k_log1pf,	1,1,	"extended",	0,
"k_log2f",	     k_log2f,	1,1,	"extended",	0,
"k_nextafterf",	     k_nextafterf,2,1,	"extended",	0,
"k_nintf",	     k_nintf,	1,1,	"extended",	0,
"k_powf",	     k_powf,	2,1,	"extended",	0,
#ifdef UCBFLIBTEST
"k_pow_ri",	     k_pow_ri,	2,1,	"extended",	0,
#endif
"k_remainderf",	     k_remainderf,2,1,	"extended",	0,
"k_rintf",	     k_rintf,	1,1,	"extended",	0,
"k_scalbnf",	     k_scalbnf,	2,1,	"extended",	0,
"k_signbitf",	     k_signbitf,1,1,	"extended",	0,
"k_sincosf_c",	     k_sincosf_c,1,1,	"extended",	0,
"k_sincosf_s",	     k_sincosf_s,1,1,	"extended",	0,
"k_sincospif_c",     k_sincospif_c,1,1,	"extended",	0,
"k_sincospif_s",     k_sincospif_s,1,1,	"extended",	0,
"k_sinf",	     k_sinf,	1,1,	"extended",	0,
"k_sinhf",	     k_sinhf,	1,1,	"extended",	0,
"k_sinpif",	     k_sinpif,	1,1,	"extended",	0,
"k_sqrtf",	     k_sqrtf,	1,1,	"extended",	0,
"k_tanf",	     k_tanf,	1,1,	"extended",	0,
"k_tanhf",	     k_tanhf,	1,1,	"extended",	0,
"k_tanpif",	     k_tanpif,	1,1,	"extended",	0,
"k_y0f",	     k_y0f,	1,1,	"extended",	0,
"k_y1f",	     k_y1f,	1,1,	"extended",	0,
"k_ynf",	     k_ynf,	2,1,	"extended",	0,
#endif
};

#define NFUN	(sizeof(kfun)/sizeof(struct fun))

#include <stdio.h>

#ifndef lint
static	char sccsid[] = "@(#)kcvector.c 1.3 91/08/14 SMI";
#endif

/*
 * Copyright (c) 1989 by Sun Microsystems, Inc.
 */

/*
 * Program to verify library functions: see README file for details.
 */

#define SKIP_EX		0	/* skip exception flags test */
#define SKIP_RD		0	/* skip other rounding test */
#define SKIP_NAN_VERIFY	1	/* skip verifying NaN result when invalid occurs */
#define N_OF_PRINT 	100	/* skip exception flags test */
#define NEARBY		10	/* x nearby y if |x-y|<NEARBY in ulp */
#define VERYNEAR	3	/* x verynear y if |x-y|<VERYNEAR in ulp */


#ifdef QP
int iq[NQ];
#endif

int id[2],sizeoffun,skipex,skiprd;
UINT32 arg[8],ans[4],ans_e[4];
static unsigned sub3wc(), neg2wc();

char exce[10],exce_e[10],name[20],rd[4],rel[4],type_out[10],line[90];
int fid;

static (*f)();

#ifdef ANSI_PROTOTYPES
void ucblibtest_(int *pargc,char **argv)
#else
void ucblibtest_(pargc, argv)
int *pargc; char **argv;
#endif
{

    int c,i,j,k,ex,ex_e,skip,fail;
    int totalcount, failcount, passcount, oneid, first, notav;
    int firstid,valueerr,flagserr;
    char *ch,*out;
    UINT32 iz[4],uk,exmask;

	generic_precision();
	significand_length = get_significand();
	switch (significand_length) {
	case 24: break; 
	case 53: break; 
	case 113: break;
	default:	printf(" significand length %ld not supported \n",
			significand_length);
			ucbfail ( __FILE__ , __LINE__ );
	}	

    skipex = SKIP_EX; skiprd = SKIP_RD; skip=notav=0;

    initsetup();	/* setting q0-q3,d0-d1,skipex,skiprd */
	sizeoffun=NFUN;

    totalcount=failcount=passcount=0; oneid=1;
    valueerr=flagserr=0;

    first = 1;
    while((k=scanf("%s",name))!=EOF) {
	skip = 0;

	if(first) {
	    fid = -1;
	    for(i=0;i<sizeoffun;i++) {		/* determine fid */
		if(strcmp(kfun[i].name,name)==0) {fid=i; i=sizeoffun;}
	    }
	    if(fid == -1) skip = 1;		/* skip a line */
	    else {
	        first = 0;
	        firstid = fid;
	    }
	} else {
	    if(strcmp(kfun[firstid].name,name)!=0) {
	        fid = -1;
	        for(i=0;i<sizeoffun;i++) {	/* determine fid */
		    if(strcmp(kfun[i].name,name)==0) {fid=i; i=sizeoffun;}
	        }
		if(fid == -1) {			/* skip a line */
		    skip = 1;		
		} else
		    oneid=0;
	    } else fid = firstid;
	} 

	if(skip) {
	    c = getchar();
	    while(c!='\n'&&c!=EOF) 	/* skip a line */
		c=getchar();
	} else {
   /* read in
    *  rd	round direction as char string "n,z,p,m"
    *  rel	the relation expected between ans[] and ans_e[]
    *  exce_e	expected exception flags "v,o,u,d,x"
    *  arg[]	input arguments
    *  ans_e[] 	expected answer
    */
	fail=0;
	j=scanf("%s %s %s",rd,rel,exce_e);
	for(i=0;i<kfun[fid].in;i++)  
		j+=scanf("%x",&arg[i]);
	for(i=0;i<kfun[fid].out;i++) 
		j+=scanf("%x",&ans_e[i]);
	if(j!=3+kfun[fid].in+kfun[fid].out) {
	    c = getchar();
	    while(c!='\n'&&c!=EOF) 	/* skip a line */
		c=getchar();
	    if(c==EOF)
	    printf("Input fault: please check input at the last line.\n");
	    else
	    printf("Input fault: please check input at line before:\n");
	    i=0;k=80;
	    while(--k>0&&(c=getchar())!=EOF&&c!='\n')
		line[i++]=c;
	    if(c=='\n') line[i++]=c;
	    line[i]='\0';
	    printf("%s\n",line);
	    ucbfail( __FILE__ , __LINE__ );
	    exit(1);
	}

    /* set rounding direction */
	switch (rd[0]) {
	    case 'n': ch = "nearest"; break;
	    case 'z': ch = "tozero"; break;
	    case 'p': ch = "positive"; break;
	    case 'm': ch = "negative"; break;
	    default: printf("rd=%s: no such rounding mode.\n",rd);skip=1;
	}
	if(skiprd==0) ieee_flags("set","direction",ch,&out);
	else if(rd[0]!='n') skip=1;

    /* set input precision */
	if(skip==0) {
	    ch = kfun[fid].prec;
	    ieee_flags("set","precision",ch,&out);
	}

    /* convert exce_e to int value ex_e */
	if(skipex==0) {
	    exmask = 0x1f;
	    i=0; ex_e=0;
	    while((c=exce_e[i++])!='\0') {
		switch(c) {
		    case 'v': ex_e |= 0x10; break;
		    case 'o': ex_e |= 0x8;  break;
		    case 'u': ex_e |= 0x4;  break;
		    case 'd': ex_e |= 0x2;  break;
		    case 'x': ex_e |= 0x1;  break;
		    case '?':
			c=exce_e[i++];
			if(c=='\0') exmask = 0;
			else if(c=='v') exmask ^= 0x10;
			else if(c=='o') exmask ^= 0x8;
			else if(c=='u') exmask ^= 0x4;
			else if(c=='d') exmask ^= 0x2;
			else if(c=='x') exmask ^= 0x1;
	}   }   }

	if(kfun[fid].x_mask==1) exmask &= 0x1e;

    /* call kfun[fid] to compute */
	if(skip==0) {
	    f = (int (*)()) kfun[fid].address;
	    ieee_flags("clear","exception","all",&out);
	    totalcount += 1;
	    
	    k=f();

	    if(k<0) {
		/*
		printf("Function %s is unavailable.\n",name);
		*/
		notav = 1;
		skip = 1;
		totalcount -= 1;
	    }

    /* get exception status in ex */
	    ex = 0;	/* always clear */
	    if((skip|skipex)==0) {
	    ieee_flags("get","exception","invalid",&out);
	    if(strcmp(out,"invalid")==0) ex |= 0x10;
	    ieee_flags("get","exception","overflow",&out);
	    if(strcmp(out,"overflow")==0) ex |= 0x8;
	    ieee_flags("get","exception","underflow",&out);
	    if(strcmp(out,"underflow")==0) ex |= 0x4;
	    ieee_flags("get","exception","division",&out);
	    if(strcmp(out,"division")==0) ex |= 0x2;
	    ieee_flags("get","exception","inexact",&out);
	    if(strcmp(out,"inexact")==0) ex |= 0x1;

	    if((ex&exmask)!=(ex_e&exmask)) fail |= 2;
	    }

    /* compare result */
	if(skip==0) {
	    if(strcmp(rel,"eq")==0||strcmp(rel,"eqz")==0) {
		i=0xffffffff;
		if(strcmp(rel,"eqz")==0) i=0x7fffffff;
		if((ans[0]&i)!=(ans_e[0]&i)) fail |= 1;
		for(i=1;i<kfun[fid].out;i++) if(ans[i]!=ans_e[i]) fail |= 1;
	    }
	    if(strcmp(rel,"uo")==0 && !SKIP_NAN_VERIFY) {
		switch(kfun[fid].out) {
		    case 1: k = 0x7fc00000; break;
		    case 2: k = 0x7ff80000; break;
		    case 4: k = 0x7fff8000; break;
		}
		if((ans[0]&k)!=k) fail |=1;
	    }
	    if(strcmp(rel,"vn")==0||strcmp(rel,"nb")==0) {	/* nearby */
		switch(kfun[fid].out) {
		    case 1: 
			if(ans_e[0]>ans[0]) uk = ans_e[0]-ans[0];
			else uk = ans[0]-ans_e[0];
			break;
		    case 2: 
			c = 0;
			c = sub3wc(&iz[1],ans_e[1],ans[1],c);
			c = sub3wc(&iz[0],ans_e[0],ans[0],c);
			if(iz[0]>=0x80000000) {
			    c = 0;
			    c = neg2wc(&iz[1],iz[1],c);
			    c = neg2wc(&iz[0],iz[0],c);
			}
			if(iz[0]==0) uk = iz[1];
			else uk = 0xffffffff;
			break;
		    case 4: 
			c = 0;
			c = sub3wc(&iz[3],ans_e[3],ans[3],c);
			c = sub3wc(&iz[2],ans_e[2],ans[2],c);
			c = sub3wc(&iz[1],ans_e[1],ans[1],c);
			c = sub3wc(&iz[0],ans_e[0],ans[0],c);
			if(iz[0]>=0x80000000) {
			    c = 0;
			    c = neg2wc(&iz[3],iz[3],c);
			    c = neg2wc(&iz[2],iz[2],c);
			    c = neg2wc(&iz[1],iz[1],c);
			    c = neg2wc(&iz[0],iz[0],c);
			}
			if((iz[0]|iz[1]|iz[2])==0) uk = iz[3];
			else uk = 0xffffffff;
			break;
		}
	        if(strcmp(rel,"vn")==0) 	/* very near */
		    {if(uk>=VERYNEAR) fail |= 1;}
		else
		    {if(uk>=NEARBY) fail |= 1;}
	    }
	    if(strcmp(rel,"ge")==0) {	/* >= */
		i=ans[0];j=ans_e[0]; k=i^j;
		if((i^j)<0) { if(i<0) fail |= 1; }
		else {
		    if(i<0) {k=i;i=j;j=k;}
		    i &= 0x7fffffff; j &= 0x7fffffff;
		    if(i<j) fail |= 1;
		    else if(i==j) {
		    	k = kfun[fid].out;
		    	if(k==2&&ans[1]<ans_e[1]) fail |= 1;
		    	if(k==4) {
			    if(ans[1]<ans_e[1]) fail |= 1;
			    else if(ans_e[1]==ans[1]) {
			        if(ans[2]<ans_e[2]) fail |= 1;
			        else 
				    if(ans_e[2]==ans[2]&&
					ans[3]<ans_e[3]) fail |= 1;
	    }	}    }    }    }
	    if(strcmp(rel,"gt")==0) {	/* > */
		i=ans[0];j=ans_e[0];
		if((i^j)<0) { if(i<0) fail |= 1; }
		else {
		    if(i<0) {k=i;i=j;j=k;}
		    i &= 0x7fffffff; j &= 0x7fffffff;
		    if(i<j) fail |= 1;
		    else if(i==j) {
		    	k = kfun[fid].out;
		        if(k==1) fail |= 1;
		    	if(k==2&&ans[1]<=ans_e[1]) fail |= 1;
		    	if(k==4) {
			    if(ans[1]<ans_e[1]) fail |= 1;
			    else if(ans_e[1]==ans[1]) {
			        if(ans[2]<ans_e[2]) fail |= 1;
			        else 
				    if(ans_e[2]==ans[2]&&
					ans[3]<=ans_e[3]) fail |= 1;
	    }	}    }    }    }
	    if(strcmp(rel,"le")==0) {	/* >= */
		i=ans[0];j=ans_e[0];
		if((i^j)<0) { if(i>0) fail |= 1; }
		else {
		    if(i<0) {k=i;i=j;j=k;}
		    i &= 0x7fffffff; j &= 0x7fffffff;
		    if(i>j) fail |= 1;
		    else if(i==j) {
		    	k = kfun[fid].out;
		    	if(k==2&&ans[1]>ans_e[1]) fail |= 1;
		    	if(k==4) {
			    if(ans[1]>ans_e[1]) fail |= 1;
			    else if(ans_e[1]==ans[1]) {
			        if(ans[2]>ans_e[2]) fail |= 1;
			        else 
				    if(ans_e[2]==ans[2]&&
					ans[3]>ans_e[3]) fail |= 1;
	    }	}    }    }    }
	    if(strcmp(rel,"lt")==0) {	/* > */
		i=ans[0];j=ans_e[0];
		if((i^j)<0) { if(i>0) fail |= 1; }
		else {
		    if(i<0) {k=i;i=j;j=k;}
		    i &= 0x7fffffff; j &= 0x7fffffff;
		    if(i>j) fail |= 1;
		    else if(i==j) {
		    	k = kfun[fid].out;
		        if(k==1) fail |= 1;
		    	if(k==2&&ans[1]>=ans_e[1]) fail |= 1;
		    	if(k==4) {
			    if(ans[1]>ans_e[1]) fail |= 1;
			    else if(ans_e[1]==ans[1]) {
			        if(ans[2]>ans_e[2]) fail |= 1;
			        else 
				    if(ans_e[2]==ans[2]&&
					ans[3]>=ans_e[3]) fail |= 1;
	    }	}    }    }    }
	}

    /* print out result if failed */
	    if(skip==0&&fail>0) {
		failcount += 1;

		valueerr  += fail&1;
		if((fail&1)==0) flagserr  += (fail&2)>>1;
		if(failcount < N_OF_PRINT) {

	    /* convert ex to exce */
		i=0;
		if((ex&0x10)==0x10) exce[i++]='v';
		if((ex&0x8)==0x8)  exce[i++]='o';
		if((ex&0x4)==0x4)  exce[i++]='u';
		if((ex&0x2)==0x2)  exce[i++]='d';
		if((ex&0x1)==0x1)  exce[i++]='x';
		exce[i] = '\0';
		switch(fail) {
		    case 1: printf("Value error: "); break;
		    case 2: printf("Flags error: "); break;
		    case 3: 	/* value and flags, but print value */
			printf("Value error: "); break;
		}
		printf("%s %s %s %s\n",name,rd,rel,exce_e);
		printf("\tInput:   ");
		if(kfun[fid].in>6) {
		    for(i=0;i<4    ;i++) printf(" %08X",arg[i]);
		    printf("\n\t         ");
		    for(i=4;i<kfun[fid].in ;i++) printf(" %08X",arg[i]);
		} else
		    for(i=0;i<kfun[fid].in ;i++) printf(" %08X",arg[i]);
		
		printf("\n\tComputed:");
		for(i=0;i<kfun[fid].out;i++) printf(" %08X",ans[i]);
		printf(" %s\n\tExpected:",exce);
		for(i=0;i<kfun[fid].out;i++) printf(" %08X",ans_e[i]);
		printf(" %s\n",exce_e);
		} else if(failcount == N_OF_PRINT)
		printf("\nToo many error ..... skip printing.\n");
	    }
	}
	}
    }
    passcount = totalcount-failcount;
    if(oneid) {
	if(notav||totalcount==0) 
printf("                 --- not available ---                         %s\n",
		name);
	else {
	    printf("Total %4d tests:  pass %4d,  flags err %4d,  ",
		totalcount,passcount,flagserr);
	    printf("value err% 4d,     %s\n", valueerr,name);
	}
    } else
	printf("Total %4d tests:  pass %4d,  flags err %4d,  value err %4d\n",
		totalcount,passcount,flagserr,valueerr);
    if (failcount != 0) 
		ucbfail( name , __LINE__ );
/* ucbpass( __FILE__ , __LINE__ ); not used because UCBPASS and UCBFAIL could occur in same output file */
    exit(0); 	
    return;
}


static unsigned
sub3wc(z,x,y,carry)
        unsigned *z,x,y,carry;
{                               /*  *z = x - y - carry, set carry; */
        if(carry==0) {
                *z = x-y;
                return (*z>x);
        } else {
                *z = x-y-1;
                return (*z>=x);
        }
}

static unsigned
neg2wc(z,x,carry)
        unsigned *z,x,carry;
{                               /*  *x = 0 - *x - carry, set carry; */
        if(carry==0) {
                *z = -x;
                return ((*z)!=0);
        } else {
                *z = -x-1;
                return 1;
        }
}

#include <math.h>

extern int iq[],id[],skipex,skiprd;
extern UINT32 arg[],ans[];
extern int k_single(),k_single2(),k_single_i(),k_single_ir(),k_single_ri();
extern int k_double(),k_double2(),k_double_i(),k_double_id(),k_double_di();
extern ieee_flags();

/* turn off Sys 5's message */
int matherr(x) struct exception *x; { return 1; }

initsetup()
{
	char *out; int i;
	static double one = 1.0;
	if(*(int*)&one==0) {  /* x86 double,  extended */
	    for(i=0;i<2;i++) id[i] = 1-i;
#ifdef QP
	    for(i=0;i<NQ;i++) iq[i] = 3-i;
#endif
	} else {  /* RISC double, quad */
	    for(i=0;i<2;i++) id[i] = i;
#ifdef QP
	    for(i=0;i<NQ;i++) iq[i] = i;
#endif
	}
	if(skiprd==0) {
	    skiprd = ieee_flags("set","direction","nearest",&out);
	    /*
	    if(skiprd==1) 
		printf("Cannot set rounding mode. Reset skiprd=1.\n");
	    */
	}
	if(skipex==0) {
	    ieee_flags("get","exception","",&out);
	    if(strcmp(out,"not available")==0) {
		skipex=1;
		/*
		printf("Cannot get exception status. Reset skipex=1.\n");
		*/
	    }
	}
	return 0;
}

#ifdef _PLAIN_UNIX
#ifdef ANSI_PROTOTYPES
int ieee_flags (const char *a, const char *b, const char *c, char **d)
#else
int ieee_flags (a, b, c, d)
char *a,*b,*c,**d;
#endif
{
	*d = "not available";
	return 1;
}


/*
 * _PLAIN_UNIX function
 */

#ifdef DP
#ifdef __STDC__
int k_toextd() 	 { return k_double(testtoextd_) ;}
#endif
int k_tointd() 	 { return k_double(testtointd_) ;}
int k_tosingled(){ return k_double(testtosingled_) ;}
int k_todoubled(){ return k_double(testtodoubled_) ;}
int k_addd() 	{ return k_double2(testaddd_) ;}
int k_subd() 	{ return k_double2(testsubd_) ;}
int k_muld() 	{ return k_double2(testmuld_) ;}
int k_divd() 	{ return k_double2(testdivd_) ;}
int k_acosd() 	{ return k_double(testacosd_);}
int k_asind() 	{ return k_double(testasind_) ;}
int k_atand() 	{ return k_double(testatand_) ;}
int k_atan2d() 	{ return k_double2(testatan2d_) ;}
int k_cabsd() 	{
	double x,y,z; 
	struct doublecomplex t;
	INT32 *px = (INT32*)&x, *py = (INT32*)&y, *pz = (INT32*)&z;
	int i;
	for(i=0;i<2;i++) px[id[i]] = arg[i];
	for(i=0;i<2;i++) py[id[i]] = arg[i+2];
	t.real=x; t.imag=y;
	testcabsd_(&z,&t);
	for(i=0;i<2;i++) ans[i] = pz[id[i]];
	return 0;
}
int k_ceild() 	{ return k_double(testceild_) ;} 
int k_cosd() 	{ return k_double(testcosd_) ;}
int k_coshd() 	{ return k_double(testcoshd_) ;}
int k_expd() 	{ return k_double(testexpd_) ;}
int k_fabsd() 	{ return k_double(testabsd_) ;}
int k_floord() 	{ return k_double(testfloord_) ;}
int k_fmodd()	{ return k_double2(testmodd_) ;}
int k_hypotd()	{ return k_double2(testhypotd_) ;}
int k_logd() 	{ return k_double(testlogd_) ;}
int k_log10d() 	{ return k_double(testlog10d_) ;}
int k_powd()	{ return k_double2(testpowd_) ;}
int k_sind() 	{ return k_double(testsind_) ;}
int k_sinhd() 	{ return k_double(testsinhd_) ;}
int k_sqrtd() 	{ return k_double(testsqrtd_) ;}
int k_tand() 	{ return k_double(testtand_) ;}
int k_tanhd() 	{ return k_double(testtanhd_) ;}
#endif
#ifdef SP
#ifdef __STDC__
int k_toexts() 	 { return k_single(testtoexts_) ;}
#endif
int k_toints() 	 { return k_single(testtoints_) ;}
int k_tosingles(){ return k_single(testtosingles_) ;}
int k_todoubles(){ return k_single(testtodoubles_) ;}
int k_adds() 	{ return k_single2(testadds_) ;}
int k_subs() 	{ return k_single2(testsubs_) ;}
int k_muls() 	{ return k_single2(testmuls_) ;}
int k_divs() 	{ return k_single2(testdivs_) ;}
int k_acoss() 	{ return k_single(testacoss_);}
int k_asins() 	{ return k_single(testasins_) ;}
int k_atans() 	{ return k_single(testatans_) ;}
int k_atan2s() 	{ return k_single2(testatan2s_) ;}
int k_cabss() 	{
	float x,y,z; 
	struct singlecomplex t;
	INT32 *px = (INT32*)&x, *py = (INT32*)&y, *pz = (INT32*)&z;
	int i;
	px[0] = arg[0];
	py[0] = arg[1];
	t.real=x; t.imag=y;
	testcabss_(&z,&t);
	ans[0] = pz[0];
	return 0;
}
int k_ceils() 	{ return k_single(testceils_) ;} 
int k_coss() 	{ return k_single(testcoss_) ;}
int k_coshs() 	{ return k_single(testcoshs_) ;}
int k_exps() 	{ return k_single(testexps_) ;}
int k_fabss() 	{ return k_single(testabss_) ;}
int k_floors() 	{ return k_single(testfloors_) ;}
int k_fmods()	{ return k_single2(testmods_) ;}
int k_hypots()	{ return k_single2(testhypots_) ;}
int k_logs() 	{ return k_single(testlogs_) ;}
int k_log10s() 	{ return k_single(testlog10s_) ;}
int k_pows()	{ return k_single2(testpows_) ;}
int k_sins() 	{ return k_single(testsins_) ;}
int k_sinhs() 	{ return k_single(testsinhs_) ;}
int k_sqrts() 	{ return k_single(testsqrts_) ;}
int k_tans() 	{ return k_single(testtans_) ;}
int k_tanhs() 	{ return k_single(testtanhs_) ;}
#endif
#ifdef QP
int k_toextq() 	 { return k_quad(testtoextx_) ;}
int k_tointq() 	 { return k_quad(testtointx_) ;}
int k_tosingleq(){ return k_quad(testtosinglex_) ;}
int k_todoubleq(){ return k_quad(testtodoublex_) ;}
int k_addq() 	{ return k_quad2(testaddx_) ;}
int k_subq() 	{ return k_quad2(testsubx_) ;}
int k_mulq() 	{ return k_quad2(testmulx_) ;}
int k_divq() 	{ return k_quad2(testdivx_) ;}
int k_acosq() 	{ return k_quad(testacosx_);}
int k_asinq() 	{ return k_quad(testasinx_) ;}
int k_atanq() 	{ return k_quad(testatanx_) ;}
int k_atan2q() 	{ return k_quad2(testatan2x_) ;}
int k_cabsq() 	{
	long double x,y,z; 
	struct extendedcomplex t;
	INT32 *px = (INT32*)&x, *py = (INT32*)&y, *pz = (INT32*)&z;
	int i;
	for(i=0;i<NQ;i++) px[iq[i]] = arg[i];
	for(i=0;i<NQ;i++) py[iq[i]] = arg[i+NQ];
	t.real=x; t.imag=y;
	testcabsx_(&z,&t);
	for(i=0;i<NQ;i++) ans[i] = pz[iq[i]];
	return 0;
}
int k_ceilq() 	{ return k_quad(testceilx_) ;} 
int k_cosq() 	{ return k_quad(testcosx_) ;}
int k_coshq() 	{ return k_quad(testcoshx_) ;}
int k_expq() 	{ return k_quad(testexpx_) ;}
int k_fabsq() 	{ return k_quad(testabsx_) ;}
int k_floorq() 	{ return k_quad(testfloorx_) ;}
int k_fmodq()	{ return k_quad2(testmodx_) ;}
int k_hypotq()	{ return k_quad2(testhypotx_) ;}
int k_logq() 	{ return k_quad(testlogx_) ;}
int k_log10q() 	{ return k_quad(testlog10x_) ;}
int k_powq()	{ return k_quad2(testpowx_) ;}
int k_sinq() 	{ return k_quad(testsinx_) ;}
int k_sinhq() 	{ return k_quad(testsinhx_) ;}
int k_sqrtq() 	{ return k_quad(testsqrtx_) ;}
int k_tanq() 	{ return k_quad(testtanx_) ;}
int k_tanhq() 	{ return k_quad(testtanhx_) ;}
#endif

#else

#ifdef SUN_MATH
int k_acosd() 	{ int k_acosd_(); return k_double(k_acosd_) ;}
int k_asind() 	{ int k_asind_(); return k_double(k_asind_) ;}
int k_atand() 	{ int k_atand_(); return k_double(k_atand_) ;}
int k_atan2d()  { int k_atan2d_(); return k_double2(k_atan2d_) ;}
int k_cosd()    { int k_cosd_(); return k_double(k_cosd_) ;}
int k_sind()    { int k_sind_(); return k_double(k_sind_) ;}
int k_tand()    { int k_tand_(); return k_double(k_tand_) ;}
int k_sincosd_c(){ int k_sincosd_c_(); return k_double(k_sincosd_c_) ;}
int k_sincosd_s(){ int k_sincosd_s_(); return k_double(k_sincosd_s_) ;}
#endif

int k_acos()    { int k_acos_(); return k_double(k_acos_);}
int k_acosh() 	{ int k_acosh_(); return k_double(k_acosh_) ;}
int k_acospi() 	{ int k_acospi_(); return k_double(k_acospi_) ;}
int k_add() 	{ int k_add_(); return k_double2(k_add_) ;}
int k_aint() 	{ int k_aint_(); return k_double(k_aint_) ;}
int k_anint() 	{ int k_anint_(); return k_double(k_anint_) ;}
int k_annuity() { int k_annuity_(); return k_double2(k_annuity_) ;}
int k_asin()    { int k_asin_(); return k_double(k_asin_) ;}
int k_asinh() 	{ int k_asinh_(); return k_double(k_asinh_) ;}
int k_asinpi() 	{ int k_asinpi_(); return k_double(k_asinpi_) ;}
int k_atan()    { int k_atan_(); return k_double(k_atan_) ;}
int k_atan2()   { int k_atan2_(); return k_double2(k_atan2_) ;}
int k_atan2pi() { int k_atan2pi_(); return k_double2(k_atan2pi_) ;}
int k_atanh() 	{ int k_atanh_(); return k_double(k_atanh_) ;}
int k_atanpi() 	{ int k_atanpi_(); return k_double(k_atanpi_) ;}
int k_cabs()    {
        double x,y,z;
        int k_cabs_();
        struct { double x, y;} t;
        INT32 *px = (INT32*)&x, *py = (INT32*)&y, *pz = (INT32*)&z;
        int i;
        for(i=0;i<2;i++) px[id[i]] = arg[i];
        for(i=0;i<2;i++) py[id[i]] = arg[i+2];
        t.x=x; t.y=y;
        k_cabs_(&t,&z);
        for(i=0;i<2;i++) ans[i] = pz[id[i]];
        return 0;
}
int k_cbrt() 	{ int k_cbrt_(); return k_double(k_cbrt_) ;}
int k_ceil()    { int k_ceil_(); return k_double(k_ceil_) ;}
int k_compound(){ int k_compound_(); return k_double2(k_compound_) ;}
int k_copysign(){ int k_copysign_(); return k_double2(k_copysign_) ;}
int k_cos()     { int k_cos_(); return k_double(k_cos_) ;}
int k_cosh()    { int k_cosh_(); return k_double(k_cosh_) ;}
int k_cospi() 	{ int k_cospi_(); return k_double(k_cospi_) ;}
int k_div() 	{ int k_div_(); return k_double2(k_div_) ;}
int k_erf() 	{ int k_erf_(); return k_double(k_erf_) ;}
int k_erfc() 	{ int k_erfc_(); return k_double(k_erfc_) ;}
int k_exp()     { int k_exp_(); return k_double(k_exp_) ;}
int k_exp10() 	{ int k_exp10_(); return k_double(k_exp10_) ;}
int k_exp2() 	{ int k_exp2_(); return k_double(k_exp2_) ;}
int k_expm1() 	{ int k_expm1_(); return k_double(k_expm1_) ;}
int k_fabs()    { int k_fabs_(); return k_double(k_fabs_) ;}
int k_finite() 	{ int k_finite_(); return k_double_i(k_finite_) ;}
int k_floor()   { int k_floor_(); return k_double(k_floor_) ;}
int k_fmod()    { int k_fmod_(); return k_double2(k_fmod_) ;}
int k_fp_class(){ int k_fp_class_(); return k_double_i(k_fp_class_) ;}
int k_hypot()   { int k_hypot_(); return k_double2(k_hypot_) ;}
int k_ilogb() 	{ int k_ilogb_(); return k_double_i(k_ilogb_) ;}
int k_irint() 	{ int k_irint_(); return k_double_i(k_irint_) ;}
int k_isinf() 	{ int k_isinf_(); return k_double_i(k_isinf_) ;}
int k_isnan() 	{ int k_isnan_(); return k_double_i(k_isnan_) ;}
int k_isnormal(){ int k_isnormal_(); return k_double_i(k_isnormal_) ;}
int k_issubnormal(){ int k_issubnormal_(); return k_double_i(k_issubnormal_) ;}
int k_iszero() 	{ int k_iszero_(); return k_double_i(k_iszero_) ;}
int k_j0() 	{ int k_j0_(); return k_double(k_j0_) ;}
int k_j1() 	{ int k_j1_(); return k_double(k_j1_) ;}
int k_jn() 	{ int k_jn_(); return k_double_id(k_jn_) ;}
int k_lgamma() 	{ int k_lgamma_(); return k_double(k_lgamma_) ;}
int k_log()     { int k_log_(); return k_double(k_log_) ;}
int k_log10()   { int k_log10_(); return k_double(k_log10_) ;}
int k_log1p() 	{ int k_log1p_(); return k_double(k_log1p_) ;}
int k_log2() 	{ int k_log2_(); return k_double(k_log2_) ;}
int k_logb() 	{ int k_logb_(); return k_double(k_logb_) ;}
int k_mul() 	{ int k_mul_(); return k_double2(k_mul_) ;}
int k_nextafter(){ int k_nextafter_(); return k_double2(k_nextafter_) ;}
int k_nint() 	{ int k_nint_(); return k_double_i(k_nint_) ;}
int k_pow()     { int k_pow_(); return k_double2(k_pow_) ;}
#ifdef UCBFLIBTEST
int k_pow_di() 	{ int k_pow_di_(); return k_double_di(k_pow_di_) ;}
#endif
int k_remainder(){ int k_remainder_(); return k_double2(k_remainder_) ;}
int k_rint() 	{ int k_rint_(); return k_double(k_rint_) ;}
int k_scalb() 	{ int k_scalb_(); return k_double2(k_scalb_) ;}
int k_scalbn() 	{ int k_scalbn_(); return k_double_di(k_scalbn_) ;}
int k_signbit() { int k_signbit_(); return k_double_i(k_signbit_) ;}
int k_sin()     { int k_sin_(); return k_double(k_sin_) ;}
int k_sincos_c(){ int k_sincos_c_(); return k_double(k_sincos_c_) ;}
int k_sincos_s(){ int k_sincos_s_(); return k_double(k_sincos_s_) ;}
int k_sincospi_c(){ int k_sincospi_c_(); return k_double(k_sincospi_c_) ;}
int k_sincospi_s(){ int k_sincospi_s_(); return k_double(k_sincospi_s_) ;}
int k_sinh()    { int k_sinh_(); return k_double(k_sinh_) ;}
int k_sinpi() 	{ int k_sinpi_(); return k_double(k_sinpi_) ;}
int k_sqrt()    { int k_sqrt_(); return k_double(k_sqrt_) ;}
int k_sub() 	{ int k_sub_(); return k_double2(k_sub_) ;}
int k_tan()     { int k_tan_(); return k_double(k_tan_) ;}
int k_tanh()    { int k_tanh_(); return k_double(k_tanh_) ;}
int k_tanpi() 	{ int k_tanpi_(); return k_double(k_tanpi_) ;}
int k_y0() 	{ int k_y0_(); return k_double(k_y0_) ;}
int k_y1() 	{ int k_y1_(); return k_double(k_y1_) ;}
int k_yn() 	{ int k_yn_(); return k_double_id(k_yn_) ;}

/* 
 * for single precision function
 */

#ifdef SUN_MATH
int k_acosdf()	{ int k_acosdf_(); return k_single(k_acosdf_);}
int k_asindf()	{ int k_asindf_(); return k_single(k_asindf_);}
int k_atan2df()	{ int k_atan2df_(); return k_single2(k_atan2df_);}
int k_atandf()	{ int k_atandf_(); return k_single(k_atandf_);}
int k_cosdf()	{ int k_cosdf_(); return k_single(k_cosdf_);}
int k_sindf()	{ int k_sindf_(); return k_single(k_sindf_);}
int k_tandf()	{ int k_tandf_(); return k_single(k_tandf_);}
int k_sincosdf_c(){ int k_sincosdf_c_(); return k_single(k_sincosdf_c_) ;}
int k_sincosdf_s(){ int k_sincosdf_s_(); return k_single(k_sincosdf_s_) ;}
#endif

int k_acosf()	{ int k_acosf_(); return k_single(k_acosf_);}
int k_acoshf()	{ int k_acoshf_(); return k_single(k_acoshf_);}
int k_acospif()	{ int k_acospif_(); return k_single(k_acospif_);}
int k_aintf()	{ int k_aintf_(); return k_single(k_aintf_);}
int k_anintf()	{ int k_anintf_(); return k_single(k_anintf_);}
int k_annuityf(){ int k_annuityf_(); return k_single2(k_annuityf_);}
int k_asinf()	{ int k_asinf_(); return k_single(k_asinf_);}
int k_asinhf()	{ int k_asinhf_(); return k_single(k_asinhf_);}
int k_asinpif()	{ int k_asinpif_(); return k_single(k_asinpif_);}
int k_atanf()	{ int k_atanf_(); return k_single(k_atanf_);}
int k_atan2f()	{ int k_atan2f_(); return k_single2(k_atan2f_);}
int k_atan2pif(){ int k_atan2pif_(); return k_single2(k_atan2pif_);}
int k_atanhf()	{ int k_atanhf_(); return k_single(k_atanhf_);}
int k_atanpif()	{ int k_atanpif_(); return k_single(k_atanpif_);}
int k_cbrtf()	{ int k_cbrtf_(); return k_single(k_cbrtf_);}
int k_ceilf()	{ int k_ceilf_(); return k_single(k_ceilf_);}
int k_fp_classf(){ int k_fp_classf_(); return k_single_i(k_fp_classf_);}
int k_compoundf(){ int k_compoundf_(); return k_single2(k_compoundf_);}
int k_copysignf(){ int k_copysignf_(); return k_single2(k_copysignf_);}
int k_cosf()	{ int k_cosf_(); return k_single(k_cosf_);}
int k_coshf()	{ int k_coshf_(); return k_single(k_coshf_);}
int k_cospif()	{ int k_cospif_(); return k_single(k_cospif_);}
int k_erff()	{ int k_erff_(); return k_single(k_erff_);}
int k_erfcf()	{ int k_erfcf_(); return k_single(k_erfcf_);}
int k_expf()	{ int k_expf_(); return k_single(k_expf_);}
int k_exp10f()	{ int k_exp10f_(); return k_single(k_exp10f_);}
int k_exp2f()	{ int k_exp2f_(); return k_single(k_exp2f_);}
int k_expm1f()	{ int k_expm1f_(); return k_single(k_expm1f_);}
int k_fabsf()	{ int k_fabsf_(); return k_single(k_fabsf_);}
int k_finitef()	{ int k_finitef_(); return k_single_i(k_finitef_);}
int k_floorf()	{ int k_floorf_(); return k_single(k_floorf_);}
int k_fmodf()	{ int k_fmodf_(); return k_single2(k_fmodf_);}
int k_hypotf()	{ int k_hypotf_(); return k_single2(k_hypotf_);}
int k_ilogbf()	{ int k_ilogbf_(); return k_single_i(k_ilogbf_);}
int k_irintf()	{ int k_irintf_(); return k_single_i(k_irintf_);}
int k_isinff()	{ int k_isinff_(); return k_single_i(k_isinff_);}
int k_isnanf()	{ int k_isnanf_(); return k_single_i(k_isnanf_);}
int k_isnormalf(){ int k_isnormalf_(); return k_single_i(k_isnormalf_);}
int k_issubnormalf(){int k_issubnormalf_();return k_single_i(k_issubnormalf_);}
int k_iszerof()	{ int k_iszerof_(); return k_single_i(k_iszerof_);}
int k_j0f()	{ int k_j0f_(); return k_single(k_j0f_);}
int k_j1f()	{ int k_j1f_(); return k_single(k_j1f_);}
int k_jnf()	{ int k_jnf_(); return k_single_ir(k_jnf_);}
int k_logf()	{ int k_logf_(); return k_single(k_logf_);}
int k_log10f()	{ int k_log10f_(); return k_single(k_log10f_);}
int k_log1pf()	{ int k_log1pf_(); return k_single(k_log1pf_);}
int k_log2f()	{ int k_log2f_(); return k_single(k_log2f_);}
int k_nextafterf(){ int k_nextafterf_(); return k_single2(k_nextafterf_);}
int k_nintf()	{ int k_nintf_(); return k_single_i(k_nintf_);}
int k_powf()	{ int k_powf_(); return k_single2(k_powf_);}
#ifdef UCBFLIBTEST
int k_pow_ri()	{ int k_pow_ri_(); return k_single_ri(k_pow_ri_);}
#endif
int k_remainderf(){ int k_remainderf_(); return k_single2(k_remainderf_);}
int k_rintf()	{ int k_rintf_(); return k_single(k_rintf_);}
int k_scalbnf()	{ int k_scalbnf_(); return k_single_ri(k_scalbnf_);}
int k_signbitf(){ int k_signbitf_(); return k_single_i(k_signbitf_);}
int k_sinf()	{ int k_sinf_(); return k_single(k_sinf_);}
int k_sincosf_c(){ int k_sincosf_c_(); return k_single(k_sincosf_c_) ;}
int k_sincosf_s(){ int k_sincosf_s_(); return k_single(k_sincosf_s_) ;}
int k_sincospif_c(){ int k_sincospif_c_(); return k_single(k_sincospif_c_) ;}
int k_sincospif_s(){ int k_sincospif_s_(); return k_single(k_sincospif_s_) ;}
int k_sinhf()	{ int k_sinhf_(); return k_single(k_sinhf_);}
int k_sinpif()	{ int k_sinpif_(); return k_single(k_sinpif_);}
int k_sqrtf()	{ int k_sqrtf_(); return k_single(k_sqrtf_);}
int k_tanf()	{ int k_tanf_(); return k_single(k_tanf_);}
int k_tanhf()	{ int k_tanhf_(); return k_single(k_tanhf_);}
int k_tanpif()	{ int k_tanpif_(); return k_single(k_tanpif_);}
int k_y0f()	{ int k_y0f_(); return k_single(k_y0f_);}
int k_y1f()	{ int k_y1f_(); return k_single(k_y1f_);}
int k_ynf()	{ int k_ynf_(); return k_single_ir(k_ynf_);}
#endif

/*
 * standard function
 */
int k_single(fun)	/* for f->f float function */
int (*fun)();
{
	float x,z;
	*(int*)&x = arg[0];
	fun(&z,&x); 
	ans[0] = *(int*)&z;
	return 0;
}
int k_single2(fun)	/* for f->f float function */
int (*fun)();
{
	float x,y,z;
	*(int*)&x = arg[0];
	*(int*)&y = arg[1];
	fun(&z,&x,&y); 
	ans[0] = *(int*)&z;
	return 0;
}

int k_single_ri(fun)	/* for f->f float function */
int (*fun)();
{
	float x,z;
	int i;
	*(int*)&x = arg[0];
	i = arg[1];
	fun(&z,&x,&i); 
	ans[0] = *(int*)&z;
	return 0;
}

int k_single_ir(fun)	/* for f->f float function */
int (*fun)();
{
	float x,z;
	int i;
	i = arg[0];
	*(int*)&x = arg[1];
	fun(&z,&i,&x); 
	ans[0] = *(int*)&z;
	return 0;
}

int k_single_i(fun)
int (*fun)();
{
	float x; 
	INT32 *px = (INT32*)&x;
	int i;
	px[0] = arg[0];
	fun(&i,&x);
	ans[0] = i;
	return 0;
}

int k_double(fun)
int (*fun)();
{
	double x,z;
	INT32 *px = (INT32*)&x, *pz = (INT32*)&z;
	px[id[0]] = arg[0]; px[id[1]] = arg[1];
	fun(&z,&x);
	ans[0] = pz[id[0]]; ans[1] = pz[id[1]];
	return 0;
}

int k_double2(fun)
int (*fun)();
{
	double x,y,z;
	INT32 *px = (INT32*)&x, *py = (INT32*)&y, *pz = (INT32*)&z;
	px[id[0]] = arg[0]; px[id[1]] = arg[1];
	py[id[0]] = arg[2]; py[id[1]] = arg[3];
	fun(&z,&x,&y);
	ans[0] = pz[id[0]]; ans[1] = pz[id[1]];
	return 0;
}

int k_double_i(fun)
int (*fun)();
{
	double x; 
	INT32 *px = (INT32*)&x;
	int i;
	for(i=0;i<2;i++) px[id[i]] = arg[i];
	fun(&i,&x);
	ans[0] = i;
	/* for testing error, replace the above two lines by: 
	ans[0] = fun(x);	
	*/
	return 0;
}

int k_double_id(fun)
int (*fun)();
{
	double x,z; 
	INT32 *px = (INT32*)&x, *pz = (INT32*)&z;
	int i,n;
	n = arg[0];
	for(i=0;i<2;i++) px[id[i]] = arg[i+1];
	fun(&z,&n,&x);
	for(i=0;i<2;i++) ans[i] = pz[id[i]];
	return 0;
}

int k_double_di(fun)
int (*fun)();
{
	double x,z; 
	INT32 *px = (INT32*)&x, *pz = (INT32*)&z;
	int i,n;
	for(i=0;i<2;i++) px[id[i]] = arg[i];
	n = arg[2];
	fun(&z,&x,&n);
	for(i=0;i<2;i++) ans[i] = pz[id[i]];
	return 0;
}
#ifdef QP
int k_quad(fun)
int (*fun)();
{
	long double x,z; int i;
	INT32 *px = (INT32*)&x, *pz = (INT32*)&z;
	for (i=0 ; i<NQ ; i++) px[iq[i]] = arg[i];
	fun(&z,&x);
	for (i=0 ; i<NQ ; i++) ans[iq[i]] = pz[i];
	return 0;
}

int k_quad2(fun)
int (*fun)();
{
	long double x,y,z; int i;
	INT32 *px = (INT32*)&x, *py = (INT32*)&y, *pz = (INT32*)&z;
	for (i=0 ; i<NQ ; i++) px[iq[i]] = arg[i];
	for (i=0 ; i<NQ ; i++) py[iq[i]] = arg[NQ+i];
	fun(&z,&x,&y);
	for (i=0 ; i<NQ ; i++) ans[iq[i]] = pz[i];
	return 0;
}
#endif
