/*	Not all Fortrans support exit, it seems... */

#ifdef ANSI_PROTOTYPES
void ucbexit_( int *pes );
#else
void ucbexit_( pes ) int *pes ;
#endif
{
exit(*pes);
}
