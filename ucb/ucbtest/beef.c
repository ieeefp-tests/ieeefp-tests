/*
 * Copyright 1/5/95 Zhishun Alex Liu.  All Rights Reserved.
 *
 ***************************
 * WORK IN PROGRESS
 ***************************
 *
 * Written by Zhishun Alex Liu under direction of Professor William Kahan.
 * The author's current electronic mail address as of December 1994 is
 * Alex.Liu@Eng.Sun.COM.
 *
 * Use of this software is granted with the understanding that all recipients
 * should regard themselves as participants in an ongoing research project and
 * hence should feel obligated to report their experiences (good or bad) with
 * this elementary functions test suite to the author.
 */

#include "ucbtest.h"


#ifdef macintosh			/* MPW C 2.02 */
#define	__SEG__ MPW_BeEF
#endif	/* macintosh */
#ifndef lint
static char sccsid[] = "@(#)driver.c	1.2 (BeEF) 1/5/95";
#endif
/*
 * DESCRIPTION:
 * This main program drives the testing of the following functions:
 *	<fid>		<function>		<region implemented>
 *	CASE_SIN	sin(x)			[0, 2 * pi]
 *	CASE_COS	cos(x)			[0, 2 * pi]
 *	CASE_EXP	exp(x)			[-B, B)
 *	CASE_EM1	expm1(x) := exp(x) - 1	[-1, 1)
 *	CASE_LOG	log(x)			[2^-16.5, 2^16.5)
 *	CASE_L1P	log1p(x) := log(1 + x)	[1 / sqrt(2) - 1, sqrt(2) - 1)
 *	CASE_ATN	atan(x)			[0, INF)
 * where
 *  B := (2^(width of exponent - 1) - 2^{floor(lg2(sig_bits)) + 1}) * LN2_EXP
 * and
 *  LN2_EXP := log(2) truncated to 8 bits.
 *
 * Symmetry tests are performed for sin, cos and atan.
 *
 * extern void bgn_all(.), bgn_atn(.), bgn_em1(.), bgn_exp(.), bgn_log(.),
 * bgn_sin(.), double_check(.), dbc_atn(.), dbc_em1(.), dbc_l1p(.), dbc_log(.),
 * dbc_sin(.), gsrand(.), anlyzr(.), title(.), subtitle(.);
 */
#ifdef __STDC__
#include <stdio.h>
extern void exit(int);
#endif
#include "sys.h"
/*
 * POSIX_TIMES takes precedence.
 */
#if POSIX_TIMES && BSD_RUSAGE
#undef BSD_RUSAGE
#define	BSD_RUSAGE	0
#endif

extern int sig_bits;			/* declared and computed in others.c */
static FLOAT brk_sin[] = {		/* # of entries = BRK_SIN + 1 */
    0.0000e+00,
    8.7500e-01,
    2.2500e+00,
    4.0000e+00,
    5.4375e+00,
    7.0000e+00,
};
#define BRK_SIN	(sizeof(brk_sin) / sizeof(brk_sin[0]) - 1)

static struct rational bpq_sin[ ] = {
	{   0, 16},		/*  0:  0.0000e+00 */
	{  14, 16},		/*  1:  8.7500e-01 */
	{  36, 16},		/*  2:  2.2500e+00 */
	{  64, 16},		/*  3:  4.0000e+00 */
	{  87, 16},		/*  4:  5.4375e+00 */
	{ 112, 16},		/*  5:  7.0000e+00 */
};

static FLOAT brk_cos[] = {		/* # of entries = BRK_COS + 1 */
    0.0000e+00,
    7.5000e-01,
    2.4375e+00,
    3.8750e+00,
    5.5625e+00,
    7.0000e+00,
};
#define BRK_COS (sizeof(brk_cos) / sizeof(brk_cos[0]) - 1)

static struct rational bpq_cos[ ] = {
	{   0, 16},		/*  0:  0.0000e+00 */
	{  12, 16},		/*  1:  7.5000e-01 */
	{  39, 16},		/*  2:  2.4375e+00 */
	{  62, 16},		/*  3:  3.8750e+00 */
	{  89, 16},		/*  4:  5.5625e+00 */
	{ 112, 16},		/*  5:  7.0000e+00 */
};

FLOAT brk_em1[] = {	/* # of entries = BRK_EM1 + 1; ALSO USED in er_exp(.) */
    -1.0371093750e+00,		/*  0:-1062/1024 */
    -9.3847656250e-01,		/*  1: -961/1024 */
    -8.3300781250e-01,		/*  2: -853/1024 */
    -7.1777343750e-01,		/*  3: -735/1024 */
    -5.8886718750e-01,		/*  4: -603/1024 */
    -4.3261718750e-01,		/*  5: -443/1024 */
/*  -3.5937500000e-01,		 *  X: -368/1024 */
    -2.6171875000e-01,		/*  6: -268/1024 */
    -1.2500000000e-01,		/*  7: -128/1024 := mid_exp */
     1.4355468750e-01,		/*  8:  147/1024 */
     3.3398437500e-01,		/*  9:  342/1024 */
/*   3.5937500000e-01,		 * XX:  368/1024 */
     5.2148437500e-01,		/* 10:  534/1024 */
     6.7382812500e-01,		/* 11:  690/1024 */
     8.4667968750e-01,		/* 12:  867/1024 */
     1.0087890625e+00,		/* 13: 1033/1024 */
};
#define BRK_EM1 (sizeof(brk_em1) / sizeof(brk_em1[0]) - 1)

static struct rational bpq_em1[ ] = {
	{-1062, 1024},		/*  0: -1.0371093750e+00 */
	{ -961, 1024},		/*  1: -9.3847656250e-01 */
	{ -853, 1024},		/*  2: -8.3300781250e-01 */
	{ -735, 1024},		/*  3: -7.1777343750e-01 */
	{ -603, 1024},		/*  4: -5.8886718750e-01 */
	{ -443, 1024},		/*  5: -4.3261718750e-01 */
	{ -268, 1024},		/*  6: -2.6171875000e-01 */
	{ -128, 1024},		/*  7: -1.2500000000e-01 */
	{  147, 1024},		/*  8:  1.4355468750e-01 */
	{  342, 1024},		/*  9:  3.3398437500e-01 */
	{  534, 1024},		/* 10:  5.2148437500e-01 */
	{  690, 1024},		/* 11:  6.7382812500e-01 */
	{  867, 1024},		/* 12:  8.4667968750e-01 */
	{ 1033, 1024},		/* 13:  1.0087890625e+00 */
};

#if TEST_EXP
int mid_exp = 7;	/* USED in red_exp(.) */
#endif	/* TEST_EXP */

static FLOAT brk_exp[ ] = {
    -3.5937500e-01,		/* 0: -23/064 */
     3.5937500e-01,		/* 1:  23/064 */
     6.9140625e-01,		/* 2: 177/256 */

};
#define	BRK_EXP	(sizeof(brk_exp) / sizeof(brk_exp[0]) - 1)
#define	BGN_EXP	(GENERIC) brk_exp[0]
#define	END_EXP	(GENERIC) brk_exp[1]
#define	LN2_EXP	(GENERIC) brk_exp[2]

static struct rational bpq_exp[ ] = {
	{ -23,  64},		/* 0: -3.5937500e-01 */
	{  23,  64},		/* 1:  3.5937500e-01 */
	{ 177, 256},		/* 2:  6.9140625e-01 */
};

static FLOAT brk_l1p[] = {		/* # of entries = BRK_L1P + 1 */
/*  -2.96875000e-01,	*/
    -2.92893219e-01,			/* slightly less than 1 / sqrt(2) - 1 */
    -2.53906250e-01,
    -2.18750000e-01,
    -1.95312500e-01,
    -1.71875000e-01,
    -1.48437500e-01,
    -1.25000000e-01,
     1.25000000e-01,
     1.40625000e-01,
     1.75781250e-01,
     2.10937500e-01,
     2.46093750e-01,
     2.85156250e-01,
     3.65234375e-01,
     4.14213570e-01,			/* slightly bigger than sqrt(2) - 1 */
/*   4.51171875e-01,	*/
};
#define BRK_L1P (sizeof(brk_l1p) / sizeof(brk_l1p[0]) - 1)

static struct rational bpq_l1p[ ] = {
/*	{ -304, 1024},		    X: -2.9687500000e-01 */
	{    0,    0},		/*  0: -2.9289321900e-01 */
	{ -260, 1024},		/*  1: -2.5390625000e-01 */
	{ -224, 1024},		/*  2: -2.1875000000e-01 */
	{ -200, 1024},		/*  3: -1.9531250000e-01 */
	{ -176, 1024},		/*  4: -1.7187500000e-01 */
	{ -152, 1024},		/*  5: -1.4843750000e-01 */
	{ -128, 1024},		/*  6: -1.2500000000e-01 */
	{  128, 1024},		/*  7:  1.2500000000e-01 */
	{  144, 1024},		/*  8:  1.4062500000e-01 */
	{  180, 1024},		/*  9:  1.7578125000e-01 */
	{  216, 1024},		/* 10:  2.1093750000e-01 */
	{  252, 1024},		/* 11:  2.4609375000e-01 */
	{  292, 1024},		/* 12:  2.8515625000e-01 */
	{  374, 1024},		/* 13:  3.6523437500e-01 */
	{    0,    0},		/* 14:  4.1421357000e-01 */
/*	{  462, 1024},		   XX:  4.5117187500e-01 */
};

static FLOAT brk_atn[] = {	/* # of entries = BRK_ATN + 1 */
	 0.000000000e+00,	/* B:   0/   1 */
	 2.294000000e-01,	/* B:1879/8192 */
	 2.451171875e-01,	/* B: 251/1024 */
	 2.607421875e-01,	/* B: 267/1024 */
	 2.939453125e-01,	/* B: 301/1024 */
	 3.271484375e-01,	/* B: 335/1024 */
	 3.603515625e-01,	/* B: 369/1024 */
	 3.955078125e-01,	/* B: 405/1024 */
	 4.306640625e-01,	/* B: 441/1024 */
	 4.677734375e-01,	/* B: 479/1024 */
	 5.039062500e-01,	/* B: 129/ 256 */
	 5.429687500e-01,	/* B: 139/ 256 */
	 6.035156250e-01,	/* B: 309/ 512 */
	 6.914062500e-01,	/* B: 177/ 256 */
	 7.871093750e-01,	/* B: 403/ 512 */
	 8.906250000e-01,	/* B:  57/  64 */
	 1.007812500e+00,	/* B: 129/ 128 */
	 1.097656250e+00,	/* B: 281/ 256 */
	 1.207031250e+00,	/* B: 309/ 256 */
	 1.347656250e+00,	/* B: 345/ 256 */
	 1.515625000e+00,	/* B:  97/  64 */
	 1.851562500e+00,	/* B: 237/ 128 */
	 2.367187500e+00,	/* B: 303/ 128 */
	 3.203125000e+00,	/* B: 205/  64 */
	 4.625000000e+00,	/* B:  37/   8 */
	 1.012500000e+01,	/* B:  81/   8 */
	 6.553600000e+04,	/* B:   2^16   */
};
#define BRK_ATN (sizeof(brk_atn) / sizeof(brk_atn[0]) - 1)

static struct rational bpq_atn[ ] = {
	{    0, 1024},		/*  0:  0.0000000000e+00 */
	{    0,    0},		/*  1:  2.2940000000e-01 */
	{  251, 1024},		/*  2:  2.4511718750e-01 */
	{  267, 1024},		/*  3:  2.6074218750e-01 */
	{  301, 1024},		/*  4:  2.9394531250e-01 */
	{  335, 1024},		/*  5:  3.2714843750e-01 */
	{  369, 1024},		/*  6:  3.6035156250e-01 */
	{  405, 1024},		/*  7:  3.9550781250e-01 */
	{  441, 1024},		/*  8:  4.3066406250e-01 */
	{  479, 1024},		/*  9:  4.6777343750e-01 */
	{  516, 1024},		/* 10:  5.0390625000e-01 */
	{  556, 1024},		/* 11:  5.4296875000e-01 */
	{  618, 1024},		/* 12:  6.0351562500e-01 */
	{  708, 1024},		/* 13:  6.9140625000e-01 */
	{  806, 1024},		/* 14:  7.8710937500e-01 */
	{  912, 1024},		/* 15:  8.9062500000e-01 */
	{ 1032, 1024},		/* 16:  1.0078125000e+00 */
	{ 1124, 1024},		/* 17:  1.0976562500e+00 */
	{ 1236, 1024},		/* 18:  1.2070312500e+00 */
	{ 1380, 1024},		/* 19:  1.3476562500e+00 */
	{ 1552, 1024},		/* 20:  1.5156250000e+00 */
	{ 1896, 1024},		/* 21:  1.8515625000e+00 */
	{ 2424, 1024},		/* 22:  2.3671875000e+00 */
	{ 3280, 1024},		/* 23:  3.2031250000e+00 */
	{ 4736, 1024},		/* 24:  4.6250000000e+00 */
	{10368, 1024},		/* 25:  1.0125000000e+01 */
	{    0,    0},		/* 26:  6.5536000000e+04 */
};

static FLOAT brk_log[] = {		/* # of entries = BRK_LOG + 1 */
/*   7.031250000e-01,	*/
     7.071067810e-01,			/* slightly less than 1 / sqrt(2) */
     7.460937500e-01,
     7.812500000e-01,
     8.046875000e-01,
     8.281250000e-01,
     8.515625000e-01,
     8.750000000e-01,
     1.125000000e+00,
     1.140625000e+00,
     1.175781250e+00,
     1.210937500e+00,
     1.246093750e+00,
     1.285156250e+00,
     1.365234375e+00,
     1.414213563e+00,			/* slightly bigger than sqrt(2) */
/*   1.451171875e+00,	*/
};
#define BRK_LOG (sizeof(brk_log) / sizeof(brk_log[0]) - 1)

static struct rational bpq_log[ ] = {
/*	{  720, 1024},		    X:  7.0312500000e-01 */
	{    0,    0},		/*  0:  7.0710678100e-01 */
	{  764, 1024},		/*  1:  7.4609375000e-01 */
	{  800, 1024},		/*  2:  7.8125000000e-01 */
	{  824, 1024},		/*  3:  8.0468750000e-01 */
	{  848, 1024},		/*  4:  8.2812500000e-01 */
	{  872, 1024},		/*  5:  8.5156250000e-01 */
	{  896, 1024},		/*  6:  8.7500000000e-01 */
	{ 1152, 1024},		/*  7:  1.1250000000e+00 */
	{ 1168, 1024},		/*  8:  1.1406250000e+00 */
	{ 1204, 1024},		/*  9:  1.1757812500e+00 */
	{ 1240, 1024},		/* 10:  1.2109375000e+00 */
	{ 1276, 1024},		/* 11:  1.2460937500e+00 */
	{ 1316, 1024},		/* 12:  1.2851562500e+00 */
	{ 1398, 1024},		/* 13:  1.3652343750e+00 */
	{    0,    0},		/* 14:  1.4142135630e+00 */
/*	{ 1486, 1024},		   XX:  1.4511718750e+00 */
};

static void
#ifdef __STDC__
dbc_all(void)
#else
dbc_all( )
#endif
{
	int i;

	for (i = 0; i <= BRK_SIN; i++)
		double_check(i, "brk_sin", &bpq_sin[i], &brk_sin[i]);
	for (i = 0; i <= BRK_COS; i++)
		double_check(i, "brk_cos", &bpq_cos[i], &brk_cos[i]);
	for (i = 0; i <= BRK_EM1; i++)
		double_check(i, "brk_em1", &bpq_em1[i], &brk_em1[i]);
	for (i = 0; i <= BRK_EXP; i++)
		double_check(i, "brk_exp", &bpq_exp[i], &brk_exp[i]);
	for (i = 0; i <= BRK_L1P; i++)
		double_check(i, "brk_l1p", &bpq_l1p[i], &brk_l1p[i]);
	for (i = 0; i <= BRK_ATN; i++)
		double_check(i, "brk_atn", &bpq_atn[i], &brk_atn[i]);
	for (i = 0; i <= BRK_LOG; i++)
		double_check(i, "brk_log", &bpq_log[i], &brk_log[i]);
}

#if BSD_RUSAGE
# include <sys/time.h>
# include <sys/resource.h>
# define	msec(t)	((t).tv_sec * 100 + (t).tv_usec / 10000)
#else
# include <time.h>		/* ANSI-C has time(.); POSIX has CLK_TCK */
# if POSIX_TIMES
#  include <sys/times.h>	/* POSIX has times(.) */
# else	/* POSIX_TIMES */
#  ifndef __STDC__
    typedef long time_t;
    extern time_t time PROTO((time_t *));
#  endif
# endif	/* POSIX_TIMES */
#endif	/* BSD_RUSAGE */

static void
#ifdef __STDC__
time_it(char *msg, int state)
#else
time_it(msg, state)
char *msg;
int state;		/* 0: clock begins; 1: clock ends */
#endif
{
#if BSD_RUSAGE
	static struct rusage r0;

	if (!state)
		(void) getrusage(RUSAGE_SELF, &r0);
	else {
		long ut, st;
		struct rusage r1;

		(void) getrusage(RUSAGE_SELF, &r1);
		ut = msec(r1.ru_utime) - msec(r0.ru_utime);
		st = msec(r1.ru_stime) - msec(r0.ru_stime);
		(void) printf("--> Timing {%s}: %ld.%02ldu %ld.%02lds",
			      msg, ut / 100, ut % 100, st / 100, st % 100);
		(void) printf(" %ld+%ldio %ldpf+%ldw\n\f",
			      r1.ru_inblock - r0.ru_inblock,
			      r1.ru_oublock - r0.ru_oublock,
			      r1.ru_majflt - r0.ru_majflt,
			      r1.ru_nswap - r0.ru_nswap);
	}
#else	/* BSD_RUSAGE */
# if POSIX_TIMES
# define sec(t)		((t) / clk_tck)
# define csec(t)	((((t) % clk_tck) * 100 + clk_tck - 1) / clk_tck)
        static struct tms t0;
 
	if (!state)
		(void) times(&t0);
	else {
		clock_t ut, st, clk_tck = CLK_TCK;
		struct tms t1;

		(void) times(&t1);
		ut = t1.tms_utime - t0.tms_utime;	/* 1 / CLK_TCK sec */
		st = t1.tms_stime - t0.tms_stime;	/* 1 / CLK_TCK sec */
		(void) printf("--> Timing {%s}: %ld.%02ldu %ld.%02lds\n\f",
			      msg, sec(ut), csec(ut), sec(st), csec(st));
	}
# else	/* POSIX_TIMES */
	static time_t clock;

	if (!state)
		clock = time((time_t *) 0);
	else
		(void) printf("--> Timing {%s}: %ld seconds (elapsed time)\n\f",
			      msg, (clock = time((time_t *) 0) - clock));
# endif	/* POSIX_TIMES */
#endif	/* BSD_RUSAGE */
}

void
#ifdef __STDC__
ucbeeftest_ (int *pargc, char *argv[])
#else
ucbeeftest_ (pargc, argv)
int *pargc;
char *argv[];
#endif
{
	int ndiv = NDIV, narg = NARG, i = 0;
	PRTYPE pbgn, pend;
	GENERIC gbgn, gend;

#if 0
	int argc = *pargc;
	argc--, argv++;
	if (argc > 0) {
		i = sscanf(*argv, "%d %d", &ndiv, &narg);
		argc--, argv++;
		if (i == 1 && argc)
			i += sscanf(*argv, "%d", &narg);
		if (i < 2)
			(void) printf("--> Malformed command-line arguments.\n");
	}
	if (i < 2)
		(void) scanf("%d %d", &ndiv, &narg);
	if (ndiv <= 0)
		ndiv = NLINES;
	if (narg <= 0)
		narg = 1;
	ndiv = (ndiv + NLINES - 1) / NLINES;
	ndiv *= NLINES;		/* round up to nearest multiple of NLINES */
#endif

	if (nregions <= 0)
		nregions = NLINES;
	nregions = (nregions + NLINES - 1) / NLINES;
	nregions *= NLINES;	/* round up to nearest multiple of NLINES */

#if TEST_LOG
	ntests /= 10;	/* log takes much longer than anything else */
#endif
	ntests /= (10 * nregions);
	if (ntests <= 0)
		ntests = 1;

	ucbstart( __FILE__ , __LINE__ );
	default_precision();  /* a lot doesn't work right with less */
	ndiv=nregions; narg=ntests;

	dbc_all( );
	dbc_atn( );
	dbc_em1( );
	dbc_l1p( );
	dbc_log( );
	dbc_sin( );
	gsrand((unsigned) 1);
	bgn_all( );
	bgn_atn( );
	bgn_em1( );
	bgn_exp( );
	bgn_log( );
	bgn_sin( );

#if TEST_SIN
	time_it((char *) 0, 0);
	title("SIN(X)", narg);
	for (i = 0; i < BRK_SIN; i++) {
		pbgn = (PRTYPE) brk_sin[i];
		pend = (PRTYPE) brk_sin[i + 1];
		subtitle(&pbgn, &pend, ndiv);
		gbgn = (GENERIC) brk_sin[i];
		gend = (GENERIC) brk_sin[i + 1];
		anlyzr(CASE_SIN, i, ndiv, narg, &gbgn, &gend);
	}
	time_it("SIN(X)", 1);
#endif
#if TEST_COS
	time_it((char *) 0, 0);
	title("COS(X)", narg);
	for (i = 0; i < BRK_COS; i++) {
		pbgn = (PRTYPE) brk_cos[i];
		pend = (PRTYPE) brk_cos[i + 1];
		subtitle(&pbgn, &pend, ndiv);
		gbgn = (GENERIC) brk_cos[i];
		gend = (GENERIC) brk_cos[i + 1];
		anlyzr(CASE_COS, i, ndiv, narg, &gbgn, &gend);
	}
	time_it("COS(X)", 1);
#endif
#if TEST_EXP
	{
		int grd_2m = 1, width;

		while (grd_2m <= sig_bits)
			grd_2m += grd_2m;
		if (BITS_2M < grd_2m + 4) {
			(void) printf("%s: 2**BITS_M(%d) < grd_2m(%d) + 4\n",
				      "%%% Anomaly", BITS_M, grd_2m);
			(void) printf("%s: testing of EXP(X) skipped\n\f",
				      "%%% Anomaly");
			goto next;
		}
		width = (BITS_2M - grd_2m) >> 2;
		time_it((char *) 0, 0);
		title("EXP(X)", narg);
		for (i = -(BITS_2M - grd_2m - 1); i <= -(width - 1); i += width) {
			gbgn = LN2_EXP * (GENERIC) i;
			gend = i == -(width - 1) ? BGN_EXP :
				LN2_EXP * (GENERIC) (i + width);
			pbgn = (PRTYPE) gbgn;
			pend = (PRTYPE) gend;
			subtitle(&pbgn, &pend, ndiv);
			anlyzr(CASE_EXP, -1, ndiv, narg, &gbgn, &gend);
		}
		for (i = 5; i < 10; i++) {
			gbgn = i == 5 ? BGN_EXP : (GENERIC) brk_em1[i];
			gend = i == 9 ? END_EXP : (GENERIC) brk_em1[i + 1];
			pbgn = (PRTYPE) gbgn;
			pend = (PRTYPE) gend;
			subtitle(&pbgn, &pend, ndiv);
			anlyzr(CASE_EXP, i, ndiv, narg, &gbgn, &gend);
		}
		for (i = -1; i < BITS_2M - grd_2m - 1; i += width) {
			gbgn = i == -1 ? END_EXP : LN2_EXP * (GENERIC) i;
			gend = LN2_EXP * (GENERIC) (i + width);
			pbgn = (PRTYPE) gbgn;
			pend = (PRTYPE) gend;
			subtitle(&pbgn, &pend, ndiv);
			anlyzr(CASE_EXP, -1, ndiv, narg, &gbgn, &gend);
		}
		time_it("EXP(X)", 1);
	}
next:
#endif
#if TEST_EM1
	time_it((char *) 0, 0);
	title("EXPM1(X)", narg);
	for (i = 0; i < BRK_EM1; i++) {
		pbgn = (PRTYPE) brk_em1[i];
		pend = (PRTYPE) brk_em1[i + 1];
		subtitle(&pbgn, &pend, ndiv);
		gbgn = (GENERIC) brk_em1[i];
		gend = (GENERIC) brk_em1[i + 1];
		anlyzr(CASE_EM1, i, ndiv, narg, &gbgn, &gend);
	}
	time_it("EXPM1(X)", 1);
#endif
#if TEST_L1P
	time_it((char *) 0, 0);
	title("LOG1P(X)", narg);
	for (i = 0; i < BRK_L1P; i++) {
		pbgn = (PRTYPE) brk_l1p[i];
		pend = (PRTYPE) brk_l1p[i + 1];
		subtitle(&pbgn, &pend, ndiv);
		gbgn = (GENERIC) brk_l1p[i];
		gend = (GENERIC) brk_l1p[i + 1];
		anlyzr(CASE_L1P, i, ndiv, narg, &gbgn, &gend);
	}
	time_it("LOG1P(X)", 1);
#endif
#if TEST_ATN
	time_it((char *) 0, 0);
	title("ATAN(X)", narg);
	for (i = 1; i <= BRK_ATN; i++) {
		pbgn = (PRTYPE) brk_atn[i - 1];
		pend = (PRTYPE) brk_atn[i];
		subtitle(&pbgn, &pend, ndiv);
		gbgn = (GENERIC) brk_atn[i - 1];
		gend = (GENERIC) brk_atn[i];
		anlyzr(CASE_ATN, i, ndiv, narg, &gbgn, &gend);
	}
	time_it("ATAN(X)", 1);
#endif
#if TEST_LOG
	time_it((char *) 0, 0);
	title("LOG(X)", narg);
	for (i = 0; i < BRK_LOG; i++) {
		pbgn = (PRTYPE) brk_log[i];
		pend = (PRTYPE) brk_log[i + 1];
		subtitle(&pbgn, &pend, ndiv);
		gbgn = (GENERIC) brk_log[i];
		gend = (GENERIC) brk_log[i + 1];
		anlyzr(CASE_LOG, i, ndiv, narg, &gbgn, &gend);
	}
	time_it("LOG(X)", 1);
#endif
	exit(0);
	/* NOTREACHED */
}
