#ifndef _UCBTEST

#define _UCBTEST

/*	special definitions for Sun test harness	*/

#ifdef SP
#undef SP
#define FLOAT
#endif

#ifdef DP
#undef DP
#define DOUBLE
#endif

#ifdef QP
#undef QP
#define LONGDOUBLE
#endif

#ifndef SUN_IEEE
#if (sunpro_version >= 100) || (sunos_version/100 == 4)
#define SUN_IEEE
#endif
#endif

#ifndef SUN_MATH
#if (sunpro_version >= 300)
#define SUN_MATH
#endif
#endif

/*	Global macro definitions.	*/

#ifdef PC
#define FINT	long		/* C equivalent of Fortran integer */
#define INT32	long		/* 32 bit int */
#define UINT32	unsigned long	/* 32 bit unsigned */
#else
#define FINT	int		/* C equivalent of Fortran integer */
#define INT32	int		/* 32 bit int */
#define UINT32	unsigned int	/* 32 bit unsigned */
#endif

#ifdef SUN_MATH
#include <sunmath.h>
#endif
#include <math.h>

#ifndef NTESTS
#define NTESTS 1000000L   /* DOS wants L specifier added */
#endif

#if defined(__STDC__) || defined(DOS)
#define ANSI_PROTOTYPES
#define PROTO(p)  p
#else
#undef ANSI_PROTOTYPES
#define PROTO(p)  ()
#endif

#ifdef FLOAT
#define SP
#define GENERIC float
#define GENERIC_STRING "float"
#define PRECISION_STRING "single"
#define ABS(X) fabs((double)(X))
#define MOD(X,Y) fmod((double)(X),(double)(Y))
#define CEIL(X) ceil((double)(X))
#define FLOOR(X) floor((double)(X))
#if defined(__STDC__) && !defined(NO_FUNCF)
#define SQRT(X) sqrtf((float)(X))
#define LOG(X) logf((float)(X))
#define SIN(X) sinf((float)(X))
#define COS(X) cosf((float)(X))
#define TAN(X) tanf((float)(X))
#define ASIN(X) asinf((float)(X))
#define ACOS(X) acosf((float)(X))
#define ATAN(X) atanf((float)(X))
#define POW(X,Y) powf((float)(X),(float)(Y))
extern float sqrtf(float);
extern float logf(float);
extern float sinf(float);
extern float cosf(float);
extern float tanf(float);
extern float asinf(float);
extern float acosf(float);
extern float atanf(float);
extern float powf(float,float);
#else
#define SQRT(X) sqrt((double)(X))
#define LOG(X) log((double)(X))
#define SIN(X) sin((double)(X))
#define COS(X) cos((double)(X))
#define TAN(X) tan((double)(X))
#define ASIN(X) asin((double)(X))
#define ACOS(X) acos((double)(X))
#define ATAN(X) atan((double)(X))
#define POW(X,Y) pow((double)(X),(double)(Y))
#endif
#endif

#ifdef DOUBLE
#define DP
#define GENERIC double
#define GENERIC_STRING "double"
#define PRECISION_STRING "double"
#define ABS(X) fabs((double)(X))
#define MOD(X,Y) fmod((double)(X),(double)(Y))
#define CEIL(X) ceil((double)(X))
#define FLOOR(X) floor((double)(X))
#define SQRT(X) sqrt((double)(X))
#define LOG(X) log((double)(X))
#define SIN(X) sin((double)(X))
#define COS(X) cos((double)(X))
#define TAN(X) tan((double)(X))
#define ASIN(X) asin((double)(X))
#define ACOS(X) acos((double)(X))
#define ATAN(X) atan((double)(X))
#define POW(X,Y) pow((double)(X),(double)(Y))
#endif

#ifdef LONGDOUBLE
#define QP
#define GENERIC long double
#define GENERIC_STRING "long double"
#define PRECISION_STRING "extended"
#define ABS(X) fabsl((long double)(X))
#define MOD(X,Y) fmodl((long double)(X),(long double)(Y))
#define CEIL(X) ceill((long double)(X))
#define FLOOR(X) floorl((long double)(X))
#define SQRT(X) sqrtl((long double)(X))
#define LOG(X) logl((long double)(X))
#define SIN(X) sinl((long double)(X))
#define COS(X) cosl((long double)(X))
#define TAN(X) tanl((long double)(X))
#define ASIN(X) asinl((long double)(X))
#define ACOS(X) acosl((long double)(X))
#define ATAN(X) atanl((long double)(X))
#define POW(X,Y) powl((long double)(X),(long double)(Y))
extern long double fabsl(long double);
extern long double fmodl(long double, long double);
extern long double ceill(long double);
extern long double floorl(long double);
extern long double sqrtl(long double);
extern long double logl(long double);
extern long double sinl(long double);
extern long double cosl(long double);
extern long double tanl(long double);
extern long double asinl(long double);
extern long double acosl(long double);
extern long double atanl(long double);
extern long double powl(long double, long double);
#endif

#define TOGENERIC(X) ((GENERIC)(X))
#define ZERO ((GENERIC)(0))
#define ONE  ((GENERIC)(1))
#define TWO  ((GENERIC)(2))
#define FOUR ((GENERIC)(4))

extern UINT32 ntests, nregions;
extern UINT32 significand_length;


/*	IEEE features with machine-dependent syntax 	*/

extern void round_nearest PROTO((void));
extern void round_positive PROTO((void));
extern void round_zero PROTO((void));
extern void generic_precision PROTO((void));
extern void default_precision PROTO((void));

/*	Common ucbtest functions			*/

extern void ucbstart PROTO((char *file,int line));
extern void ucbfail PROTO((char *file,int line));
extern void ucbpass PROTO((char *file,int line));

#include "stdio.h"

#ifdef sunos_version

#if (sunos_version/100 >= 4)
#include "stdlib.h"
#endif

#else
#include "stdlib.h"
#endif

#ifndef __NEWVALID
extern UINT32 get_significand PROTO((void));
extern void ucbdivtest_ PROTO((int *, char **));
extern void ucbmultest_ PROTO((int *, char **));
extern void ucbsqrtest_ PROTO((int *, char **));
extern void ucbfindpitest_ PROTO((int *, char **));
extern void ucbeeftest_ PROTO((int *, char **));
#endif

#endif /* _UCBTEST */

