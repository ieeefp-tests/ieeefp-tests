extern int _lib_version;

#define FE_TONEAREST	0
#define FE_DOWNWARD	1
#define FE_UPWARD	2
#define FE_TOWARDZERO	3

int fesetround(x)
int x;

{
short __fpucontrolword;

__fpucontrolword=(__fpucontrolword &~ 0x0c00) | (x << 10);
}

#define PC_24	0
#define PC_53	2
#define PC_64	3

int fesetprec(x)
int x;

{
short __fpucontrolword;

_lib_version= -1;		/* we wish - but it's const */

__fpucontrolword=(__fpucontrolword &~ 0x0300) | (x << 8) ;
}
