/* Copyright (C) 1988-1994 Sun Microsystems, Inc. 2550 Garcia Avenue */
/* Mountain View, California  94043 All rights reserved. */
/*  */
/* Any person is hereby authorized to download, copy, use, create bug fixes,  */
/* and distribute, subject to the following conditions: */
/*  */
/* 	1.  the software may not be redistributed for a fee except as */
/* 	    reasonable to cover media costs; */
/* 	2.  any copy of the software must include this notice, as well as  */
/* 	    any other embedded copyright notices; and  */
/* 	3.  any distribution of this software or derivative works thereof  */
/* 	    must comply with all applicable U.S. export control laws. */
/*  */
/* THE SOFTWARE IS MADE AVAILABLE "AS IS" AND WITHOUT EXPRESS OR IMPLIED */
/* WARRANTY OF ANY KIND, INCLUDING BUT NOT LIMITED TO THE IMPLIED */
/* WARRANTIES OF DESIGN, MERCHANTIBILITY, FITNESS FOR A PARTICULAR */
/* PURPOSE, NON-INFRINGEMENT, PERFORMANCE OR CONFORMANCE TO */
/* SPECIFICATIONS.   */
/*  */
/* BY DOWNLOADING AND/OR USING THIS SOFTWARE, THE USER WAIVES ALL CLAIMS */
/* AGAINST SUN MICROSYSTEMS, INC. AND ITS AFFILIATED COMPANIES IN ANY */
/* JURISDICTION, INCLUDING BUT NOT LIMITED TO CLAIMS FOR DAMAGES OR */
/* EQUITABLE RELIEF BASED ON LOSS OF DATA, AND SPECIFICALLY WAIVES EVEN */
/* UNKNOWN OR UNANTICIPATED CLAIMS OR LOSSES, PRESENT AND FUTURE. */
/*  */
/* IN NO EVENT WILL SUN MICROSYSTEMS, INC. OR ANY OF ITS AFFILIATED */
/* COMPANIES BE LIABLE FOR ANY LOST REVENUE OR PROFITS OR OTHER SPECIAL, */
/* INDIRECT AND CONSEQUENTIAL DAMAGES, EVEN IF IT HAS BEEN ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGES. */
/*  */
/* This file is provided with no support and without any obligation on the */
/* part of Sun Microsystems, Inc. ("Sun") or any of its affiliated */
/* companies to assist in its use, correction, modification or */
/* enhancement.  Nevertheless, and without creating any obligation on its */
/* part, Sun welcomes your comments concerning the software and requests */
/* that they be sent to fdlibm-comments@sunpro.sun.com. */

	/* 0.7max,0.6max */
cabss n vn x 7f333333 7f19999a 7f6c0535
cabss z vn x 7f333333 7f19999a 7f6c0535
cabss p vn x 7f333333 7f19999a 7f6c0535
cabss m vn x 7f333333 7f19999a 7f6c0535
	/* tiny,huge = huge,tiny = huge */
cabss n eq - 0 7f7fffff 7f7fffff
cabss n eq x 1 7f7fffff 7f7fffff
cabss n eq x 00800000 7f7fffff 7f7fffff
cabss n eq x 3f800000 7f7fffff 7f7fffff
cabss n eq - 7f7fffff 0 7f7fffff
cabss n eq x 7f7fffff 1 7f7fffff
cabss n eq x 7f7fffff 00800000 7f7fffff
cabss n eq x 7f7fffff 3f800000 7f7fffff
	/* pythagoras integers test */
cabss n eq ?x 4afff1ce 46e24000 4afff232
cabss n eq ?x 4afff15e 474ba000 4afff2a2
cabss n eq ?x 4afff0ae 47931000 4afff352
cabss n eq ?x 4affefbe 47c05000 4afff442
cabss n eq ?x 4b000f9d 46351000 4b000fa5
cabss n eq ?x 4b000f7d 4707cc00 4b000fc5
cabss n eq ?x 4b000f3d 47625400 4b001005
cabss n eq ?x 4b000edd 479e6e00 4b001065
cabss n eq ?x 4b000e5d 47cbb200 4b0010e5
cabss n eq ?x 4b002643 45b52000 4b002645
cabss n eq ?x 4b00262b 46e26800 4b00265d
cabss n eq ?x 4b0025f3 474bc400 4b002695
cabss n eq ?x 4b00259b 47932a00 4b0026ed
cabss n eq ?x 4b002523 47c07200 4b002765
cabss n eq ?x 4b003ce5 46353000 4b003ced
cabss n eq ?x 4b003cc5 4707e400 4b003d0d
cabss n eq ?x 4b003c85 47627c00 4b003d4d
cabss n eq ?x 4b003c25 479e8a00 4b003dad
cabss n eq ?x 4b003ba5 47cbd600 4b003e2d
cabss n eq ?x 4b00538f 45b54000 4b005391
	/* radom argument in (-10,10) */
cabss n eq x c02bf92e 40e3e988 40f397e2
cabss n eq x 410cdf38 c0d5b6b8 4130d076
cabss n eq x c02d60c7 bec92cc1 402f3134
cabss n eq x 40b910e7 4108d06a 41252b01
cabss n eq x bfd720d1 c11948aa 411b9ff0
cabss n eq x c0856678 3eeca289 408637b5
cabss n eq x c0f2b200 40e3e5f9 41267653
cabss n eq x c11f6307 c11fd1e7 4161b6a6
cabss n eq x c091b7e9 40ba1219 40ec56cd
cabss n eq x 4037b28f 409dfebc 40b6c0bf
	/* nan's resutls */
cabss n uo v 7f800001 3f800000 0
cabss n uo - ffc00000 3f800000 0
cabss n uo v 7f800001 7fc00000 0
cabss n uo - ffc00000 7fc00000 0
	/* inf result */
cabss n eq - ff800000 7fc00000 7f800000
cabss z eq - fff00001 ff800000 7f800000
	/* inf result with snan argument raise invalid flag */
cabss n eq v ff800000 7f800001 7f800000
cabss n eq v ff800001 7f800000 7f800000
cabss z eq v ff8f0001 ff800000 7f800000
	/* overflow */
cabss n eq ox ff7fffff 7f7eeeee 7f800000
cabss z eq ox ff7fffff 7f7eeeee 7f7fffff
cabss p eq ox ff7fffff 7f7eeeee 7f800000
cabss m eq ox ff7fffff 7f7eeeee 7f7fffff
	/* subnormal number */
cabss n eq - 0 1 1
cabss n eq - 1 0 1
cabss n eq - 00800000 0 00800000
cabss n eq - 0 80800000 00800000
cabss n vn ux 1 1 1
cabss n vn ux 80000002 80000002 3
cabss m eq - 00000000 00000000 00000000
cabss m eq - 00000000 00000001 00000001
cabss m eq - 00000000 007fffff 007fffff
cabss m eq - 00000000 3f800000 3f800000
cabss m eq - 00000000 7f000000 7f000000
cabss m eq - 00000000 7f7fffff 7f7fffff
cabss m eq - 00000000 7f800000 7f800000
cabss m eq - 00000000 80000000 00000000
cabss m eq - 00000000 80000001 00000001
cabss m eq - 00000000 807fffff 007fffff
cabss m eq - 00000000 bf800000 3f800000
cabss m eq - 00000000 ff000000 7f000000
cabss m eq - 00000000 ff7fffff 7f7fffff
cabss m eq - 00000000 ff800000 7f800000
cabss m eq - 00000001 00000000 00000001
cabss m eq - 007fffff 00000000 007fffff
cabss m eq - 3f800000 00000000 3f800000
cabss m eq - 41400000 c0a00000 41500000
cabss m eq - 7f000000 00000000 7f000000
cabss m eq - 7f7fffff 00000000 7f7fffff
cabss m eq - 7f800000 00000000 7f800000
cabss m eq - 7f800000 7fff0000 7f800000
cabss m eq - 7f800000 ffff0000 7f800000
cabss m eq - 7fff0000 7f800000 7f800000
cabss m eq - 7fff0000 ff800000 7f800000
cabss m eq - 80000000 00000000 00000000
cabss m eq - 80000001 00000000 00000001
cabss m eq - 807fffff 00000000 007fffff
cabss m eq - bf800000 00000000 3f800000
cabss m eq - c0400000 c0800000 40a00000
cabss m eq - c1c00000 40e00000 41c80000
cabss m eq - ff000000 00000000 7f000000
cabss m eq - ff7fffff 00000000 7f7fffff
cabss m eq - ff800000 00000000 7f800000
cabss m eq - ff800000 7fff0000 7f800000
cabss m eq - ff800000 ffff0000 7f800000
cabss m eq - ffff0000 7f800000 7f800000
cabss m eq - ffff0000 ff800000 7f800000
cabss m eq v 7f800000 7f810000 7f800000
cabss m eq v 7f800000 ff810000 7f800000
cabss m eq v 7f810000 7f800000 7f800000
cabss m eq v 7f810000 ff800000 7f800000
cabss m eq v ff800000 7f810000 7f800000
cabss m eq v ff800000 ff810000 7f800000
cabss m eq v ff810000 7f800000 7f800000
cabss m eq v ff810000 ff800000 7f800000
cabss m eq xo ff7fffff 7f7eeeee 7f7fffff
cabss m uo - 00000000 7fff0000 7fff0000
cabss m uo - 00000000 ffff0000 7fff0000
cabss m uo - 00000001 7fff0000 7fff0000
cabss m uo - 00000003 7fff0000 7fff0000
cabss m uo - 7fff0000 00000000 7fff0000
cabss m uo - 7fff0000 80000001 7fff0000
cabss m uo - 7fff0000 80000003 7fff0000
cabss m uo - 80000001 ffff0000 7fff0000
cabss m uo - 80000003 ffff0000 7fff0000
cabss m uo - ffff0000 00000000 7fff0000
cabss m uo - ffff0000 00000001 7fff0000
cabss m uo - ffff0000 00000003 7fff0000
cabss m uo v 00000000 7f810000 7fff0000
cabss m uo v 00000000 ff810000 7fff0000
cabss m uo v 00000001 7f810000 7fff0000
cabss m uo v 00000003 7f810000 7fff0000
cabss m uo v 7f810000 00000000 7fff0000
cabss m uo v 7f810000 80000001 7fff0000
cabss m uo v 7f810000 80000003 7fff0000
cabss m uo v 80000001 ff810000 7fff0000
cabss m uo v 80000003 ff810000 7fff0000
cabss m uo v ff810000 00000000 7fff0000
cabss m uo v ff810000 00000001 7fff0000
cabss m uo v ff810000 00000003 7fff0000
cabss n eq - 00000000 00000000 00000000
cabss n eq - 00000000 007fffff 007fffff
cabss n eq - 00000000 3f800000 3f800000
cabss n eq - 00000000 7f000000 7f000000
cabss n eq - 00000000 7f800000 7f800000
cabss n eq - 00000000 80000000 00000000
cabss n eq - 00000000 80000001 00000001
cabss n eq - 00000000 807fffff 007fffff
cabss n eq - 00000000 bf800000 3f800000
cabss n eq - 00000000 ff000000 7f000000
cabss n eq - 00000000 ff7fffff 7f7fffff
cabss n eq - 00000000 ff800000 7f800000
cabss n eq - 007fffff 00000000 007fffff
cabss n eq - 3f800000 00000000 3f800000
cabss n eq - 41400000 c0a00000 41500000
cabss n eq - 7f000000 00000000 7f000000
cabss n eq - 7f800000 00000000 7f800000
cabss n eq - 7f800000 7fff0000 7f800000
cabss n eq - 7f800000 ffff0000 7f800000
cabss n eq - 7fff0000 7f800000 7f800000
cabss n eq - 7fff0000 ff800000 7f800000
cabss n eq - 80000000 00000000 00000000
cabss n eq - 80000001 00000000 00000001
cabss n eq - 807fffff 00000000 007fffff
cabss n eq - bf800000 00000000 3f800000
cabss n eq - c0400000 c0800000 40a00000
cabss n eq - c1c00000 40e00000 41c80000
cabss n eq - ff000000 00000000 7f000000
cabss n eq - ff7fffff 00000000 7f7fffff
cabss n eq - ff800000 00000000 7f800000
cabss n eq - ff800000 7fff0000 7f800000
cabss n eq - ff800000 ffff0000 7f800000
cabss n eq - ffff0000 7f800000 7f800000
cabss n eq - ffff0000 ff800000 7f800000
cabss n eq v 7f800000 7f810000 7f800000
cabss n eq v 7f800000 ff810000 7f800000
cabss n eq v 7f810000 7f800000 7f800000
cabss n eq v 7f810000 ff800000 7f800000
cabss n eq v ff800000 7f810000 7f800000
cabss n eq v ff800000 ff810000 7f800000
cabss n eq v ff810000 7f800000 7f800000
cabss n eq v ff810000 ff800000 7f800000
cabss n eq xo ff7fffff 7f7eeeee 7f800000
cabss n uo - 00000000 7fff0000 7fff0000
cabss n uo - 00000000 ffff0000 7fff0000
cabss n uo - 00000001 7fff0000 7fff0000
cabss n uo - 00000003 7fff0000 7fff0000
cabss n uo - 7fff0000 00000000 7fff0000
cabss n uo - 7fff0000 80000001 7fff0000
cabss n uo - 7fff0000 80000003 7fff0000
cabss n uo - 80000001 ffff0000 7fff0000
cabss n uo - 80000003 ffff0000 7fff0000
cabss n uo - ffc00000 3f800000 00000000
cabss n uo - ffc00000 7fc00000 00000000
cabss n uo - ffff0000 00000000 7fff0000
cabss n uo - ffff0000 00000001 7fff0000
cabss n uo - ffff0000 00000003 7fff0000
cabss n uo v 00000000 7f810000 7fff0000
cabss n uo v 00000000 ff810000 7fff0000
cabss n uo v 00000001 7f810000 7fff0000
cabss n uo v 00000003 7f810000 7fff0000
cabss n uo v 7f800001 3f800000 00000000
cabss n uo v 7f800001 7fc00000 00000000
cabss n uo v 7f810000 00000000 7fff0000
cabss n uo v 7f810000 80000001 7fff0000
cabss n uo v 7f810000 80000003 7fff0000
cabss n uo v 80000001 ff810000 7fff0000
cabss n uo v 80000003 ff810000 7fff0000
cabss n uo v ff810000 00000000 7fff0000
cabss n uo v ff810000 00000001 7fff0000
cabss n uo v ff810000 00000003 7fff0000
cabss n vn xu 00000001 00000001 00000001
cabss n vn xu 80000002 80000002 00000003
cabss p eq - 00000000 00000000 00000000
cabss p eq - 00000000 00000001 00000001
cabss p eq - 00000000 007fffff 007fffff
cabss p eq - 00000000 3f800000 3f800000
cabss p eq - 00000000 7f000000 7f000000
cabss p eq - 00000000 7f7fffff 7f7fffff
cabss p eq - 00000000 7f800000 7f800000
cabss p eq - 00000000 80000000 00000000
cabss p eq - 00000000 80000001 00000001
cabss p eq - 00000000 807fffff 007fffff
cabss p eq - 00000000 bf800000 3f800000
cabss p eq - 00000000 ff000000 7f000000
cabss p eq - 00000000 ff7fffff 7f7fffff
cabss p eq - 00000000 ff800000 7f800000
cabss p eq - 00000001 00000000 00000001
cabss p eq - 007fffff 00000000 007fffff
cabss p eq - 3f800000 00000000 3f800000
cabss p eq - 41400000 c0a00000 41500000
cabss p eq - 7f000000 00000000 7f000000
cabss p eq - 7f7fffff 00000000 7f7fffff
cabss p eq - 7f800000 00000000 7f800000
cabss p eq - 7f800000 7fff0000 7f800000
cabss p eq - 7f800000 ffff0000 7f800000
cabss p eq - 7fff0000 7f800000 7f800000
cabss p eq - 7fff0000 ff800000 7f800000
cabss p eq - 80000000 00000000 00000000
cabss p eq - 80000001 00000000 00000001
cabss p eq - 807fffff 00000000 007fffff
cabss p eq - bf800000 00000000 3f800000
cabss p eq - c0400000 c0800000 40a00000
cabss p eq - c1c00000 40e00000 41c80000
cabss p eq - ff000000 00000000 7f000000
cabss p eq - ff7fffff 00000000 7f7fffff
cabss p eq - ff800000 00000000 7f800000
cabss p eq - ff800000 7fff0000 7f800000
cabss p eq - ff800000 ffff0000 7f800000
cabss p eq - ffff0000 7f800000 7f800000
cabss p eq - ffff0000 ff800000 7f800000
cabss p eq v 7f800000 7f810000 7f800000
cabss p eq v 7f800000 ff810000 7f800000
cabss p eq v 7f810000 7f800000 7f800000
cabss p eq v 7f810000 ff800000 7f800000
cabss p eq v ff800000 7f810000 7f800000
cabss p eq v ff800000 ff810000 7f800000
cabss p eq v ff810000 7f800000 7f800000
cabss p eq v ff810000 ff800000 7f800000
cabss p eq xo ff7fffff 7f7eeeee 7f800000
cabss p uo - 00000000 7fff0000 7fff0000
cabss p uo - 00000000 ffff0000 7fff0000
cabss p uo - 00000001 7fff0000 7fff0000
cabss p uo - 00000003 7fff0000 7fff0000
cabss p uo - 7fff0000 00000000 7fff0000
cabss p uo - 7fff0000 80000001 7fff0000
cabss p uo - 7fff0000 80000003 7fff0000
cabss p uo - 80000001 ffff0000 7fff0000
cabss p uo - 80000003 ffff0000 7fff0000
cabss p uo - ffff0000 00000000 7fff0000
cabss p uo - ffff0000 00000001 7fff0000
cabss p uo - ffff0000 00000003 7fff0000
cabss p uo v 00000000 7f810000 7fff0000
cabss p uo v 00000000 ff810000 7fff0000
cabss p uo v 00000001 7f810000 7fff0000
cabss p uo v 00000003 7f810000 7fff0000
cabss p uo v 7f810000 00000000 7fff0000
cabss p uo v 7f810000 80000001 7fff0000
cabss p uo v 7f810000 80000003 7fff0000
cabss p uo v 80000001 ff810000 7fff0000
cabss p uo v 80000003 ff810000 7fff0000
cabss p uo v ff810000 00000000 7fff0000
cabss p uo v ff810000 00000001 7fff0000
cabss p uo v ff810000 00000003 7fff0000
cabss z eq - 00000000 00000000 00000000
cabss z eq - 00000000 00000001 00000001
cabss z eq - 00000000 007fffff 007fffff
cabss z eq - 00000000 3f800000 3f800000
cabss z eq - 00000000 7f000000 7f000000
cabss z eq - 00000000 7f7fffff 7f7fffff
cabss z eq - 00000000 7f800000 7f800000
cabss z eq - 00000000 80000000 00000000
cabss z eq - 00000000 80000001 00000001
cabss z eq - 00000000 807fffff 007fffff
cabss z eq - 00000000 bf800000 3f800000
cabss z eq - 00000000 ff000000 7f000000
cabss z eq - 00000000 ff7fffff 7f7fffff
cabss z eq - 00000000 ff800000 7f800000
cabss z eq - 00000001 00000000 00000001
cabss z eq - 007fffff 00000000 007fffff
cabss z eq - 3f800000 00000000 3f800000
cabss z eq - 41400000 c0a00000 41500000
cabss z eq - 7f000000 00000000 7f000000
cabss z eq - 7f7fffff 00000000 7f7fffff
cabss z eq - 7f800000 00000000 7f800000
cabss z eq - 7f800000 7fff0000 7f800000
cabss z eq - 7f800000 ffff0000 7f800000
cabss z eq - 7fff0000 7f800000 7f800000
cabss z eq - 7fff0000 ff800000 7f800000
cabss z eq - 80000000 00000000 00000000
cabss z eq - 80000001 00000000 00000001
cabss z eq - 807fffff 00000000 007fffff
cabss z eq - bf800000 00000000 3f800000
cabss z eq - c0400000 c0800000 40a00000
cabss z eq - c1c00000 40e00000 41c80000
cabss z eq - ff000000 00000000 7f000000
cabss z eq - ff7fffff 00000000 7f7fffff
cabss z eq - ff800000 00000000 7f800000
cabss z eq - ff800000 7fff0000 7f800000
cabss z eq - ff800000 ffff0000 7f800000
cabss z eq - ffff0000 7f800000 7f800000
cabss z eq - ffff0000 ff800000 7f800000
cabss z eq v 7f800000 7f810000 7f800000
cabss z eq v 7f800000 ff810000 7f800000
cabss z eq v 7f810000 7f800000 7f800000
cabss z eq v 7f810000 ff800000 7f800000
cabss z eq v ff800000 7f810000 7f800000
cabss z eq v ff800000 ff810000 7f800000
cabss z eq v ff810000 7f800000 7f800000
cabss z eq v ff810000 ff800000 7f800000
cabss z eq xo ff7fffff 7f7eeeee 7f7fffff
cabss z uo - 00000000 7fff0000 7fff0000
cabss z uo - 00000000 ffff0000 7fff0000
cabss z uo - 00000001 7fff0000 7fff0000
cabss z uo - 00000003 7fff0000 7fff0000
cabss z uo - 7fff0000 00000000 7fff0000
cabss z uo - 7fff0000 80000001 7fff0000
cabss z uo - 7fff0000 80000003 7fff0000
cabss z uo - 80000001 ffff0000 7fff0000
cabss z uo - 80000003 ffff0000 7fff0000
cabss z uo - ffff0000 00000000 7fff0000
cabss z uo - ffff0000 00000001 7fff0000
cabss z uo - ffff0000 00000003 7fff0000
cabss z uo v 00000000 7f810000 7fff0000
cabss z uo v 00000000 ff810000 7fff0000
cabss z uo v 00000001 7f810000 7fff0000
cabss z uo v 00000003 7f810000 7fff0000
cabss z uo v 7f810000 00000000 7fff0000
cabss z uo v 7f810000 80000001 7fff0000
cabss z uo v 7f810000 80000003 7fff0000
cabss z uo v 80000001 ff810000 7fff0000
cabss z uo v 80000003 ff810000 7fff0000
cabss z uo v ff810000 00000000 7fff0000
cabss z uo v ff810000 00000001 7fff0000
cabss z uo v ff810000 00000003 7fff0000
