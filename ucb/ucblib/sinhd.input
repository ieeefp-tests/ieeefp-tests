/* Copyright (C) 1988-1994 Sun Microsystems, Inc. 2550 Garcia Avenue */
/* Mountain View, California  94043 All rights reserved. */
/*  */
/* Any person is hereby authorized to download, copy, use, create bug fixes,  */
/* and distribute, subject to the following conditions: */
/*  */
/* 	1.  the software may not be redistributed for a fee except as */
/* 	    reasonable to cover media costs; */
/* 	2.  any copy of the software must include this notice, as well as  */
/* 	    any other embedded copyright notices; and  */
/* 	3.  any distribution of this software or derivative works thereof  */
/* 	    must comply with all applicable U.S. export control laws. */
/*  */
/* THE SOFTWARE IS MADE AVAILABLE "AS IS" AND WITHOUT EXPRESS OR IMPLIED */
/* WARRANTY OF ANY KIND, INCLUDING BUT NOT LIMITED TO THE IMPLIED */
/* WARRANTIES OF DESIGN, MERCHANTIBILITY, FITNESS FOR A PARTICULAR */
/* PURPOSE, NON-INFRINGEMENT, PERFORMANCE OR CONFORMANCE TO */
/* SPECIFICATIONS.   */
/*  */
/* BY DOWNLOADING AND/OR USING THIS SOFTWARE, THE USER WAIVES ALL CLAIMS */
/* AGAINST SUN MICROSYSTEMS, INC. AND ITS AFFILIATED COMPANIES IN ANY */
/* JURISDICTION, INCLUDING BUT NOT LIMITED TO CLAIMS FOR DAMAGES OR */
/* EQUITABLE RELIEF BASED ON LOSS OF DATA, AND SPECIFICALLY WAIVES EVEN */
/* UNKNOWN OR UNANTICIPATED CLAIMS OR LOSSES, PRESENT AND FUTURE. */
/*  */
/* IN NO EVENT WILL SUN MICROSYSTEMS, INC. OR ANY OF ITS AFFILIATED */
/* COMPANIES BE LIABLE FOR ANY LOST REVENUE OR PROFITS OR OTHER SPECIAL, */
/* INDIRECT AND CONSEQUENTIAL DAMAGES, EVEN IF IT HAS BEEN ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGES. */
/*  */
/* This file is provided with no support and without any obligation on the */
/* part of Sun Microsystems, Inc. ("Sun") or any of its affiliated */
/* companies to assist in its use, correction, modification or */
/* enhancement.  Nevertheless, and without creating any obligation on its */
/* part, Sun welcomes your comments concerning the software and requests */
/* that they be sent to fdlibm-comments@sunpro.sun.com. */

	/* sinhd(log(2*max)chopped) is finite, overflow threshold */
sinhd n nb x 408633ce 8fb9f87d 7fefffff fffffd3b
sinhd n nb x c08633ce 8fb9f87d ffefffff fffffd3b
sinhd z nb x 408633ce 8fb9f87d 7fefffff fffffd3b
sinhd z nb x c08633ce 8fb9f87d ffefffff fffffd3b
sinhd p nb x 408633ce 8fb9f87d 7fefffff fffffd3b
sinhd p nb x c08633ce 8fb9f87d ffefffff fffffd3b
sinhd m nb x 408633ce 8fb9f87d 7fefffff fffffd3b
sinhd m nb x c08633ce 8fb9f87d ffefffff fffffd3b
sinhd n nb ox 408633ce 8fb9f87e 7ff00000 0
sinhd n nb ox c08633ce 8fb9f87e fff00000 0
sinhd z nb ox 408633ce 8fb9f87e 7fefffff ffffffff
sinhd z nb ox c08633ce 8fb9f87e ffefffff ffffffff
sinhd p nb ox 408633ce 8fb9f87e 7ff00000 0
sinhd p nb ox c08633ce 8fb9f87e ffefffff ffffffff
sinhd m nb ox 408633ce 8fb9f87e 7fefffff ffffffff
sinhd m nb ox c08633ce 8fb9f87e fff00000 0
	/* sinhd(tiny) :=: tiny */
sinhd n eq x 3bc00000 0 3bc00000 0
sinhd n eq x bbc00000 0 bbc00000 0
sinhd n eq x 00100000 0 00100000 0
sinhd n eq x 80100000 0 80100000 0
sinhd n eq x 1 0 1 0
sinhd n eq x 80000001 0 80000001 0
sinhd n eq x 0 1 0 1
sinhd n eq x 80000000 1 80000000 1
	/* sinhd(+-0) = +-0 */
sinhd n eq - 0 0 0 0
sinhd n eq - 80000000 0 80000000 0
sinhd z eq - 0 0 0 0
sinhd z eq - 80000000 0 80000000 0
sinhd p eq - 0 0 0 0
sinhd p eq - 80000000 0 80000000 0
sinhd m eq - 0 0 0 0
sinhd m eq - 80000000 0 80000000 0
	/* random arguments between -30 30 */
sinhd n nb x c0201f5c b2b5006d c098c286 19d32c08
sinhd n nb x 40355de4 fb825911 41cc5bef 10311486
sinhd n nb x 403a69db 09de7505 42413a2f 3f7db55b
sinhd n nb x c0340920 fba96889 c1adf7c9 0f0e645a
sinhd n nb x c0204112 e27084dd c09a71eb 14c98b30
sinhd n nb x bff2dc32 1b093c41 bff78a9a 1930d4d2
sinhd n nb x 40315995 d18455f5 41705806 5e10a9f3
sinhd n nb x 4039a714 4a51b239 42301981 76da4c49
sinhd n nb x c0142b13 1079de4d c053590d 1df8ba22
sinhd n nb x c03cbda0 3103b871 c2761243 48c6ad56
	/* sinhd(nan) is nan , sinhd(+-inf) is +-inf */
sinhd n uo v 7ff00000 1 0 0
sinhd n uo v 7ff00001 0 0 0
sinhd n uo - 7ff80000 0 0 0
sinhd n eq - 7ff00000 0 7ff00000 0
sinhd n eq - fff00000 0 fff00000 0
sinhd m eq - 7ff00000 00000000 7ff00000 00000000
sinhd m eq - fff00000 00000000 fff00000 00000000
sinhd m eq x 00100000 00000000 00100000 00000000
sinhd m eq x 00100000 00000001 00100000 00000001
sinhd m eq x 00100000 00000002 00100000 00000002
sinhd m eq x 00200000 00000000 00200000 00000000
sinhd m eq x 00300000 00000000 00300000 00000000
sinhd m eq x 3e500000 00000000 3e500000 00000000
sinhd m eq x 3e580000 00000000 3e580000 00000000
sinhd m eq x 80100000 00000000 80100000 00000000
sinhd m eq x 80100000 00000001 80100000 00000001
sinhd m eq x 80100000 00000002 80100000 00000002
sinhd m eq x 80200000 00000000 80200000 00000000
sinhd m eq x 80300000 00000000 80300000 00000000
sinhd m eq x?u 00000000 00000001 00000000 00000001
sinhd m eq x?u 00000000 00000002 00000000 00000002
sinhd m eq x?u 00040000 00000000 00040000 00000000
sinhd m eq x?u 00080000 00000000 00080000 00000000
sinhd m eq x?u 000fffff fffffffe 000fffff fffffffe
sinhd m eq x?u 000fffff ffffffff 000fffff ffffffff
sinhd m eq x?u 80000000 00000001 80000000 00000001
sinhd m eq x?u 80000000 00000002 80000000 00000002
sinhd m eq x?u 80040000 00000000 80040000 00000000
sinhd m eq x?u 80080000 00000000 80080000 00000000
sinhd m eq x?u 800fffff fffffffe 800fffff fffffffe
sinhd m eq x?u 800fffff ffffffff 800fffff ffffffff
sinhd m eq xo 40863400 00000000 7fefffff ffffffff
sinhd m eq xo 7fd00000 00000000 7fefffff ffffffff
sinhd m eq xo 7fe00000 00000000 7fefffff ffffffff
sinhd m eq xo 7fefffff fffffffe 7fefffff ffffffff
sinhd m eq xo 7fefffff ffffffff 7fefffff ffffffff
sinhd m eq xo c0863400 00000000 fff00000 00000000
sinhd m eq xo ffd00000 00000000 fff00000 00000000
sinhd m eq xo ffe00000 00000000 fff00000 00000000
sinhd m eq xo ffefffff fffffffe fff00000 00000000
sinhd m eq xo ffefffff ffffffff fff00000 00000000
sinhd m uo - 7fffe000 00000000 7fffe000 00000000
sinhd m uo - ffffe000 00000000 ffffe000 00000000
sinhd m uo v 7ff02000 00000000 7fffe000 00000000
sinhd m uo v fff02000 00000000 ffffe000 00000000
sinhd m vn x 3e600000 00000000 3e600000 00000001
sinhd m vn x 3e640000 00000000 3e640000 00000001
sinhd m vn x be500000 00000000 be500000 00000000
sinhd m vn x be580000 00000000 be580000 00000001
sinhd m vn x be600000 00000000 be600000 00000001
sinhd m vn x be640000 00000000 be640000 00000001
sinhd m vn xo 408633ce 8fb9f87e 7fefffff ffffffff
sinhd m vn xo c08633ce 8fb9f87e fff00000 00000000
sinhd n eq x 00100000 00000001 00100000 00000001
sinhd n eq x 00100000 00000002 00100000 00000002
sinhd n eq x 00200000 00000000 00200000 00000000
sinhd n eq x 00300000 00000000 00300000 00000000
sinhd n eq x 3e500000 00000000 3e500000 00000000
sinhd n eq x 3e600000 00000000 3e600000 00000001
sinhd n eq x 3e640000 00000000 3e640000 00000001
sinhd n eq x 80100000 00000001 80100000 00000001
sinhd n eq x 80100000 00000002 80100000 00000002
sinhd n eq x 80200000 00000000 80200000 00000000
sinhd n eq x 80300000 00000000 80300000 00000000
sinhd n eq x be500000 00000000 be500000 00000000
sinhd n eq x be600000 00000000 be600000 00000001
sinhd n eq x be640000 00000000 be640000 00000001
sinhd n eq x?u 00000000 00000002 00000000 00000002
sinhd n eq x?u 00040000 00000000 00040000 00000000
sinhd n eq x?u 00080000 00000000 00080000 00000000
sinhd n eq x?u 000fffff fffffffe 000fffff fffffffe
sinhd n eq x?u 000fffff ffffffff 000fffff ffffffff
sinhd n eq x?u 80000000 00000002 80000000 00000002
sinhd n eq x?u 80040000 00000000 80040000 00000000
sinhd n eq x?u 80080000 00000000 80080000 00000000
sinhd n eq x?u 800fffff fffffffe 800fffff fffffffe
sinhd n eq x?u 800fffff ffffffff 800fffff ffffffff
sinhd n eq xo 40863400 00000000 7ff00000 00000000
sinhd n eq xo 7fd00000 00000000 7ff00000 00000000
sinhd n eq xo 7fe00000 00000000 7ff00000 00000000
sinhd n eq xo 7fefffff fffffffe 7ff00000 00000000
sinhd n eq xo 7fefffff ffffffff 7ff00000 00000000
sinhd n eq xo c0863400 00000000 fff00000 00000000
sinhd n eq xo ffd00000 00000000 fff00000 00000000
sinhd n eq xo ffe00000 00000000 fff00000 00000000
sinhd n eq xo ffefffff fffffffe fff00000 00000000
sinhd n eq xo ffefffff ffffffff fff00000 00000000
sinhd n uo - 7ff80000 00000000 00000000 00000000
sinhd n uo - 7fffe000 00000000 7fffe000 00000000
sinhd n uo - ffffe000 00000000 ffffe000 00000000
sinhd n uo v 7ff00000 00000001 00000000 00000000
sinhd n uo v 7ff00001 00000000 00000000 00000000
sinhd n uo v 7ff02000 00000000 7fffe000 00000000
sinhd n uo v fff02000 00000000 ffffe000 00000000
sinhd n vn xo 408633ce 8fb9f87e 7ff00000 00000000
sinhd n vn xo c08633ce 8fb9f87e fff00000 00000000
sinhd p eq - 7ff00000 00000000 7ff00000 00000000
sinhd p eq - fff00000 00000000 fff00000 00000000
sinhd p eq x 3e580000 00000000 3e580000 00000001
sinhd p eq x 80100000 00000000 80100000 00000000
sinhd p eq x 80100000 00000001 80100000 00000001
sinhd p eq x 80100000 00000002 80100000 00000002
sinhd p eq x 80200000 00000000 80200000 00000000
sinhd p eq x 80300000 00000000 80300000 00000000
sinhd p eq x?u 80000000 00000001 80000000 00000001
sinhd p eq x?u 80000000 00000002 80000000 00000002
sinhd p eq x?u 80040000 00000000 80040000 00000000
sinhd p eq x?u 80080000 00000000 80080000 00000000
sinhd p eq x?u 800fffff fffffffe 800fffff fffffffe
sinhd p eq x?u 800fffff ffffffff 800fffff ffffffff
sinhd p eq xo 40863400 00000000 7ff00000 00000000
sinhd p eq xo 7fd00000 00000000 7ff00000 00000000
sinhd p eq xo 7fe00000 00000000 7ff00000 00000000
sinhd p eq xo 7fefffff fffffffe 7ff00000 00000000
sinhd p eq xo 7fefffff ffffffff 7ff00000 00000000
sinhd p eq xo c0863400 00000000 ffefffff ffffffff
sinhd p eq xo ffd00000 00000000 ffefffff ffffffff
sinhd p eq xo ffe00000 00000000 ffefffff ffffffff
sinhd p eq xo ffefffff fffffffe ffefffff ffffffff
sinhd p eq xo ffefffff ffffffff ffefffff ffffffff
sinhd p uo - 7fffe000 00000000 7fffe000 00000000
sinhd p uo - ffffe000 00000000 ffffe000 00000000
sinhd p uo v 7ff02000 00000000 7fffe000 00000000
sinhd p uo v fff02000 00000000 ffffe000 00000000
sinhd p vn x 00100000 00000000 00100000 00000000
sinhd p vn x 00100000 00000001 00100000 00000001
sinhd p vn x 00100000 00000002 00100000 00000002
sinhd p vn x 00200000 00000000 00200000 00000000
sinhd p vn x 00300000 00000000 00300000 00000000
sinhd p vn x 3e500000 00000000 3e500000 00000000
sinhd p vn x 3e600000 00000000 3e600000 00000001
sinhd p vn x 3e640000 00000000 3e640000 00000001
sinhd p vn x be500000 00000000 be500000 00000000
sinhd p vn x be580000 00000000 be580000 00000000
sinhd p vn x be600000 00000000 be600000 00000001
sinhd p vn x be640000 00000000 be640000 00000001
sinhd p vn x?u 00000000 00000001 00000000 00000001
sinhd p vn x?u 00000000 00000002 00000000 00000002
sinhd p vn x?u 00040000 00000000 00040000 00000000
sinhd p vn x?u 00080000 00000000 00080000 00000000
sinhd p vn x?u 000fffff fffffffe 000fffff fffffffe
sinhd p vn x?u 000fffff ffffffff 000fffff ffffffff
sinhd p vn xo 408633ce 8fb9f87e 7ff00000 00000000
sinhd p vn xo c08633ce 8fb9f87e ffefffff ffffffff
sinhd z eq - 7ff00000 00000000 7ff00000 00000000
sinhd z eq - fff00000 00000000 fff00000 00000000
sinhd z eq x 00100000 00000000 00100000 00000000
sinhd z eq x 00100000 00000001 00100000 00000001
sinhd z eq x 00100000 00000002 00100000 00000002
sinhd z eq x 00200000 00000000 00200000 00000000
sinhd z eq x 00300000 00000000 00300000 00000000
sinhd z eq x 3e500000 00000000 3e500000 00000000
sinhd z eq x 3e580000 00000000 3e580000 00000000
sinhd z eq x 80100000 00000000 80100000 00000000
sinhd z eq x 80100000 00000001 80100000 00000001
sinhd z eq x 80100000 00000002 80100000 00000002
sinhd z eq x 80200000 00000000 80200000 00000000
sinhd z eq x 80300000 00000000 80300000 00000000
sinhd z eq x?u 00000000 00000001 00000000 00000001
sinhd z eq x?u 00000000 00000002 00000000 00000002
sinhd z eq x?u 00040000 00000000 00040000 00000000
sinhd z eq x?u 00080000 00000000 00080000 00000000
sinhd z eq x?u 000fffff fffffffe 000fffff fffffffe
sinhd z eq x?u 000fffff ffffffff 000fffff ffffffff
sinhd z eq x?u 80000000 00000001 80000000 00000001
sinhd z eq x?u 80000000 00000002 80000000 00000002
sinhd z eq x?u 80040000 00000000 80040000 00000000
sinhd z eq x?u 80080000 00000000 80080000 00000000
sinhd z eq x?u 800fffff fffffffe 800fffff fffffffe
sinhd z eq x?u 800fffff ffffffff 800fffff ffffffff
sinhd z eq xo 40863400 00000000 7fefffff ffffffff
sinhd z eq xo 7fd00000 00000000 7fefffff ffffffff
sinhd z eq xo 7fe00000 00000000 7fefffff ffffffff
sinhd z eq xo 7fefffff fffffffe 7fefffff ffffffff
sinhd z eq xo 7fefffff ffffffff 7fefffff ffffffff
sinhd z eq xo c0863400 00000000 ffefffff ffffffff
sinhd z eq xo ffd00000 00000000 ffefffff ffffffff
sinhd z eq xo ffe00000 00000000 ffefffff ffffffff
sinhd z eq xo ffefffff fffffffe ffefffff ffffffff
sinhd z eq xo ffefffff ffffffff ffefffff ffffffff
sinhd z uo - 7fffe000 00000000 7fffe000 00000000
sinhd z uo - ffffe000 00000000 ffffe000 00000000
sinhd z uo v 7ff02000 00000000 7fffe000 00000000
sinhd z uo v fff02000 00000000 ffffe000 00000000
sinhd z vn x 3e600000 00000000 3e600000 00000001
sinhd z vn x 3e640000 00000000 3e640000 00000001
sinhd z vn x be500000 00000000 be500000 00000000
sinhd z vn x be580000 00000000 be580000 00000000
sinhd z vn x be600000 00000000 be600000 00000001
sinhd z vn x be640000 00000000 be640000 00000001
sinhd z vn xo 408633ce 8fb9f87e 7fefffff ffffffff
sinhd z vn xo c08633ce 8fb9f87e ffefffff ffffffff
