/* Copyright (C) 1988-1994 Sun Microsystems, Inc. 2550 Garcia Avenue */
/* Mountain View, California  94043 All rights reserved. */
/*  */
/* Any person is hereby authorized to download, copy, use, create bug fixes,  */
/* and distribute, subject to the following conditions: */
/*  */
/* 	1.  the software may not be redistributed for a fee except as */
/* 	    reasonable to cover media costs; */
/* 	2.  any copy of the software must include this notice, as well as  */
/* 	    any other embedded copyright notices; and  */
/* 	3.  any distribution of this software or derivative works thereof  */
/* 	    must comply with all applicable U.S. export control laws. */
/*  */
/* THE SOFTWARE IS MADE AVAILABLE "AS IS" AND WITHOUT EXPRESS OR IMPLIED */
/* WARRANTY OF ANY KIND, INCLUDING BUT NOT LIMITED TO THE IMPLIED */
/* WARRANTIES OF DESIGN, MERCHANTIBILITY, FITNESS FOR A PARTICULAR */
/* PURPOSE, NON-INFRINGEMENT, PERFORMANCE OR CONFORMANCE TO */
/* SPECIFICATIONS.   */
/*  */
/* BY DOWNLOADING AND/OR USING THIS SOFTWARE, THE USER WAIVES ALL CLAIMS */
/* AGAINST SUN MICROSYSTEMS, INC. AND ITS AFFILIATED COMPANIES IN ANY */
/* JURISDICTION, INCLUDING BUT NOT LIMITED TO CLAIMS FOR DAMAGES OR */
/* EQUITABLE RELIEF BASED ON LOSS OF DATA, AND SPECIFICALLY WAIVES EVEN */
/* UNKNOWN OR UNANTICIPATED CLAIMS OR LOSSES, PRESENT AND FUTURE. */
/*  */
/* IN NO EVENT WILL SUN MICROSYSTEMS, INC. OR ANY OF ITS AFFILIATED */
/* COMPANIES BE LIABLE FOR ANY LOST REVENUE OR PROFITS OR OTHER SPECIAL, */
/* INDIRECT AND CONSEQUENTIAL DAMAGES, EVEN IF IT HAS BEEN ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGES. */
/*  */
/* This file is provided with no support and without any obligation on the */
/* part of Sun Microsystems, Inc. ("Sun") or any of its affiliated */
/* companies to assist in its use, correction, modification or */
/* enhancement.  Nevertheless, and without creating any obligation on its */
/* part, Sun welcomes your comments concerning the software and requests */
/* that they be sent to fdlibm-comments@sunpro.sun.com. */

	/* sind(+-max) */ 
sind n le x 7fefffff ffffffff 3ff00000 0
sind n ge x 7fefffff ffffffff bff00000 0
sind z le x 7fefffff ffffffff 3ff00000 0
sind z ge x 7fefffff ffffffff bff00000 0
sind p le x 7fefffff ffffffff 3ff00000 0
sind p ge x 7fefffff ffffffff bff00000 0
sind m le x 7fefffff ffffffff 3ff00000 0
sind m ge x 7fefffff ffffffff bff00000 0
	/* sind(tiny) is tiny */
sind n eq x 3e100000 0 3e100000 0
sind n eq x be100000 0 be100000 0
sind n eq x 00100000 0 00100000 0
sind n eq x 80100000 0 80100000 0
sind n eq x 0 1 0 1
sind n eq x 80000000 1 80000000 1
sind n eq - 0 0 0 0
sind n eq - 80000000 0 80000000 0
sind z eq - 0 0 0 0
sind z eq - 80000000 0 80000000 0
sind p eq - 0 0 0 0
sind p eq - 80000000 0 80000000 0
sind m eq - 0 0 0 0
sind m eq - 80000000 0 80000000 0
	/* sind(nan or inf) is nan */
sind n uo v 7ff00000 0 0 0
sind n uo v fff00000 0 0 0
sind n uo v 7ff00000 1 0 0
sind n uo v fff00000 1 0 0
sind n uo - 7ff80000 0 0 0
sind m eq x 00100000 00000001 00100000 00000001
sind m eq x 00100000 00000002 00100000 00000002
sind m eq x 00200000 00000000 00200000 00000000
sind m eq x 00300000 00000000 00300000 00000000
sind m eq x 80100000 00000001 80100000 00000001
sind m eq x 80100000 00000002 80100000 00000002
sind m eq x 801fffff fffffffb 801fffff fffffffb
sind m eq x 80200000 00000000 80200000 00000000
sind m eq x 80200000 00000003 80200000 00000003
sind m eq x 80300000 00000000 80300000 00000000
sind m eq x be480000 00000000 be480000 00000000
sind m eq x be500000 00000000 be500000 00000000
sind m eq x?u 00000000 00000001 00000000 00000001
sind m eq x?u 00000000 00000002 00000000 00000002
sind m eq x?u 00040000 00000000 00040000 00000000
sind m eq x?u 00080000 00000000 00080000 00000000
sind m eq x?u 000fffff fffffffe 000fffff fffffffe
sind m eq x?u 000fffff ffffffff 000fffff ffffffff
sind m eq x?u 00100000 00000000 00100000 00000000
sind m eq x?u 80000000 00000001 80000000 00000001
sind m eq x?u 80000000 00000002 80000000 00000002
sind m eq x?u 80000000 00000009 80000000 00000009
sind m eq x?u 80040000 00000000 80040000 00000000
sind m eq x?u 80080000 00000000 80080000 00000000
sind m eq x?u 800fffff fffffffe 800fffff fffffffe
sind m eq x?u 800fffff ffffffff 800fffff ffffffff
sind m eq x?u 80100000 00000000 80100000 00000000
sind m uo - 7fffe000 00000000 7fffe000 00000000
sind m uo - ffffe000 00000000 ffffe000 00000000
sind m uo v 7ff00000 00000000 7fffe000 00000000
sind m uo v 7ff02000 00000000 7fffe000 00000000
sind m uo v fff00000 00000000 7fffe000 00000000
sind m uo v fff02000 00000000 ffffe000 00000000
sind m vn x 3e480000 00000000 3e480000 00000000
sind m vn x 3e500000 00000000 3e500000 00000000
sind n eq x 00100000 00000001 00100000 00000001
sind n eq x 00100000 00000002 00100000 00000002
sind n eq x 001fffff fffffffb 001fffff fffffffb
sind n eq x 00200000 00000000 00200000 00000000
sind n eq x 00200000 00000003 00200000 00000003
sind n eq x 00300000 00000000 00300000 00000000
sind n eq x 3e480000 00000000 3e480000 00000000
sind n eq x 3e500000 00000000 3e500000 00000000
sind n eq x 80100000 00000001 80100000 00000001
sind n eq x 80100000 00000002 80100000 00000002
sind n eq x 801fffff fffffffb 801fffff fffffffb
sind n eq x 80200000 00000000 80200000 00000000
sind n eq x 80200000 00000003 80200000 00000003
sind n eq x 80300000 00000000 80300000 00000000
sind n eq x be480000 00000000 be480000 00000000
sind n eq x be500000 00000000 be500000 00000000
sind n eq x?u 00000000 00000002 00000000 00000002
sind n eq x?u 00000000 00000009 00000000 00000009
sind n eq x?u 00040000 00000000 00040000 00000000
sind n eq x?u 00080000 00000000 00080000 00000000
sind n eq x?u 000fffff fffffffe 000fffff fffffffe
sind n eq x?u 000fffff ffffffff 000fffff ffffffff
sind n eq x?u 80000000 00000002 80000000 00000002
sind n eq x?u 80000000 00000009 80000000 00000009
sind n eq x?u 80040000 00000000 80040000 00000000
sind n eq x?u 80080000 00000000 80080000 00000000
sind n eq x?u 800fffff fffffffe 800fffff fffffffe
sind n eq x?u 800fffff ffffffff 800fffff ffffffff
sind n uo - 7ff80000 00000000 00000000 00000000
sind n uo - 7fffe000 00000000 7fffe000 00000000
sind n uo - ffffe000 00000000 ffffe000 00000000
sind n uo v 7ff00000 00000000 00000000 00000000
sind n uo v 7ff00000 00000001 00000000 00000000
sind n uo v 7ff02000 00000000 7fffe000 00000000
sind n uo v fff00000 00000000 00000000 00000000
sind n uo v fff00000 00000001 00000000 00000000
sind n uo v fff02000 00000000 ffffe000 00000000
sind p eq x 00100000 00000001 00100000 00000001
sind p eq x 00100000 00000002 00100000 00000002
sind p eq x 001fffff fffffffb 001fffff fffffffb
sind p eq x 00200000 00000000 00200000 00000000
sind p eq x 00200000 00000003 00200000 00000003
sind p eq x 00300000 00000000 00300000 00000000
sind p eq x 3e480000 00000000 3e480000 00000000
sind p eq x 3e500000 00000000 3e500000 00000000
sind p eq x 80100000 00000001 80100000 00000001
sind p eq x 80100000 00000002 80100000 00000002
sind p eq x 80200000 00000000 80200000 00000000
sind p eq x 80300000 00000000 80300000 00000000
sind p eq x?u 00000000 00000001 00000000 00000001
sind p eq x?u 00000000 00000002 00000000 00000002
sind p eq x?u 00000000 00000009 00000000 00000009
sind p eq x?u 00040000 00000000 00040000 00000000
sind p eq x?u 00080000 00000000 00080000 00000000
sind p eq x?u 000fffff fffffffe 000fffff fffffffe
sind p eq x?u 000fffff ffffffff 000fffff ffffffff
sind p eq x?u 00100000 00000000 00100000 00000000
sind p eq x?u 80000000 00000001 80000000 00000001
sind p eq x?u 80000000 00000002 80000000 00000002
sind p eq x?u 80040000 00000000 80040000 00000000
sind p eq x?u 80080000 00000000 80080000 00000000
sind p eq x?u 800fffff fffffffe 800fffff fffffffe
sind p eq x?u 800fffff ffffffff 800fffff ffffffff
sind p eq x?u 80100000 00000000 80100000 00000000
sind p uo - 7fffe000 00000000 7fffe000 00000000
sind p uo - ffffe000 00000000 ffffe000 00000000
sind p uo v 7ff00000 00000000 7fffe000 00000000
sind p uo v 7ff02000 00000000 7fffe000 00000000
sind p uo v fff00000 00000000 7fffe000 00000000
sind p uo v fff02000 00000000 ffffe000 00000000
sind p vn x be480000 00000000 be480000 00000000
sind p vn x be500000 00000000 be500000 00000000
sind z eq x 00100000 00000001 00100000 00000001
sind z eq x 00100000 00000002 00100000 00000002
sind z eq x 00200000 00000000 00200000 00000000
sind z eq x 00300000 00000000 00300000 00000000
sind z eq x 80100000 00000001 80100000 00000001
sind z eq x 80100000 00000002 80100000 00000002
sind z eq x 80200000 00000000 80200000 00000000
sind z eq x 80300000 00000000 80300000 00000000
sind z eq x?u 00000000 00000001 00000000 00000001
sind z eq x?u 00000000 00000002 00000000 00000002
sind z eq x?u 00040000 00000000 00040000 00000000
sind z eq x?u 00080000 00000000 00080000 00000000
sind z eq x?u 000fffff fffffffe 000fffff fffffffe
sind z eq x?u 000fffff ffffffff 000fffff ffffffff
sind z eq x?u 00100000 00000000 00100000 00000000
sind z eq x?u 80000000 00000001 80000000 00000001
sind z eq x?u 80000000 00000002 80000000 00000002
sind z eq x?u 80040000 00000000 80040000 00000000
sind z eq x?u 80080000 00000000 80080000 00000000
sind z eq x?u 800fffff fffffffe 800fffff fffffffe
sind z eq x?u 800fffff ffffffff 800fffff ffffffff
sind z eq x?u 80100000 00000000 80100000 00000000
sind z uo - 7fffe000 00000000 7fffe000 00000000
sind z uo - ffffe000 00000000 ffffe000 00000000
sind z uo v 7ff00000 00000000 7fffe000 00000000
sind z uo v 7ff02000 00000000 7fffe000 00000000
sind z uo v fff00000 00000000 7fffe000 00000000
sind z uo v fff02000 00000000 ffffe000 00000000
sind z vn x 3e480000 00000000 3e480000 00000000
sind z vn x 3e500000 00000000 3e500000 00000000
sind z vn x be480000 00000000 be480000 00000000
sind z vn x be500000 00000000 be500000 00000000
