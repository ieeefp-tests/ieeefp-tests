/* Copyright (C) 1988-1994 Sun Microsystems, Inc. 2550 Garcia Avenue */
/* Mountain View, California  94043 All rights reserved. */
/*  */
/* Any person is hereby authorized to download, copy, use, create bug fixes,  */
/* and distribute, subject to the following conditions: */
/*  */
/* 	1.  the software may not be redistributed for a fee except as */
/* 	    reasonable to cover media costs; */
/* 	2.  any copy of the software must include this notice, as well as  */
/* 	    any other embedded copyright notices; and  */
/* 	3.  any distribution of this software or derivative works thereof  */
/* 	    must comply with all applicable U.S. export control laws. */
/*  */
/* THE SOFTWARE IS MADE AVAILABLE "AS IS" AND WITHOUT EXPRESS OR IMPLIED */
/* WARRANTY OF ANY KIND, INCLUDING BUT NOT LIMITED TO THE IMPLIED */
/* WARRANTIES OF DESIGN, MERCHANTIBILITY, FITNESS FOR A PARTICULAR */
/* PURPOSE, NON-INFRINGEMENT, PERFORMANCE OR CONFORMANCE TO */
/* SPECIFICATIONS.   */
/*  */
/* BY DOWNLOADING AND/OR USING THIS SOFTWARE, THE USER WAIVES ALL CLAIMS */
/* AGAINST SUN MICROSYSTEMS, INC. AND ITS AFFILIATED COMPANIES IN ANY */
/* JURISDICTION, INCLUDING BUT NOT LIMITED TO CLAIMS FOR DAMAGES OR */
/* EQUITABLE RELIEF BASED ON LOSS OF DATA, AND SPECIFICALLY WAIVES EVEN */
/* UNKNOWN OR UNANTICIPATED CLAIMS OR LOSSES, PRESENT AND FUTURE. */
/*  */
/* IN NO EVENT WILL SUN MICROSYSTEMS, INC. OR ANY OF ITS AFFILIATED */
/* COMPANIES BE LIABLE FOR ANY LOST REVENUE OR PROFITS OR OTHER SPECIAL, */
/* INDIRECT AND CONSEQUENTIAL DAMAGES, EVEN IF IT HAS BEEN ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGES. */
/*  */
/* This file is provided with no support and without any obligation on the */
/* part of Sun Microsystems, Inc. ("Sun") or any of its affiliated */
/* companies to assist in its use, correction, modification or */
/* enhancement.  Nevertheless, and without creating any obligation on its */
/* part, Sun welcomes your comments concerning the software and requests */
/* that they be sent to fdlibm-comments@sunpro.sun.com. */

	/* asind(+-1) is +-pi/2 */
asind n eq x 3ff00000 00000000 3ff921fb 54442d18
asind n eq x bff00000 00000000 bff921fb 54442d18
	/* asind(+-(1 - tiny)) :=: +-(pi/2-sqrt(2*tiny)) */
asind n nb x 3fefffff ffffffff 3ff921fb 50442d18
asind n nb x bfefffff ffffffff bff921fb 50442d18
asind z nb x 3fefffff ffffffff 3ff921fb 50442d18
asind z nb x bfefffff ffffffff bff921fb 50442d18
asind p nb x 3fefffff ffffffff 3ff921fb 50442d18
asind p nb x bfefffff ffffffff bff921fb 50442d18
asind m nb x 3fefffff ffffffff 3ff921fb 50442d18
asind m nb x bfefffff ffffffff bff921fb 50442d18
	/* asind(tiny) is tiny (only in nearest mode) */
asind n eq x 3e100000 0 3e100000 0
asind n eq x be100000 0 be100000 0
asind n eq x 00100000 0 00100000 0
asind n eq x 80100000 0 80100000 0
	/* asind(+-0) is +-0 */
asind n eq - 0 0 0 0
asind n eq - 80000000 0 80000000 0
asind z eq - 0 0 0 0
asind z eq - 80000000 0 80000000 0
asind p eq - 0 0 0 0
asind p eq - 80000000 0 80000000 0
asind m eq - 0 0 0 0
asind m eq - 80000000 0 80000000 0
	/* random arguments between -1 and 1 */
asind n nb x bfd13284 b2b5006d bfd1694c 32f76fb4
asind n nb x 3fe6ca8d fb825911 3fe95d81 3a6096cb
asind n nb x 3fec2ca6 09de7505 3ff13a9e 309847c0
asind n nb x bfe55f11 fba96889 bfe76700 71abc478
asind n nb x bfd15679 e27084dd bfd18ea3 e9808b2e
asind n nb x bfa41e13 1b093c41 bfa41f66 95577251
asind n nb x 3fe281b0 d18455f5 3fe3bbdb 8b39a790
asind n nb x 3feb5ce3 4a51b239 3ff0695b a906e921
asind n nb x bfc58348 1079de4d bfc59d8b 525cfa06
asind n nb x bfeea822 3103b871 bff47aff 79a824b2
	/* exception cases */
asind n uo v 3ff00000 1 0 0
asind z uo v 3ff00000 1 0 0
asind p uo v 3ff00000 1 0 0
asind m uo v 3ff00000 1 0 0
asind n uo v bff00000 1 0 0
asind n uo v 40000000 0 0 0
asind n uo v 40f00000 1 0 0
asind n uo v c0ffffff ffffffff 0 0
asind n uo v 7ff00000 1 0 0
asind n uo v fff00000 1 0 0
asind z uo v fff00000 1 0 0
asind p uo v fff00000 1 0 0
asind m uo v fff00000 1 0 0
asind n uo - 7ff80000 1 0 0
asind z uo - 7ff80000 1 0 0
asind p uo - 7ff80000 1 0 0
asind m uo - 7ff80000 1 0 0
asind n uo - fff80000 1 0 0

asind m eq x 00100000 00000001 00100000 00000001
asind m eq x 00100000 00000002 00100000 00000002
asind m eq x 00200000 00000000 00200000 00000000
asind m eq x 00300000 00000000 00300000 00000000
asind m eq x 80100000 00000001 80100000 00000001
asind m eq x 80100000 00000002 80100000 00000002
asind m eq x 80200000 00000000 80200000 00000000
asind m eq x 80300000 00000000 80300000 00000000
asind m eq x?u 00000000 00000001 00000000 00000001
asind m eq x?u 00000000 00000002 00000000 00000002
asind m eq x?u 00040000 00000000 00040000 00000000
asind m eq x?u 00080000 00000000 00080000 00000000
asind m eq x?u 000fffff fffffffe 000fffff fffffffe
asind m eq x?u 000fffff ffffffff 000fffff ffffffff
asind m eq x?u 00100000 00000000 00100000 00000000
asind m eq x?u 80000000 00000001 80000000 00000001
asind m eq x?u 80000000 00000002 80000000 00000002
asind m eq x?u 80040000 00000000 80040000 00000000
asind m eq x?u 80080000 00000000 80080000 00000000
asind m eq x?u 800fffff fffffffe 800fffff fffffffe
asind m eq x?u 800fffff ffffffff 800fffff ffffffff
asind m eq x?u 80100000 00000000 80100000 00000000
asind m uo - 7ff80000 00000001 00000000 00000000
asind m uo - 7fffe000 00000000 7fffe000 00000000
asind m uo - ffffe000 00000000 ffffe000 00000000
asind m uo v 3ff00000 00000001 7fffe000 00000000
asind m uo v 3ff00000 00000002 7fffe000 00000000
asind m uo v 40000000 00000000 7fffe000 00000000
asind m uo v 40100000 00000000 7fffe000 00000000
asind m uo v 7fd00000 00000000 7fffe000 00000000
asind m uo v 7fe00000 00000000 7fffe000 00000000
asind m uo v 7fefffff fffffffe 7fffe000 00000000
asind m uo v 7fefffff ffffffff 7fffe000 00000000
asind m uo v 7ff00000 00000000 7fffe000 00000000
asind m uo v 7ff02000 00000000 7fffe000 00000000
asind m uo v bff00000 00000001 ffffe000 00000000
asind m uo v bff00000 00000002 ffffe000 00000000
asind m uo v c0000000 00000000 ffffe000 00000000
asind m uo v c0100000 00000000 ffffe000 00000000
asind m uo v ffd00000 00000000 ffffe000 00000000
asind m uo v ffe00000 00000000 ffffe000 00000000
asind m uo v ffefffff fffffffe ffffe000 00000000
asind m uo v ffefffff ffffffff ffffe000 00000000
asind m uo v fff00000 00000000 ffffe000 00000000
asind m uo v fff00000 00000001 00000000 00000000
asind m uo v fff02000 00000000 ffffe000 00000000
asind n eq x 00100000 00000001 00100000 00000001
asind n eq x 00100000 00000002 00100000 00000002
asind n eq x 00200000 00000000 00200000 00000000
asind n eq x 00300000 00000000 00300000 00000000
asind n eq x 80100000 00000001 80100000 00000001
asind n eq x 80100000 00000002 80100000 00000002
asind n eq x 80200000 00000000 80200000 00000000
asind n eq x 80300000 00000000 80300000 00000000
asind n eq x?u 00000000 00000001 00000000 00000001
asind n eq x?u 00000000 00000002 00000000 00000002
asind n eq x?u 00040000 00000000 00040000 00000000
asind n eq x?u 00080000 00000000 00080000 00000000
asind n eq x?u 000fffff fffffffe 000fffff fffffffe
asind n eq x?u 000fffff ffffffff 000fffff ffffffff
asind n eq x?u 80000000 00000001 80000000 00000001
asind n eq x?u 80000000 00000002 80000000 00000002
asind n eq x?u 80040000 00000000 80040000 00000000
asind n eq x?u 80080000 00000000 80080000 00000000
asind n eq x?u 800fffff fffffffe 800fffff fffffffe
asind n eq x?u 800fffff ffffffff 800fffff ffffffff
asind n uo - 7ff80000 00000001 00000000 00000000
asind n uo - 7fffe000 00000000 7fffe000 00000000
asind n uo - fff80000 00000001 00000000 00000000
asind n uo - ffffe000 00000000 ffffe000 00000000
asind n uo v 3ff00000 00000001 7fffe000 00000000
asind n uo v 3ff00000 00000002 7fffe000 00000000
asind n uo v 40000000 00000000 7fffe000 00000000
asind n uo v 40100000 00000000 7fffe000 00000000
asind n uo v 40f00000 00000001 00000000 00000000
asind n uo v 7fd00000 00000000 7fffe000 00000000
asind n uo v 7fe00000 00000000 7fffe000 00000000
asind n uo v 7fefffff fffffffe 7fffe000 00000000
asind n uo v 7fefffff ffffffff 7fffe000 00000000
asind n uo v 7ff00000 00000000 7fffe000 00000000
asind n uo v 7ff00000 00000001 00000000 00000000
asind n uo v 7ff02000 00000000 7fffe000 00000000
asind n uo v bff00000 00000001 ffffe000 00000000
asind n uo v bff00000 00000002 ffffe000 00000000
asind n uo v c0000000 00000000 ffffe000 00000000
asind n uo v c0100000 00000000 ffffe000 00000000
asind n uo v c0ffffff ffffffff 00000000 00000000
asind n uo v ffd00000 00000000 ffffe000 00000000
asind n uo v ffe00000 00000000 ffffe000 00000000
asind n uo v ffefffff fffffffe ffffe000 00000000
asind n uo v ffefffff ffffffff ffffe000 00000000
asind n uo v fff00000 00000000 ffffe000 00000000
asind n uo v fff00000 00000001 00000000 00000000
asind n uo v fff02000 00000000 ffffe000 00000000
asind p eq x 00100000 00000001 00100000 00000001
asind p eq x 00100000 00000002 00100000 00000002
asind p eq x 00200000 00000000 00200000 00000000
asind p eq x 00300000 00000000 00300000 00000000
asind p eq x 80100000 00000001 80100000 00000001
asind p eq x 80100000 00000002 80100000 00000002
asind p eq x 80200000 00000000 80200000 00000000
asind p eq x 80300000 00000000 80300000 00000000
asind p eq x?u 00000000 00000001 00000000 00000001
asind p eq x?u 00000000 00000002 00000000 00000002
asind p eq x?u 00040000 00000000 00040000 00000000
asind p eq x?u 00080000 00000000 00080000 00000000
asind p eq x?u 000fffff fffffffe 000fffff fffffffe
asind p eq x?u 000fffff ffffffff 000fffff ffffffff
asind p eq x?u 00100000 00000000 00100000 00000000
asind p eq x?u 80000000 00000001 80000000 00000001
asind p eq x?u 80000000 00000002 80000000 00000002
asind p eq x?u 80040000 00000000 80040000 00000000
asind p eq x?u 80080000 00000000 80080000 00000000
asind p eq x?u 800fffff fffffffe 800fffff fffffffe
asind p eq x?u 800fffff ffffffff 800fffff ffffffff
asind p eq x?u 80100000 00000000 80100000 00000000
asind p uo - 7ff80000 00000001 00000000 00000000
asind p uo - 7fffe000 00000000 7fffe000 00000000
asind p uo - ffffe000 00000000 ffffe000 00000000
asind p uo v 3ff00000 00000001 7fffe000 00000000
asind p uo v 3ff00000 00000002 7fffe000 00000000
asind p uo v 40000000 00000000 7fffe000 00000000
asind p uo v 40100000 00000000 7fffe000 00000000
asind p uo v 7fd00000 00000000 7fffe000 00000000
asind p uo v 7fe00000 00000000 7fffe000 00000000
asind p uo v 7fefffff fffffffe 7fffe000 00000000
asind p uo v 7fefffff ffffffff 7fffe000 00000000
asind p uo v 7ff00000 00000000 7fffe000 00000000
asind p uo v 7ff02000 00000000 7fffe000 00000000
asind p uo v bff00000 00000001 ffffe000 00000000
asind p uo v bff00000 00000002 ffffe000 00000000
asind p uo v c0000000 00000000 ffffe000 00000000
asind p uo v c0100000 00000000 ffffe000 00000000
asind p uo v ffd00000 00000000 ffffe000 00000000
asind p uo v ffe00000 00000000 ffffe000 00000000
asind p uo v ffefffff fffffffe ffffe000 00000000
asind p uo v ffefffff ffffffff ffffe000 00000000
asind p uo v fff00000 00000000 ffffe000 00000000
asind p uo v fff00000 00000001 00000000 00000000
asind p uo v fff02000 00000000 ffffe000 00000000
asind z eq x 00100000 00000001 00100000 00000001
asind z eq x 00100000 00000002 00100000 00000002
asind z eq x 00200000 00000000 00200000 00000000
asind z eq x 00300000 00000000 00300000 00000000
asind z eq x 80100000 00000001 80100000 00000001
asind z eq x 80100000 00000002 80100000 00000002
asind z eq x 80200000 00000000 80200000 00000000
asind z eq x 80300000 00000000 80300000 00000000
asind z eq x?u 00000000 00000001 00000000 00000001
asind z eq x?u 00000000 00000002 00000000 00000002
asind z eq x?u 00040000 00000000 00040000 00000000
asind z eq x?u 00080000 00000000 00080000 00000000
asind z eq x?u 000fffff fffffffe 000fffff fffffffe
asind z eq x?u 000fffff ffffffff 000fffff ffffffff
asind z eq x?u 00100000 00000000 00100000 00000000
asind z eq x?u 80000000 00000001 80000000 00000001
asind z eq x?u 80000000 00000002 80000000 00000002
asind z eq x?u 80040000 00000000 80040000 00000000
asind z eq x?u 80080000 00000000 80080000 00000000
asind z eq x?u 800fffff fffffffe 800fffff fffffffe
asind z eq x?u 800fffff ffffffff 800fffff ffffffff
asind z eq x?u 80100000 00000000 80100000 00000000
asind z uo - 7ff80000 00000001 00000000 00000000
asind z uo - 7fffe000 00000000 7fffe000 00000000
asind z uo - ffffe000 00000000 ffffe000 00000000
asind z uo v 3ff00000 00000001 7fffe000 00000000
asind z uo v 3ff00000 00000002 7fffe000 00000000
asind z uo v 40000000 00000000 7fffe000 00000000
asind z uo v 40100000 00000000 7fffe000 00000000
asind z uo v 7fd00000 00000000 7fffe000 00000000
asind z uo v 7fe00000 00000000 7fffe000 00000000
asind z uo v 7fefffff fffffffe 7fffe000 00000000
asind z uo v 7fefffff ffffffff 7fffe000 00000000
asind z uo v 7ff00000 00000000 7fffe000 00000000
asind z uo v 7ff02000 00000000 7fffe000 00000000
asind z uo v bff00000 00000001 ffffe000 00000000
asind z uo v bff00000 00000002 ffffe000 00000000
asind z uo v c0000000 00000000 ffffe000 00000000
asind z uo v c0100000 00000000 ffffe000 00000000
asind z uo v ffd00000 00000000 ffffe000 00000000
asind z uo v ffe00000 00000000 ffffe000 00000000
asind z uo v ffefffff fffffffe ffffe000 00000000
asind z uo v ffefffff ffffffff ffffe000 00000000
asind z uo v fff00000 00000000 ffffe000 00000000
asind z uo v fff00000 00000001 00000000 00000000
asind z uo v fff02000 00000000 ffffe000 00000000
