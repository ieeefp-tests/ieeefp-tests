/* Copyright (C) 1988-1994 Sun Microsystems, Inc. 2550 Garcia Avenue */
/* Mountain View, California  94043 All rights reserved. */
/*  */
/* Any person is hereby authorized to download, copy, use, create bug fixes,  */
/* and distribute, subject to the following conditions: */
/*  */
/* 	1.  the software may not be redistributed for a fee except as */
/* 	    reasonable to cover media costs; */
/* 	2.  any copy of the software must include this notice, as well as  */
/* 	    any other embedded copyright notices; and  */
/* 	3.  any distribution of this software or derivative works thereof  */
/* 	    must comply with all applicable U.S. export control laws. */
/*  */
/* THE SOFTWARE IS MADE AVAILABLE "AS IS" AND WITHOUT EXPRESS OR IMPLIED */
/* WARRANTY OF ANY KIND, INCLUDING BUT NOT LIMITED TO THE IMPLIED */
/* WARRANTIES OF DESIGN, MERCHANTIBILITY, FITNESS FOR A PARTICULAR */
/* PURPOSE, NON-INFRINGEMENT, PERFORMANCE OR CONFORMANCE TO */
/* SPECIFICATIONS.   */
/*  */
/* BY DOWNLOADING AND/OR USING THIS SOFTWARE, THE USER WAIVES ALL CLAIMS */
/* AGAINST SUN MICROSYSTEMS, INC. AND ITS AFFILIATED COMPANIES IN ANY */
/* JURISDICTION, INCLUDING BUT NOT LIMITED TO CLAIMS FOR DAMAGES OR */
/* EQUITABLE RELIEF BASED ON LOSS OF DATA, AND SPECIFICALLY WAIVES EVEN */
/* UNKNOWN OR UNANTICIPATED CLAIMS OR LOSSES, PRESENT AND FUTURE. */
/*  */
/* IN NO EVENT WILL SUN MICROSYSTEMS, INC. OR ANY OF ITS AFFILIATED */
/* COMPANIES BE LIABLE FOR ANY LOST REVENUE OR PROFITS OR OTHER SPECIAL, */
/* INDIRECT AND CONSEQUENTIAL DAMAGES, EVEN IF IT HAS BEEN ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGES. */
/*  */
/* This file is provided with no support and without any obligation on the */
/* part of Sun Microsystems, Inc. ("Sun") or any of its affiliated */
/* companies to assist in its use, correction, modification or */
/* enhancement.  Nevertheless, and without creating any obligation on its */
/* part, Sun welcomes your comments concerning the software and requests */
/* that they be sent to fdlibm-comments@sunpro.sun.com. */

	/* overflow boundary */
coshs n nb x 42b2d4fc 7f7fffec
coshs z nb x 42b2d4fc 7f7fffec
coshs p nb x 42b2d4fc 7f7fffec
coshs m nb x 42b2d4fc 7f7fffec
coshs n nb x c2b2d4fc 7f7fffec
coshs n eq ox 42b2d4fd 7f800000
coshs n eq ox c2b2d4fd 7f800000
coshs z eq ox 42b2d4fd 7f7fffff
coshs p eq ox 42b2d4fd 7f800000
coshs m eq ox 42b2d4fd 7f7fffff
	/* coshs(0 or tiny) :=: 1.0 */
coshs n eq x 31000000 3f800000
coshs n eq x b1000000 3f800000
coshs n eq x 00800000 3f800000
coshs n eq x 80800000 3f800000
coshs n eq x 1 3f800000
coshs n eq x 80000001 3f800000
coshs n eq - 0 3f800000
coshs n eq - 80000000 3f800000
	/* random arguments between -50,50 */
coshs n nb x c156f77e 48a6f197
coshs n nb x 420e71f8 58a62373
coshs n nb x 42301708 5eb6689b
coshs n nb x c2059230 5710908d
coshs n nb x c158b8f7 48ba4e60
coshs n nb x bffb77f9 4068b660
coshs n nb x 41e7551f 53d283f2
coshs n nb x 422b048a 5dcd4d76
coshs n nb x c1067481 450b7340
coshs n nb x c23f9ada 6189d729
	/* coshs(nan/inf) */
coshs n uo v 7f800001 0
coshs n uo - 7fc00000 0
coshs n uo v ff800001 0
coshs n eq - 7f800000 7f800000
coshs n eq - ff800000 7f800000
coshs m eq - 7f800000 7f800000
coshs m eq - 80000000 3f800000
coshs m eq - ff800000 7f800000
coshs m eq x 00000001 3f800000
coshs m eq x 00000002 3f800000
coshs m eq x 00200000 3f800000
coshs m eq x 00400000 3f800000
coshs m eq x 007ffffe 3f800000
coshs m eq x 007fffff 3f800000
coshs m eq x 00800000 3f800000
coshs m eq x 00800001 3f800000
coshs m eq x 00800002 3f800000
coshs m eq x 01000000 3f800000
coshs m eq x 01800000 3f800000
coshs m eq x 32000000 3f800000
coshs m eq x 39800000 3f800000
coshs m eq x 3a000000 3f800001
coshs m eq x 80000001 3f800000
coshs m eq x 80000002 3f800000
coshs m eq x 80200000 3f800000
coshs m eq x 80400000 3f800000
coshs m eq x 807ffffe 3f800000
coshs m eq x 807fffff 3f800000
coshs m eq x 80800000 3f800000
coshs m eq x 80800001 3f800000
coshs m eq x 80800002 3f800000
coshs m eq x 81000000 3f800000
coshs m eq x 81800000 3f800000
coshs m eq x b2000000 3f800000
coshs m eq x b9800000 3f800000
coshs m eq x ba000000 3f800001
coshs m eq xo 42b2d4fd 7f7fffff
coshs m eq xo 42b30000 7f7fffff
coshs m eq xo 4431a000 7f7fffff
coshs m eq xo 7e800000 7f7fffff
coshs m eq xo 7f000000 7f7fffff
coshs m eq xo 7f7ffffe 7f7fffff
coshs m eq xo 7f7fffff 7f7fffff
coshs m eq xo c2b30000 7f7fffff
coshs m eq xo c431a000 7f7fffff
coshs m eq xo fe800000 7f7fffff
coshs m eq xo ff000000 7f7fffff
coshs m eq xo ff7ffffe 7f7fffff
coshs m eq xo ff7fffff 7f7fffff
coshs m uo - 7fff0000 7fff0000
coshs m uo - ffff0000 7fff0000
coshs m uo v 7f810000 7fff0000
coshs m uo v ff810000 7fff0000
coshs n eq x 00000002 3f800000
coshs n eq x 00200000 3f800000
coshs n eq x 00400000 3f800000
coshs n eq x 007ffffe 3f800000
coshs n eq x 007fffff 3f800000
coshs n eq x 00800001 3f800000
coshs n eq x 00800002 3f800000
coshs n eq x 01000000 3f800000
coshs n eq x 01800000 3f800000
coshs n eq x 32000000 3f800000
coshs n eq x 39800000 3f800000
coshs n eq x 3a000000 3f800001
coshs n eq x 80000002 3f800000
coshs n eq x 80200000 3f800000
coshs n eq x 80400000 3f800000
coshs n eq x 807ffffe 3f800000
coshs n eq x 807fffff 3f800000
coshs n eq x 80800001 3f800000
coshs n eq x 80800002 3f800000
coshs n eq x 81000000 3f800000
coshs n eq x 81800000 3f800000
coshs n eq x b2000000 3f800000
coshs n eq x b9800000 3f800000
coshs n eq x ba000000 3f800001
coshs n eq xo 42b2d4fd 7f800000
coshs n eq xo 42b30000 7f800000
coshs n eq xo 4431a000 7f800000
coshs n eq xo 7e800000 7f800000
coshs n eq xo 7f000000 7f800000
coshs n eq xo 7f7ffffe 7f800000
coshs n eq xo 7f7fffff 7f800000
coshs n eq xo c2b2d4fd 7f800000
coshs n eq xo c2b30000 7f800000
coshs n eq xo c431a000 7f800000
coshs n eq xo fe800000 7f800000
coshs n eq xo ff000000 7f800000
coshs n eq xo ff7ffffe 7f800000
coshs n eq xo ff7fffff 7f800000
coshs n uo - 7fc00000 00000000
coshs n uo - 7fff0000 7fff0000
coshs n uo - ffff0000 7fff0000
coshs n uo v 7f800001 00000000
coshs n uo v 7f810000 7fff0000
coshs n uo v ff800001 00000000
coshs n uo v ff810000 7fff0000
coshs p eq - 00000000 3f800000
coshs p eq - 7f800000 7f800000
coshs p eq - 80000000 3f800000
coshs p eq - ff800000 7f800000
coshs p eq xo 42b2d4fd 7f800000
coshs p eq xo 42b30000 7f800000
coshs p eq xo 4431a000 7f800000
coshs p eq xo 7e800000 7f800000
coshs p eq xo 7f000000 7f800000
coshs p eq xo 7f7ffffe 7f800000
coshs p eq xo 7f7fffff 7f800000
coshs p eq xo c2b30000 7f800000
coshs p eq xo c431a000 7f800000
coshs p eq xo fe800000 7f800000
coshs p eq xo ff000000 7f800000
coshs p eq xo ff7ffffe 7f800000
coshs p eq xo ff7fffff 7f800000
coshs p uo - 7fff0000 7fff0000
coshs p uo - ffff0000 7fff0000
coshs p uo v 7f810000 7fff0000
coshs p uo v ff810000 7fff0000
coshs p vn x 00000001 3f800000
coshs p vn x 00000002 3f800000
coshs p vn x 00200000 3f800000
coshs p vn x 00400000 3f800000
coshs p vn x 007ffffe 3f800000
coshs p vn x 007fffff 3f800000
coshs p vn x 00800000 3f800000
coshs p vn x 00800001 3f800000
coshs p vn x 00800002 3f800000
coshs p vn x 01000000 3f800000
coshs p vn x 01800000 3f800000
coshs p vn x 32000000 3f800000
coshs p vn x 39800000 3f800000
coshs p vn x 3a000000 3f800001
coshs p vn x 80000001 3f800000
coshs p vn x 80000002 3f800000
coshs p vn x 80200000 3f800000
coshs p vn x 80400000 3f800000
coshs p vn x 807ffffe 3f800000
coshs p vn x 807fffff 3f800000
coshs p vn x 80800000 3f800000
coshs p vn x 80800001 3f800000
coshs p vn x 80800002 3f800000
coshs p vn x 81000000 3f800000
coshs p vn x 81800000 3f800000
coshs p vn x b2000000 3f800000
coshs p vn x b9800000 3f800000
coshs p vn x ba000000 3f800001
coshs z eq - 00000000 3f800000
coshs z eq - 7f800000 7f800000
coshs z eq - 80000000 3f800000
coshs z eq - ff800000 7f800000
coshs z eq x 00000001 3f800000
coshs z eq x 00000002 3f800000
coshs z eq x 00200000 3f800000
coshs z eq x 00400000 3f800000
coshs z eq x 007ffffe 3f800000
coshs z eq x 007fffff 3f800000
coshs z eq x 00800000 3f800000
coshs z eq x 00800001 3f800000
coshs z eq x 00800002 3f800000
coshs z eq x 01000000 3f800000
coshs z eq x 01800000 3f800000
coshs z eq x 32000000 3f800000
coshs z eq x 39800000 3f800000
coshs z eq x 3a000000 3f800001
coshs z eq x 80000001 3f800000
coshs z eq x 80000002 3f800000
coshs z eq x 80200000 3f800000
coshs z eq x 80400000 3f800000
coshs z eq x 807ffffe 3f800000
coshs z eq x 807fffff 3f800000
coshs z eq x 80800000 3f800000
coshs z eq x 80800001 3f800000
coshs z eq x 80800002 3f800000
coshs z eq x 81000000 3f800000
coshs z eq x 81800000 3f800000
coshs z eq x b2000000 3f800000
coshs z eq x b9800000 3f800000
coshs z eq x ba000000 3f800001
coshs z eq xo 42b2d4fd 7f7fffff
coshs z eq xo 42b30000 7f7fffff
coshs z eq xo 4431a000 7f7fffff
coshs z eq xo 7e800000 7f7fffff
coshs z eq xo 7f000000 7f7fffff
coshs z eq xo 7f7ffffe 7f7fffff
coshs z eq xo 7f7fffff 7f7fffff
coshs z eq xo c2b30000 7f7fffff
coshs z eq xo c431a000 7f7fffff
coshs z eq xo fe800000 7f7fffff
coshs z eq xo ff000000 7f7fffff
coshs z eq xo ff7ffffe 7f7fffff
coshs z eq xo ff7fffff 7f7fffff
coshs z uo - 7fff0000 7fff0000
coshs z uo - ffff0000 7fff0000
coshs z uo v 7f810000 7fff0000
coshs z uo v ff810000 7fff0000
