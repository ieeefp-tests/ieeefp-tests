/* Copyright (C) 1988-1994 Sun Microsystems, Inc. 2550 Garcia Avenue */
/* Mountain View, California  94043 All rights reserved. */
/*  */
/* Any person is hereby authorized to download, copy, use, create bug fixes,  */
/* and distribute, subject to the following conditions: */
/*  */
/* 	1.  the software may not be redistributed for a fee except as */
/* 	    reasonable to cover media costs; */
/* 	2.  any copy of the software must include this notice, as well as  */
/* 	    any other embedded copyright notices; and  */
/* 	3.  any distribution of this software or derivative works thereof  */
/* 	    must comply with all applicable U.S. export control laws. */
/*  */
/* THE SOFTWARE IS MADE AVAILABLE "AS IS" AND WITHOUT EXPRESS OR IMPLIED */
/* WARRANTY OF ANY KIND, INCLUDING BUT NOT LIMITED TO THE IMPLIED */
/* WARRANTIES OF DESIGN, MERCHANTIBILITY, FITNESS FOR A PARTICULAR */
/* PURPOSE, NON-INFRINGEMENT, PERFORMANCE OR CONFORMANCE TO */
/* SPECIFICATIONS.   */
/*  */
/* BY DOWNLOADING AND/OR USING THIS SOFTWARE, THE USER WAIVES ALL CLAIMS */
/* AGAINST SUN MICROSYSTEMS, INC. AND ITS AFFILIATED COMPANIES IN ANY */
/* JURISDICTION, INCLUDING BUT NOT LIMITED TO CLAIMS FOR DAMAGES OR */
/* EQUITABLE RELIEF BASED ON LOSS OF DATA, AND SPECIFICALLY WAIVES EVEN */
/* UNKNOWN OR UNANTICIPATED CLAIMS OR LOSSES, PRESENT AND FUTURE. */
/*  */
/* IN NO EVENT WILL SUN MICROSYSTEMS, INC. OR ANY OF ITS AFFILIATED */
/* COMPANIES BE LIABLE FOR ANY LOST REVENUE OR PROFITS OR OTHER SPECIAL, */
/* INDIRECT AND CONSEQUENTIAL DAMAGES, EVEN IF IT HAS BEEN ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGES. */
/*  */
/* This file is provided with no support and without any obligation on the */
/* part of Sun Microsystems, Inc. ("Sun") or any of its affiliated */
/* companies to assist in its use, correction, modification or */
/* enhancement.  Nevertheless, and without creating any obligation on its */
/* part, Sun welcomes your comments concerning the software and requests */
/* that they be sent to fdlibm-comments@sunpro.sun.com. */

	/* overflow threshold */
exps n vn x  42b17217 7f7fff84
exps n eq ox 42b17218 7f800000
exps n eq ox 7f7fffff 7f800000
exps n eq -  7f800000 7f800000
exps z nb x  42b17217 7f7fff84
exps z eq ox 42b17218 7f7fffff
exps z eq ox 7f7fffff 7f7fffff
exps z eq -  7f800000 7f800000
exps p nb x  42b17217 7f7fff84
exps p eq ox 42b17218 7f800000
exps p eq ox 7f7fffff 7f800000
exps p eq -  7f800000 7f800000
exps m nb x  42b17217 7f7fff84
exps m eq ox 42b17218 7f7fffff
exps m eq ox 7f7fffff 7f7fffff
exps m eq -  7f800000 7f800000
	/* exps(0 or tiny) = 1 */
exps n eq - 0 3f800000
exps n eq - 80000000 3f800000
exps n eq x 1 3f800000
exps n eq x 80000001 3f800000
exps n eq x 00800000 3f800000
exps n eq x 80800000 3f800000
	/* underflow */
exps n vn x c2aeac4f 00800026
exps z nb x c2aeac4f 00800026
exps p nb x c2aeac4f 00800026
exps m nb x c2aeac4f 00800026
exps n vn ux c2aeac50 007fffe6
exps n eq ux c2cff1b4 1
exps n eq ux c2cff1b5 0
exps n eq ux ff7fffff 0
exps z eq ux ff7fffff 0
exps p eq ux ff7fffff 1
exps m eq ux ff7fffff 0
exps n eq - ff800000 0
	/* random arguments between -20 20 */
exps n vn x c0abf92e 3b97df51
exps n vn x 4163e988 49bb7778
exps n vn x 418cdf38 4c2969ef
exps n vn x c155b6b8 35d4456f
exps n vn x c0ad60c7 3b915994
exps n vn x bf492cc1 3ee95660
exps n vn x 413910e7 47ce1675
exps n vn x 4188d06a 4bcc0773
exps n vn x c05720d1 3d0e1468
exps n vn x c19948aa 31a3f7ca
	/* exps(nan) is nan */
exps n uo v 7f800001 0 
exps n uo - ffc00000 0 
exps m eq - 7f800000 7f800000
exps m eq - 80000000 3f800000
exps m eq - ff800000 00000000
exps m eq x 00000001 3f800000
exps m eq x 00000002 3f800000
exps m eq x 00000003 3f800000
exps m eq x 00200000 3f800000
exps m eq x 00400000 3f800000
exps m eq x 007ffffe 3f800000
exps m eq x 007fffff 3f800000
exps m eq x 00800000 3f800000
exps m eq x 00800001 3f800000
exps m eq x 00800002 3f800000
exps m eq x 00fffffd 3f800000
exps m eq x 00ffffff 3f800000
exps m eq x 01000000 3f800000
exps m eq x 01800000 3f800000
exps m eq x 1b000000 3f800000
exps m eq x 1e800000 3f800000
exps m eq x 1f000000 3f800000
exps m eq xo 42b17218 7f7fffff
exps m eq xo 42b20000 7f7fffff
exps m eq xo 4664e400 7f7fffff
exps m eq xo 46800000 7f7fffff
exps m eq xo 48800000 7f7fffff
exps m eq xo 7b000000 7f7fffff
exps m eq xo 7e000000 7f7fffff
exps m eq xo 7e800000 7f7fffff
exps m eq xo 7f000000 7f7fffff
exps m eq xo 7f7ffff7 7f7fffff
exps m eq xo 7f7ffffc 7f7fffff
exps m eq xo 7f7ffffd 7f7fffff
exps m eq xo 7f7ffffe 7f7fffff
exps m eq xo 7f7fffff 7f7fffff
exps m eq xu c2d00000 00000000
exps m eq xu c664e400 00000000
exps m eq xu c8800000 00000000
exps m eq xu fd000000 00000000
exps m eq xu fd800000 00000000
exps m eq xu fe000000 00000000
exps m eq xu fe800000 00000000
exps m eq xu ff000000 00000000
exps m eq xu ff7ffff7 00000000
exps m eq xu ff7ffffc 00000000
exps m eq xu ff7ffffd 00000000
exps m eq xu ff7ffffe 00000000
exps m eq xu ff7fffff 00000000
exps m uo - 7fff0000 7fff0000
exps m uo - ffff0000 ffff0000
exps m uo v 7f810000 7fff0000
exps m uo v ff810000 ffff0000
exps m vn x 33ffffff 3f800001
exps m vn x 347ffffc 3f800002
exps m vn x 34fffffc 3f800004
exps m vn x 42b17217 7f7fff84
exps m vn x 80000001 3f800000
exps m vn x 80000002 3f800000
exps m vn x 80200000 3f800000
exps m vn x 80400000 3f800000
exps m vn x 807ffffe 3f800000
exps m vn x 807fffff 3f800000
exps m vn x 80800000 3f800000
exps m vn x 80800001 3f800000
exps m vn x 80800002 3f800000
exps m vn x 81000000 3f800000
exps m vn x 81800000 3f800000
exps m vn x 9b000000 3f800000
exps m vn x 9e800000 3f800000
exps m vn x b4800001 3f7ffffc
exps m vn x b4c00004 3f7ffffa
exps n eq - 7f800000 7f800000
exps n eq x 00000002 3f800000
exps n eq x 00000003 3f800000
exps n eq x 00200000 3f800000
exps n eq x 00400000 3f800000
exps n eq x 007ffffe 3f800000
exps n eq x 007fffff 3f800000
exps n eq x 00800001 3f800000
exps n eq x 00800002 3f800000
exps n eq x 00fffffd 3f800000
exps n eq x 00ffffff 3f800000
exps n eq x 01000000 3f800000
exps n eq x 01800000 3f800000
exps n eq x 1b000000 3f800000
exps n eq x 1e800000 3f800000
exps n eq x 1f000000 3f800000
exps n eq x 33ffffff 3f800001
exps n eq x 347ffffc 3f800002
exps n eq x 34fffffc 3f800004
exps n eq x 80000002 3f800000
exps n eq x 80200000 3f800000
exps n eq x 80400000 3f800000
exps n eq x 807ffffe 3f800000
exps n eq x 807fffff 3f800000
exps n eq x 80800001 3f800000
exps n eq x 80800002 3f800000
exps n eq x 81000000 3f800000
exps n eq x 81800000 3f800000
exps n eq x 9b000000 3f800000
exps n eq x 9e800000 3f800000
exps n eq x b4800001 3f7ffffc
exps n eq x b4c00004 3f7ffffa
exps n eq xo 42b17218 7f800000
exps n eq xo 42b20000 7f800000
exps n eq xo 4664e400 7f800000
exps n eq xo 46800000 7f800000
exps n eq xo 48800000 7f800000
exps n eq xo 7b000000 7f800000
exps n eq xo 7e000000 7f800000
exps n eq xo 7e800000 7f800000
exps n eq xo 7f000000 7f800000
exps n eq xo 7f7ffff7 7f800000
exps n eq xo 7f7ffffc 7f800000
exps n eq xo 7f7ffffd 7f800000
exps n eq xo 7f7ffffe 7f800000
exps n eq xo 7f7fffff 7f800000
exps n eq xu c2cff1b4 00000001
exps n eq xu c2cff1b5 00000000
exps n eq xu c2d00000 00000000
exps n eq xu c664e400 00000000
exps n eq xu c8800000 00000000
exps n eq xu fd000000 00000000
exps n eq xu fd800000 00000000
exps n eq xu fe000000 00000000
exps n eq xu fe800000 00000000
exps n eq xu ff000000 00000000
exps n eq xu ff7ffff7 00000000
exps n eq xu ff7ffffc 00000000
exps n eq xu ff7ffffd 00000000
exps n eq xu ff7ffffe 00000000
exps n eq xu ff7fffff 00000000
exps n uo - 7fff0000 7fff0000
exps n uo - ffc00000 00000000
exps n uo - ffff0000 ffff0000
exps n uo v 7f800001 00000000
exps n uo v 7f810000 7fff0000
exps n uo v ff810000 ffff0000
exps n vn x 42b17217 7f7fff84
exps n vn xu c2aeac50 007fffe6
exps p eq - 00000000 3f800000
exps p eq - 7f800000 7f800000
exps p eq - 80000000 3f800000
exps p eq - ff800000 00000000
exps p eq x 80000001 3f800000
exps p eq x 80000002 3f800000
exps p eq x 80200000 3f800000
exps p eq x 80400000 3f800000
exps p eq x 807ffffe 3f800000
exps p eq x 807fffff 3f800000
exps p eq x 80800000 3f800000
exps p eq x 80800001 3f800000
exps p eq x 80800002 3f800000
exps p eq x 81000000 3f800000
exps p eq x 81800000 3f800000
exps p eq x 9b000000 3f800000
exps p eq x 9e800000 3f800000
exps p eq xo 42b17218 7f800000
exps p eq xo 42b20000 7f800000
exps p eq xo 4664e400 7f800000
exps p eq xo 46800000 7f800000
exps p eq xo 48800000 7f800000
exps p eq xo 7b000000 7f800000
exps p eq xo 7e000000 7f800000
exps p eq xo 7e800000 7f800000
exps p eq xo 7f000000 7f800000
exps p eq xo 7f7ffff7 7f800000
exps p eq xo 7f7ffffc 7f800000
exps p eq xo 7f7ffffd 7f800000
exps p eq xo 7f7ffffe 7f800000
exps p eq xo 7f7fffff 7f800000
exps p eq xu c8800000 00000001
exps p eq xu fd000000 00000001
exps p eq xu fd800000 00000001
exps p eq xu fe000000 00000001
exps p eq xu fe800000 00000001
exps p eq xu ff000000 00000001
exps p eq xu ff7ffff7 00000001
exps p eq xu ff7ffffc 00000001
exps p eq xu ff7ffffd 00000001
exps p eq xu ff7ffffe 00000001
exps p eq xu ff7fffff 00000001
exps p uo - 7fff0000 7fff0000
exps p uo - ffff0000 ffff0000
exps p uo v 7f810000 7fff0000
exps p uo v ff810000 ffff0000
exps p vn x 00000001 3f800000
exps p vn x 00000002 3f800000
exps p vn x 00000003 3f800000
exps p vn x 00200000 3f800000
exps p vn x 00400000 3f800000
exps p vn x 007ffffe 3f800000
exps p vn x 007fffff 3f800000
exps p vn x 00800000 3f800000
exps p vn x 00800001 3f800000
exps p vn x 00800002 3f800000
exps p vn x 00fffffd 3f800000
exps p vn x 00ffffff 3f800000
exps p vn x 01000000 3f800000
exps p vn x 01800000 3f800000
exps p vn x 1b000000 3f800000
exps p vn x 1e800000 3f800000
exps p vn x 1f000000 3f800000
exps p vn x 33ffffff 3f800001
exps p vn x 347ffffc 3f800002
exps p vn x 34fffffc 3f800004
exps p vn x 42b17217 7f7fff84
exps p vn x b4800001 3f7ffffc
exps p vn x b4c00004 3f7ffffa
exps p vn xu c2d00000 00000000
exps p vn xu c664e400 00000000
exps z eq - 00000000 3f800000
exps z eq - 7f800000 7f800000
exps z eq - 80000000 3f800000
exps z eq - ff800000 00000000
exps z eq x 00000001 3f800000
exps z eq x 00000002 3f800000
exps z eq x 00000003 3f800000
exps z eq x 00200000 3f800000
exps z eq x 00400000 3f800000
exps z eq x 007ffffe 3f800000
exps z eq x 007fffff 3f800000
exps z eq x 00800000 3f800000
exps z eq x 00800001 3f800000
exps z eq x 00800002 3f800000
exps z eq x 00fffffd 3f800000
exps z eq x 00ffffff 3f800000
exps z eq x 01000000 3f800000
exps z eq x 01800000 3f800000
exps z eq x 1b000000 3f800000
exps z eq x 1e800000 3f800000
exps z eq x 1f000000 3f800000
exps z eq xo 42b17218 7f7fffff
exps z eq xo 42b20000 7f7fffff
exps z eq xo 4664e400 7f7fffff
exps z eq xo 46800000 7f7fffff
exps z eq xo 48800000 7f7fffff
exps z eq xo 7b000000 7f7fffff
exps z eq xo 7e000000 7f7fffff
exps z eq xo 7e800000 7f7fffff
exps z eq xo 7f000000 7f7fffff
exps z eq xo 7f7ffff7 7f7fffff
exps z eq xo 7f7ffffc 7f7fffff
exps z eq xo 7f7ffffd 7f7fffff
exps z eq xo 7f7ffffe 7f7fffff
exps z eq xo 7f7fffff 7f7fffff
exps z eq xu c2d00000 00000000
exps z eq xu c664e400 00000000
exps z eq xu c8800000 00000000
exps z eq xu fd000000 00000000
exps z eq xu fd800000 00000000
exps z eq xu fe000000 00000000
exps z eq xu fe800000 00000000
exps z eq xu ff000000 00000000
exps z eq xu ff7ffff7 00000000
exps z eq xu ff7ffffc 00000000
exps z eq xu ff7ffffd 00000000
exps z eq xu ff7ffffe 00000000
exps z eq xu ff7fffff 00000000
exps z uo - 7fff0000 7fff0000
exps z uo - ffff0000 ffff0000
exps z uo v 7f810000 7fff0000
exps z uo v ff810000 ffff0000
exps z vn x 33ffffff 3f800001
exps z vn x 347ffffc 3f800002
exps z vn x 34fffffc 3f800004
exps z vn x 42b17217 7f7fff84
exps z vn x 80000001 3f800000
exps z vn x 80000002 3f800000
exps z vn x 80200000 3f800000
exps z vn x 80400000 3f800000
exps z vn x 807ffffe 3f800000
exps z vn x 807fffff 3f800000
exps z vn x 80800000 3f800000
exps z vn x 80800001 3f800000
exps z vn x 80800002 3f800000
exps z vn x 81000000 3f800000
exps z vn x 81800000 3f800000
exps z vn x 9b000000 3f800000
exps z vn x 9e800000 3f800000
exps z vn x b4800001 3f7ffffc
exps z vn x b4c00004 3f7ffffa
