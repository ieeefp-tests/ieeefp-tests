/* Copyright (C) 1988-1994 Sun Microsystems, Inc. 2550 Garcia Avenue */
/* Mountain View, California  94043 All rights reserved. */
/*  */
/* Any person is hereby authorized to download, copy, use, create bug fixes,  */
/* and distribute, subject to the following conditions: */
/*  */
/* 	1.  the software may not be redistributed for a fee except as */
/* 	    reasonable to cover media costs; */
/* 	2.  any copy of the software must include this notice, as well as  */
/* 	    any other embedded copyright notices; and  */
/* 	3.  any distribution of this software or derivative works thereof  */
/* 	    must comply with all applicable U.S. export control laws. */
/*  */
/* THE SOFTWARE IS MADE AVAILABLE "AS IS" AND WITHOUT EXPRESS OR IMPLIED */
/* WARRANTY OF ANY KIND, INCLUDING BUT NOT LIMITED TO THE IMPLIED */
/* WARRANTIES OF DESIGN, MERCHANTIBILITY, FITNESS FOR A PARTICULAR */
/* PURPOSE, NON-INFRINGEMENT, PERFORMANCE OR CONFORMANCE TO */
/* SPECIFICATIONS.   */
/*  */
/* BY DOWNLOADING AND/OR USING THIS SOFTWARE, THE USER WAIVES ALL CLAIMS */
/* AGAINST SUN MICROSYSTEMS, INC. AND ITS AFFILIATED COMPANIES IN ANY */
/* JURISDICTION, INCLUDING BUT NOT LIMITED TO CLAIMS FOR DAMAGES OR */
/* EQUITABLE RELIEF BASED ON LOSS OF DATA, AND SPECIFICALLY WAIVES EVEN */
/* UNKNOWN OR UNANTICIPATED CLAIMS OR LOSSES, PRESENT AND FUTURE. */
/*  */
/* IN NO EVENT WILL SUN MICROSYSTEMS, INC. OR ANY OF ITS AFFILIATED */
/* COMPANIES BE LIABLE FOR ANY LOST REVENUE OR PROFITS OR OTHER SPECIAL, */
/* INDIRECT AND CONSEQUENTIAL DAMAGES, EVEN IF IT HAS BEEN ADVISED OF THE */
/* POSSIBILITY OF SUCH DAMAGES. */
/*  */
/* This file is provided with no support and without any obligation on the */
/* part of Sun Microsystems, Inc. ("Sun") or any of its affiliated */
/* companies to assist in its use, correction, modification or */
/* enhancement.  Nevertheless, and without creating any obligation on its */
/* part, Sun welcomes your comments concerning the software and requests */
/* that they be sent to fdlibm-comments@sunpro.sun.com. */

	/* sinh(log(2*max)chopped) is finite */
sinhs n nb x 42b2d4fc 7f7fffec
sinhs n nb x c2b2d4fc ff7fffec
sinhs z nb x 42b2d4fc 7f7fffec
sinhs z nb x c2b2d4fc ff7fffec
sinhs p nb x 42b2d4fc 7f7fffec
sinhs p nb x c2b2d4fc ff7fffec
sinhs m nb x 42b2d4fc 7f7fffec
sinhs m nb x c2b2d4fc ff7fffec
	/* sinh(tiny) :=: tiny */
sinhs n eq x 31000000 31000000
sinhs n eq x b1000000 b1000000
sinhs n eq x 00800000 00800000
sinhs n eq x 80800000 80800000
	/* sinh(+-0) = +-0 */
sinhs n eq - 00000000 00000000
sinhs n eq - 80000000 80000000
sinhs z eq - 00000000 00000000
sinhs z eq - 80000000 80000000
sinhs p eq - 00000000 00000000
sinhs p eq - 80000000 80000000
sinhs m eq - 00000000 00000000
sinhs m eq - 80000000 80000000
	/* random arguments between -30 30 */
sinhs n nb x c100fae6 c4c61436
sinhs n nb x 41aaef28 4e62df7c
sinhs n nb x 41d34ed8 5209d175
sinhs n nb x c1a04908 cd6fbe4d
sinhs n nb x c1020897 c4d38f58
sinhs n nb x bf96e191 bfbc54d1
sinhs n nb x 418accaf 4b82c03a
sinhs n nb x 41cd38a2 5180cc07
sinhs n nb x c0a15899 c29ac86b
sinhs n nb x c1e5ed02 d3b09225
	/* sinh(nan) is nan */
sinhs n uo v ff800001 00000000
sinhs n uo - 7fc00000 00000000
sinhs n uo v ff800001 00000000
sinhs n uo - 7fc00000 00000000
sinhs n uo v ff800001 00000000
sinhs n uo - 7fc00000 00000000
sinhs n uo v ff800001 00000000
sinhs n uo - 7fc00000 00000000
	/* sinh(+-inf) is inf */
sinhs n eq - 7f800000 7f800000
sinhs n eq - ff800000 ff800000
sinhs z eq - 7f800000 7f800000
sinhs z eq - ff800000 ff800000
sinhs p eq - 7f800000 7f800000
sinhs p eq - ff800000 ff800000
sinhs m eq - 7f800000 7f800000
sinhs m eq - ff800000 ff800000
	/* sinh(+-subnormal) */
sinhs n eq x 00000001 00000001
sinhs n eq x 80000001 80000001
	/* sinh overflow threshold */
sinhs n eq ox 42b2d4fd 7f800000
sinhs n eq ox c2b2d4fd ff800000
sinhs z eq ox 42b2d4fd 7f7fffff
sinhs z eq ox c2b2d4fd ff7fffff
sinhs p eq ox 42b2d4fd 7f800000
sinhs p eq ox c2b2d4fd ff7fffff
sinhs m eq ox 42b2d4fd 7f7fffff
sinhs m eq ox c2b2d4fd ff800000
sinhs m eq x 00800000 00800000
sinhs m eq x 00800001 00800001
sinhs m eq x 00800002 00800002
sinhs m eq x 01000000 01000000
sinhs m eq x 01800000 01800000
sinhs m eq x 32800000 32800000
sinhs m eq x 32c00000 32c00000
sinhs m eq x 33000000 33000000
sinhs m eq x 33200000 33200000
sinhs m eq x 39c00000 39c00000
sinhs m eq x 80800000 80800000
sinhs m eq x 80800001 80800001
sinhs m eq x 80800002 80800002
sinhs m eq x 81000000 81000000
sinhs m eq x 81800000 81800000
sinhs m eq x?u 00000001 00000001
sinhs m eq x?u 00000002 00000002
sinhs m eq x?u 00200000 00200000
sinhs m eq x?u 00400000 00400000
sinhs m eq x?u 007ffffe 007ffffe
sinhs m eq x?u 007fffff 007fffff
sinhs m eq x?u 80000001 80000001
sinhs m eq x?u 80000002 80000002
sinhs m eq x?u 80200000 80200000
sinhs m eq x?u 80400000 80400000
sinhs m eq x?u 807ffffe 807ffffe
sinhs m eq x?u 807fffff 807fffff
sinhs m eq xo 42b2d4fd 7f7fffff
sinhs m eq xo 42b30000 7f7fffff
sinhs m eq xo 4431a000 7f7fffff
sinhs m eq xo 7e800000 7f7fffff
sinhs m eq xo 7f000000 7f7fffff
sinhs m eq xo 7f7ffffe 7f7fffff
sinhs m eq xo 7f7fffff 7f7fffff
sinhs m eq xo c2b2d4fd ff800000
sinhs m eq xo c2b30000 ff800000
sinhs m eq xo c431a000 ff800000
sinhs m eq xo fe800000 ff800000
sinhs m eq xo ff000000 ff800000
sinhs m eq xo ff7ffffe ff800000
sinhs m eq xo ff7fffff ff800000
sinhs m uo - 7fff0000 7fff0000
sinhs m uo - ffff0000 ffff0000
sinhs m uo v 7f810000 7fff0000
sinhs m uo v ff810000 ffff0000
sinhs m vn x b2800000 b2800000
sinhs m vn x b2c00000 b2c00001
sinhs m vn x b3000000 b3000000
sinhs m vn x b3200000 b3200000
sinhs m vn x b9c00000 b9c00001
sinhs n eq x 00800001 00800001
sinhs n eq x 00800002 00800002
sinhs n eq x 01000000 01000000
sinhs n eq x 01800000 01800000
sinhs n eq x 32800000 32800000
sinhs n eq x 32c00000 32c00000
sinhs n eq x 33000000 33000000
sinhs n eq x 33200000 33200000
sinhs n eq x 39c00000 39c00000
sinhs n eq x 80800001 80800001
sinhs n eq x 80800002 80800002
sinhs n eq x 81000000 81000000
sinhs n eq x 81800000 81800000
sinhs n eq x b2800000 b2800000
sinhs n eq x b2c00000 b2c00000
sinhs n eq x b3000000 b3000000
sinhs n eq x b3200000 b3200000
sinhs n eq x b9c00000 b9c00000
sinhs n eq x?u 00000002 00000002
sinhs n eq x?u 00200000 00200000
sinhs n eq x?u 00400000 00400000
sinhs n eq x?u 007ffffe 007ffffe
sinhs n eq x?u 007fffff 007fffff
sinhs n eq x?u 80000002 80000002
sinhs n eq x?u 80200000 80200000
sinhs n eq x?u 80400000 80400000
sinhs n eq x?u 807ffffe 807ffffe
sinhs n eq x?u 807fffff 807fffff
sinhs n eq xo 42b2d4fd 7f800000
sinhs n eq xo 42b30000 7f800000
sinhs n eq xo 4431a000 7f800000
sinhs n eq xo 7e800000 7f800000
sinhs n eq xo 7f000000 7f800000
sinhs n eq xo 7f7ffffe 7f800000
sinhs n eq xo 7f7fffff 7f800000
sinhs n eq xo c2b2d4fd ff800000
sinhs n eq xo c2b30000 ff800000
sinhs n eq xo c431a000 ff800000
sinhs n eq xo fe800000 ff800000
sinhs n eq xo ff000000 ff800000
sinhs n eq xo ff7ffffe ff800000
sinhs n eq xo ff7fffff ff800000
sinhs n uo - 7fc00000 00000000
sinhs n uo - 7fff0000 7fff0000
sinhs n uo - ffff0000 ffff0000
sinhs n uo v 7f810000 7fff0000
sinhs n uo v ff800001 00000000
sinhs n uo v ff810000 ffff0000
sinhs p eq x 32c00000 32c00001
sinhs p eq x 39c00000 39c00001
sinhs p eq x 80800000 80800000
sinhs p eq x 80800001 80800001
sinhs p eq x 80800002 80800002
sinhs p eq x 81000000 81000000
sinhs p eq x 81800000 81800000
sinhs p eq x?u 80000001 80000001
sinhs p eq x?u 80000002 80000002
sinhs p eq x?u 80200000 80200000
sinhs p eq x?u 80400000 80400000
sinhs p eq x?u 807ffffe 807ffffe
sinhs p eq x?u 807fffff 807fffff
sinhs p eq xo 42b2d4fd 7f800000
sinhs p eq xo 42b30000 7f800000
sinhs p eq xo 4431a000 7f800000
sinhs p eq xo 7e800000 7f800000
sinhs p eq xo 7f000000 7f800000
sinhs p eq xo 7f7ffffe 7f800000
sinhs p eq xo 7f7fffff 7f800000
sinhs p eq xo c2b2d4fd ff7fffff
sinhs p eq xo c2b30000 ff7fffff
sinhs p eq xo c431a000 ff7fffff
sinhs p eq xo fe800000 ff7fffff
sinhs p eq xo ff000000 ff7fffff
sinhs p eq xo ff7ffffe ff7fffff
sinhs p eq xo ff7fffff ff7fffff
sinhs p uo - 7fff0000 7fff0000
sinhs p uo - ffff0000 ffff0000
sinhs p uo v 7f810000 7fff0000
sinhs p uo v ff810000 ffff0000
sinhs p vn x 00800000 00800000
sinhs p vn x 00800001 00800001
sinhs p vn x 00800002 00800002
sinhs p vn x 01000000 01000000
sinhs p vn x 01800000 01800000
sinhs p vn x 32800000 32800000
sinhs p vn x 33000000 33000000
sinhs p vn x 33200000 33200000
sinhs p vn x b2800000 b2800000
sinhs p vn x b2c00000 b2c00000
sinhs p vn x b3000000 b3000000
sinhs p vn x b3200000 b3200000
sinhs p vn x b9c00000 b9c00000
sinhs p vn x?u 00000001 00000001
sinhs p vn x?u 00000002 00000002
sinhs p vn x?u 00200000 00200000
sinhs p vn x?u 00400000 00400000
sinhs p vn x?u 007ffffe 007ffffe
sinhs p vn x?u 007fffff 007fffff
sinhs z eq x 00800000 00800000
sinhs z eq x 00800001 00800001
sinhs z eq x 00800002 00800002
sinhs z eq x 01000000 01000000
sinhs z eq x 01800000 01800000
sinhs z eq x 32800000 32800000
sinhs z eq x 32c00000 32c00000
sinhs z eq x 33000000 33000000
sinhs z eq x 33200000 33200000
sinhs z eq x 39c00000 39c00000
sinhs z eq x 80800000 80800000
sinhs z eq x 80800001 80800001
sinhs z eq x 80800002 80800002
sinhs z eq x 81000000 81000000
sinhs z eq x 81800000 81800000
sinhs z eq x?u 00000001 00000001
sinhs z eq x?u 00000002 00000002
sinhs z eq x?u 00200000 00200000
sinhs z eq x?u 00400000 00400000
sinhs z eq x?u 007ffffe 007ffffe
sinhs z eq x?u 007fffff 007fffff
sinhs z eq x?u 80000001 80000001
sinhs z eq x?u 80000002 80000002
sinhs z eq x?u 80200000 80200000
sinhs z eq x?u 80400000 80400000
sinhs z eq x?u 807ffffe 807ffffe
sinhs z eq x?u 807fffff 807fffff
sinhs z eq xo 42b2d4fd 7f7fffff
sinhs z eq xo 42b30000 7f7fffff
sinhs z eq xo 4431a000 7f7fffff
sinhs z eq xo 7e800000 7f7fffff
sinhs z eq xo 7f000000 7f7fffff
sinhs z eq xo 7f7ffffe 7f7fffff
sinhs z eq xo 7f7fffff 7f7fffff
sinhs z eq xo c2b2d4fd ff7fffff
sinhs z eq xo c2b30000 ff7fffff
sinhs z eq xo c431a000 ff7fffff
sinhs z eq xo fe800000 ff7fffff
sinhs z eq xo ff000000 ff7fffff
sinhs z eq xo ff7ffffe ff7fffff
sinhs z eq xo ff7fffff ff7fffff
sinhs z uo - 7fff0000 7fff0000
sinhs z uo - ffff0000 ffff0000
sinhs z uo v 7f810000 7fff0000
sinhs z uo v ff810000 ffff0000
sinhs z vn x b2800000 b2800000
sinhs z vn x b2c00000 b2c00000
sinhs z vn x b3000000 b3000000
sinhs z vn x b3200000 b3200000
sinhs z vn x b9c00000 b9c00000
