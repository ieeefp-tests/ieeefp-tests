#include "libctest.h"

#define NAME testsqrts_
#define TYPE SINGLE

#if defined(__STDC__) && !defined(NO_FUNCF)
extern float sqrtf(float);
#define FUNC(X) sqrtf(X)
#else
extern double sqrt();
#define FUNC(X) sqrt((double)(X))
#endif

#include "testfunc1.c"
