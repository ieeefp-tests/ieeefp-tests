#include "libctest.h"

#define NAME testsins_
#define TYPE SINGLE

#if defined(__STDC__) && !defined(NO_FUNCF)
extern float sinf(float);
#define FUNC(X) sinf(X)
#else
extern double sin();
#define FUNC(X) sin((double)(X))
#endif

#include "testfunc1.c"
