#include "libctest.h"

#define NAME testcoss_
#define TYPE SINGLE

#if defined(__STDC__) && !defined(NO_FUNCF)
extern float cosf(float);
#define FUNC(X) cosf(X)
#else
extern double cos();
#define FUNC(X) cos((double)(X))
#endif

#include "testfunc1.c"
