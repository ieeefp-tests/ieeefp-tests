#include "libctest.h"

#define NAME testatan2d_
#define TYPE DOUBLE
#ifdef ANSI_PROTOTYPES
extern double atan2(double,double);
#else
extern double atan2();
#endif
#define FUNC(X,Y) atan2(X,Y)

#include "testfunc2.c"
