#include "libctest.h"

#define NAME testasinx_
#define TYPE EXTENDED
extern long double asinl(long double);
#define FUNC(X) asinl(X)

#include "testfunc1.c"
