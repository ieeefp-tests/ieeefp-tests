#include "libctest.h"

#define NAME testtand_
#define TYPE DOUBLE
#ifdef ANSI_PROTOTYPES
extern double tan(double);
#else
extern double tan();
#endif
#define FUNC(X) tan(X)

#include "testfunc1.c"
