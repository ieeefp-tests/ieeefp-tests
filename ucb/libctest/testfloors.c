#include "libctest.h"

#define NAME testfloors_
#define TYPE SINGLE

#if defined(__STDC__) && !defined(NO_FUNCF) && !defined(__hpux)
extern float floorf(float);
#define FUNC(X) floorf(X)
#else
extern double floor();
#define FUNC(X) floor((double)(X))
#endif

#include "testfunc1.c"
