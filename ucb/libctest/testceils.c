#include "libctest.h"

#define NAME testceils_
#define TYPE SINGLE

#if defined(__STDC__) && !defined(NO_FUNCF) && !defined(__hpux)
extern float ceilf(float);
#define FUNC(X) ceilf(X)
#else
extern double ceil();
#define FUNC(X) ceil((double)(X))
#endif

#include "testfunc1.c"
