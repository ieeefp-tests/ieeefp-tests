#include "libctest.h"

#define NAME testceild_
#define TYPE DOUBLE
#ifdef ANSI_PROTOTYPES
extern double ceil(double);
#else
extern double ceil();
#endif
#define FUNC(X) ceil(X)

#include "testfunc1.c"
