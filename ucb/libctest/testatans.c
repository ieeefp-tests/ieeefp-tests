#include "libctest.h"

#define NAME testatans_
#define TYPE SINGLE

#if defined(__STDC__) && !defined(NO_FUNCF)
extern float atanf(float);
#define FUNC(X) atanf(X)
#else
extern double atan();
#define FUNC(X) atan((double)(X))
#endif

#include "testfunc1.c"
