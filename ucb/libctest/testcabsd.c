#include "libctest.h"

#if (sunos_version/100 >= 5) || defined(NO_CABS)

#define NAME testcabsd_
#define TYPE DOUBLE
#define TYPECOMPLEX DOUBLECOMPLEX
#ifdef ANSI_PROTOTYPES
extern double hypot( double, double);
#else
extern double hypot();
#endif
#define FUNC(X) hypot((X).real,(X).imag)
#include "testcabs.c"

#else

#define NAME testcabsd_
#define TYPE DOUBLE
#define TYPECOMPLEX DOUBLECOMPLEX
#ifdef ANSI_PROTOTYPES
extern double cabs( struct doublecomplex );
#else
extern double cabs();
#endif
#define FUNC(X) cabs(X)
#include "testcabs.c"

#endif
