#include "libctest.h"

#define NAME testatan2x_
#define TYPE EXTENDED
extern long double atan2l(long double, long double);
#define FUNC(X,Y) atan2l(X,Y)

#include "testfunc2.c"
