#include "libctest.h"

#define NAME testlogs_
#define TYPE SINGLE

#if defined(__STDC__) && !defined(NO_FUNCF)
extern float logf(float);
#define FUNC(X) logf(X)
#else
extern double log();
#define FUNC(X) log((double)(X))
#endif

#include "testfunc1.c"
