#include "libctest.h"

#define NAME testasins_
#define TYPE SINGLE

#if defined(__STDC__) && !defined(NO_FUNCF)
extern float asinf(float);
#define FUNC(X) asinf(X)
#else
extern double asin();
#define FUNC(X) asin((double)(X))
#endif

#include "testfunc1.c"
