#include "libctest.h"

#define NAME testcoshd_
#define TYPE DOUBLE
#ifdef ANSI_PROTOTYPES
extern double cosh(double);
#else
extern double cosh();
#endif
#define FUNC(X) cosh(X)

#include "testfunc1.c"
