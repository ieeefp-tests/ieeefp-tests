#include "libctest.h"

#define NAME testacoss_
#define TYPE SINGLE

#if defined(__STDC__) && !defined(NO_FUNCF)
extern float acosf(float);
#define FUNC(X) acosf(X)
#else
extern double acos();
#define FUNC(X) acos((double)(X))
#endif

#include "testfunc1.c"
