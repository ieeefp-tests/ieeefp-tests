#include "libctest.h"

#define NAME testasind_
#define TYPE DOUBLE
#ifdef ANSI_PROTOTYPES
extern double asin(double);
#else
extern double asin();
#endif
#define FUNC(X) asin(X)

#include "testfunc1.c"
