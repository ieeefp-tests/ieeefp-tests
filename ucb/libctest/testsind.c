#include "libctest.h"

#define NAME testsind_
#define TYPE DOUBLE
#ifdef ANSI_PROTOTYPES
extern double sin(double);
#else
extern double sin();
#endif
#define FUNC(X) sin(X)

#include "testfunc1.c"
