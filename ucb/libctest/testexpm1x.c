#include "libctest.h"

#define NAME testexpm1x_
#define TYPE EXTENDED

#if defined(__hpux) || defined(_AUX)

extern long double expl(long double);
#define FUNC(X) (expl(X)-1)

#else

extern long double expm1l(long double);
#define FUNC(X) expm1l(X)

#endif

#include "testfunc1.c"
