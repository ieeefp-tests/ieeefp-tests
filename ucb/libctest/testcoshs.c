#include "libctest.h"

#define NAME testcoshs_
#define TYPE SINGLE

#if defined(__STDC__) && !defined(NO_FUNCF)
extern float coshf(float);
#define FUNC(X) coshf(X)
#else
extern double cosh();
#define FUNC(X) cosh((double)(X))
#endif

#include "testfunc1.c"
