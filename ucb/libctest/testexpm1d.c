#include "libctest.h"

#define NAME testexpm1d_
#define TYPE DOUBLE

#if defined(__hpux) || defined(_AUX)

#ifdef ANSI_PROTOTYPES
extern double exp(double);
#else
extern double exp();
#endif
#define FUNC(X) (exp(X)-1)

#else

#ifdef ANSI_PROTOTYPES
extern double expm1(double);
#else
extern double expm1();
#endif
#define FUNC(X) expm1(X)

#endif

#include "testfunc1.c"
