#include "libctest.h"

#define NAME testhypotd_
#define TYPE DOUBLE
#ifdef ANSI_PROTOTYPES
extern double hypot(double,double);
#else
extern double hypot();
#endif
#define FUNC(X,Y) hypot(X,Y)

#include "testfunc2.c"
