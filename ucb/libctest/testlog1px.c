#include "libctest.h"

#define NAME testlog1px_
#define TYPE EXTENDED
extern long double log1pl(long double);
#define FUNC(X) log1pl(X)

#include "testfunc1.c"
