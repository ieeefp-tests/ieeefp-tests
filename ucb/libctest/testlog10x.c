#include "libctest.h"

#define NAME testlog10x_
#define TYPE EXTENDED
extern long double log10l(long double);
#define FUNC(X) log10l(X)

#include "testfunc1.c"
