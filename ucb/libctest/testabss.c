#include "libctest.h"

#define NAME testabss_
#define TYPE SINGLE

#if defined(__STDC__) && !defined(NO_FUNCF)
extern float fabsf(float);
#define FUNC(X) fabsf(X)
#else
extern double fabs();
#define FUNC(X) fabs((double)(X))
#endif

#include "testfunc1.c"
