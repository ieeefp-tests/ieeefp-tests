#include "libctest.h"

#define NAME testsqrtx_
#define TYPE EXTENDED
extern long double sqrtl(long double);
#define FUNC(X) sqrtl(X)

#include "testfunc1.c"
