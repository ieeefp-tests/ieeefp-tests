#include "libctest.h"

#define NAME testatand_
#define TYPE DOUBLE
#ifdef ANSI_PROTOTYPES
extern double atan(double);
#else
extern double atan();
#endif
#define FUNC(X) atan(X)

#include "testfunc1.c"
