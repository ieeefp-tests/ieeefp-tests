#include "libctest.h"

#define NAME testpowd_
#define TYPE DOUBLE
#ifdef ANSI_PROTOTYPES
extern double pow(double,double);
#else
extern double pow();
#endif
#define FUNC(X,Y) pow(X,Y)

#include "testfunc2.c"
