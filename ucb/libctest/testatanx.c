#include "libctest.h"

#define NAME testatanx_
#define TYPE EXTENDED
extern long double atanl(long double);
#define FUNC(X) atanl(X)

#include "testfunc1.c"
