#include "libctest.h"

#define NAME testlog1ps_
#define TYPE SINGLE

#if defined(_AUX)

#if defined(__STDC__) && !defined(NO_FUNCF)
extern float logf(float);
#define FUNC(X) logf(1+X)
#else
extern double log();
#define FUNC(X) log((double)(1+X))
#endif

#else

#if defined(__STDC__) && !defined(NO_FUNCF) && !defined(__hpux)
extern float log1pf(float);
#define FUNC(X) log1pf(X)
#else
extern double log1p();
#define FUNC(X) log1p((double)(X))
#endif

#endif

#include "testfunc1.c"
