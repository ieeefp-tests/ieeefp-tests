#include "libctest.h"

#define NAME testlog1pd_
#define TYPE DOUBLE

#if defined(_AUX)

#ifdef ANSI_PROTOTYPES
extern double log(double);
#else
extern double log();
#endif
#define FUNC(X) log(1+X)


#else

#ifdef ANSI_PROTOTYPES
extern double log1p(double);
#else
extern double log1p();
#endif
#define FUNC(X) log1p(X)

#endif

#include "testfunc1.c"
