#include "libctest.h"

#define NAME testcabsx_
#define TYPE EXTENDED
#define TYPECOMPLEX EXTENDEDCOMPLEX

extern long double hypotl( long double, long double );
#define FUNC(X) hypotl((X).real, (X).imag)

#include "testcabs.c"
