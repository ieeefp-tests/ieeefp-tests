#include "libctest.h"

#define NAME testlogd_
#define TYPE DOUBLE
#ifdef ANSI_PROTOTYPES
extern double log(double);
#else
extern double log();
#endif
#define FUNC(X) log(X)

#include "testfunc1.c"
