#include "libctest.h"

#define NAME testabsx_
#define TYPE EXTENDED
extern long double fabsl(long double);
#define FUNC(X) fabsl(X)

#include "testfunc1.c"
