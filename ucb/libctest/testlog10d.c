#include "libctest.h"

#define NAME testlog10d_
#define TYPE DOUBLE
#ifdef ANSI_PROTOTYPES
extern double log10(double);
#else
extern double log10();
#endif
#define FUNC(X) log10(X)

#include "testfunc1.c"
