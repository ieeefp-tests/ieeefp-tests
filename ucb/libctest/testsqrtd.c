#include "libctest.h"

#define NAME testsqrtd_
#define TYPE DOUBLE
#ifdef ANSI_PROTOTYPES
extern double sqrt(double);
#else
extern double sqrt();
#endif
#define FUNC(X) sqrt(X)

#include "testfunc1.c"
