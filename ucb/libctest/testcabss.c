#include "libctest.h"

#define NAME testcabss_
#define TYPE SINGLE
#define TYPECOMPLEX SINGLECOMPLEX

#if (sunos_version/100 >= 5) || defined(NO_CABS)

#if defined(__STDC__) && !defined(NO_FUNCF)
extern float hypotf(float,float);
#define FUNC(X) hypotf((X).real,(X).imag)
#else
extern double hypot();
#define FUNC(X) hypot((double)(X).real,(double)(X).imag)
#endif

#else

extern double cabs();
float cabsf(fc)  
struct singlecomplex fc ;
{
struct doublecomplex dc ;
dc.real=fc.real; dc.imag=fc.imag;
return (float) cabs(dc);
}
#define FUNC(X) cabsf(X)

#endif

#include "testcabs.c"
