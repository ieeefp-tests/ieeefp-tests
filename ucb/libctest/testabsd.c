#include "libctest.h"

#define NAME testabsd_
#define TYPE DOUBLE
#ifdef ANSI_PROTOTYPES
extern double fabs(double);
#else
extern double fabs();
#endif
#define FUNC(X) fabs(X)

#include "testfunc1.c"
