#include "libctest.h"

#define NAME testexpd_
#define TYPE DOUBLE
#ifdef ANSI_PROTOTYPES
extern double exp(double);
#else
extern double exp();
#endif
#define FUNC(X) exp(X)

#include "testfunc1.c"
