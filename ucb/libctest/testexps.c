#include "libctest.h"

#define NAME testexps_
#define TYPE SINGLE

#if defined(__STDC__) && !defined(NO_FUNCF)
extern float expf(float);
#define FUNC(X) expf(X)
#else
extern double exp();
#define FUNC(X) exp((double)(X))
#endif

#include "testfunc1.c"
