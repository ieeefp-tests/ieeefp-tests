#include "libctest.h"

#define NAME testsinhs_
#define TYPE SINGLE

#if defined(__STDC__) && !defined(NO_FUNCF)
extern float sinhf(float);
#define FUNC(X) sinhf(X)
#else
extern double sinh();
#define FUNC(X) sinh((double)(X))
#endif

#include "testfunc1.c"
