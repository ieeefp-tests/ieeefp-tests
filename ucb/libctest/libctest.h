#undef SINGLE
#undef DOUBLE
#undef EXTENDED

#ifdef __STDC__
#define ANSI_PROTOTYPES
#define PROTO(p)  p
#else
#undef ANSI_PROTOTYPES
#define PROTO(p)  ()
#endif

/*	INTEGER is the C equivalent of a Fortran INTEGER */

#define INTEGERTYPE int
#define SINGLETYPE float
#define DOUBLETYPE double
#define EXTENDEDTYPE long double
#define INTEGER INTEGERTYPE *
#define SINGLE SINGLETYPE *
#define DOUBLE DOUBLETYPE *
#define EXTENDED EXTENDEDTYPE *

struct singlecomplex { float real, imag ; } ;
struct doublecomplex { double real, imag ; } ;
#ifdef __STDC__
struct extendedcomplex { long double real, imag ; } ;
#else
struct extendedcomplex { double real, imag ; } ;
#endif

#define SINGLECOMPLEX struct singlecomplex *
#define DOUBLECOMPLEX struct doublecomplex *
#define EXTENDEDCOMPLEX struct extendedcomplex *

#ifdef sunos_version
#if (sunos_version/100 <= 4)
#undef NO_CABS
#else
#define NO_CABS
#endif
#endif

#if defined(linux) || defined(_AUX)
#define NO_CABS
#endif

extern void testsignifs_ PROTO((INTEGER));
extern void testsignifd_ PROTO((INTEGER));
extern void testsignifx_ PROTO((INTEGER));

/* basic functions */

extern void testadds_ PROTO((SINGLE,SINGLE,SINGLE));
extern void testaddd_ PROTO((DOUBLE,DOUBLE,DOUBLE));
extern void testaddx_ PROTO((EXTENDED,EXTENDED,EXTENDED));
extern void testsubs_ PROTO((SINGLE,SINGLE,SINGLE));
extern void testsubd_ PROTO((DOUBLE,DOUBLE,DOUBLE));
extern void testsubx_ PROTO((EXTENDED,EXTENDED,EXTENDED));

extern void testtoints_ PROTO((SINGLE,SINGLE));
extern void testtointd_ PROTO((DOUBLE,DOUBLE));
extern void testtointx_ PROTO((EXTENDED,EXTENDED));
extern void testtosingles_ PROTO((SINGLE,SINGLE));
extern void testtosingled_ PROTO((DOUBLE,DOUBLE));
extern void testtosinglex_ PROTO((EXTENDED,EXTENDED));
extern void testtodoubles_ PROTO((SINGLE,SINGLE));
extern void testtodoubled_ PROTO((DOUBLE,DOUBLE));
extern void testtodoublex_ PROTO((EXTENDED,EXTENDED));
extern void testtoexts_ PROTO((SINGLE,SINGLE));
extern void testtoextd_ PROTO((DOUBLE,DOUBLE));
extern void testtoextx_ PROTO((EXTENDED,EXTENDED));

/* functions needed for ucb{mul,div,sqr}test */

extern void testdivs_ PROTO((SINGLE,SINGLE,SINGLE));
extern void testdivd_ PROTO((DOUBLE,DOUBLE,DOUBLE));
extern void testdivx_ PROTO((EXTENDED,EXTENDED,EXTENDED));
extern void testmuls_ PROTO((SINGLE,SINGLE,SINGLE));
extern void testmuld_ PROTO((DOUBLE,DOUBLE,DOUBLE));
extern void testmulx_ PROTO((EXTENDED,EXTENDED,EXTENDED));
extern void testsqrts_ PROTO((SINGLE,SINGLE));
extern void testsqrtd_ PROTO((DOUBLE,DOUBLE));
extern void testsqrtx_ PROTO((EXTENDED,EXTENDED));

/* functions needed for ucbeeftest */

extern void testsins_ PROTO((SINGLE,SINGLE));
extern void testsind_ PROTO((DOUBLE,DOUBLE));
extern void testsinx_ PROTO((EXTENDED,EXTENDED));
extern void testcoss_ PROTO((SINGLE,SINGLE));
extern void testcosd_ PROTO((DOUBLE,DOUBLE));
extern void testcosx_ PROTO((EXTENDED,EXTENDED));
extern void testatans_ PROTO((SINGLE,SINGLE));
extern void testatand_ PROTO((DOUBLE,DOUBLE));
extern void testatanx_ PROTO((EXTENDED,EXTENDED));
extern void testexps_ PROTO((SINGLE,SINGLE));
extern void testexpd_ PROTO((DOUBLE,DOUBLE));
extern void testexpx_ PROTO((EXTENDED,EXTENDED));
extern void testexpm1s_ PROTO((SINGLE,SINGLE));
extern void testexpm1d_ PROTO((DOUBLE,DOUBLE));
extern void testexpm1x_ PROTO((EXTENDED,EXTENDED));
extern void testlogs_ PROTO((SINGLE,SINGLE));
extern void testlogd_ PROTO((DOUBLE,DOUBLE));
extern void testlogx_ PROTO((EXTENDED,EXTENDED));
extern void testlog1ps_ PROTO((SINGLE,SINGLE));
extern void testlog1pd_ PROTO((DOUBLE,DOUBLE));
extern void testlog1px_ PROTO((EXTENDED,EXTENDED));

/* functions needed for ucblibtest */

extern void testabss_ PROTO((SINGLE,SINGLE));
extern void testabsd_ PROTO((DOUBLE,DOUBLE));
extern void testabsx_ PROTO((EXTENDED,EXTENDED));

extern void testceils_ PROTO((SINGLE,SINGLE));
extern void testceild_ PROTO((DOUBLE,DOUBLE));
extern void testceilx_ PROTO((EXTENDED,EXTENDED));
extern void testfloors_ PROTO((SINGLE,SINGLE));
extern void testfloord_ PROTO((DOUBLE,DOUBLE));
extern void testfloorx_ PROTO((EXTENDED,EXTENDED));

extern void testmods_ PROTO((SINGLE,SINGLE,SINGLE));
extern void testmodd_ PROTO((DOUBLE,DOUBLE,DOUBLE));
extern void testmodx_ PROTO((EXTENDED,EXTENDED,EXTENDED));

extern void testcoshs_ PROTO((SINGLE,SINGLE));
extern void testcoshd_ PROTO((DOUBLE,DOUBLE));
extern void testcoshx_ PROTO((EXTENDED,EXTENDED));
extern void testsinhs_ PROTO((SINGLE,SINGLE));
extern void testsinhd_ PROTO((DOUBLE,DOUBLE));
extern void testsinhx_ PROTO((EXTENDED,EXTENDED));
extern void testtanhs_ PROTO((SINGLE,SINGLE));
extern void testtanhd_ PROTO((DOUBLE,DOUBLE));
extern void testtanhx_ PROTO((EXTENDED,EXTENDED));

extern void testlog10s_ PROTO((SINGLE,SINGLE));
extern void testlog10d_ PROTO((DOUBLE,DOUBLE));
extern void testlog10x_ PROTO((EXTENDED,EXTENDED));

extern void testtans_ PROTO((SINGLE,SINGLE));
extern void testtand_ PROTO((DOUBLE,DOUBLE));
extern void testtanx_ PROTO((EXTENDED,EXTENDED));

extern void testacoss_ PROTO((SINGLE,SINGLE));
extern void testacosd_ PROTO((DOUBLE,DOUBLE));
extern void testacosx_ PROTO((EXTENDED,EXTENDED));
extern void testasins_ PROTO((SINGLE,SINGLE));
extern void testasind_ PROTO((DOUBLE,DOUBLE));
extern void testasinx_ PROTO((EXTENDED,EXTENDED));
extern void testatans_ PROTO((SINGLE,SINGLE));
extern void testatand_ PROTO((DOUBLE,DOUBLE));
extern void testatanx_ PROTO((EXTENDED,EXTENDED));

extern void testatan2s_ PROTO((SINGLE,SINGLE,SINGLE));
extern void testatan2d_ PROTO((DOUBLE,DOUBLE,DOUBLE));
extern void testatan2x_ PROTO((EXTENDED,EXTENDED,EXTENDED));

extern void testcabss_ PROTO((SINGLE,SINGLECOMPLEX));
extern void testcabsd_ PROTO((DOUBLE,DOUBLECOMPLEX));
extern void testcabsx_ PROTO((EXTENDED,EXTENDEDCOMPLEX));

extern void testhypots_ PROTO((SINGLE,SINGLE,SINGLE));
extern void testhypotd_ PROTO((DOUBLE,DOUBLE,DOUBLE));
extern void testhypotx_ PROTO((EXTENDED,EXTENDED,EXTENDED));

extern void testpows_ PROTO((SINGLE,SINGLE,SINGLE));
extern void testpowd_ PROTO((DOUBLE,DOUBLE,DOUBLE));
extern void testpowx_ PROTO((EXTENDED,EXTENDED,EXTENDED));

