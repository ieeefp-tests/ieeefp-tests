#include "libctest.h"

#define NAME testfloord_
#define TYPE DOUBLE
#ifdef ANSI_PROTOTYPES
extern double floor(double);
#else
extern double floor();
#endif
#define FUNC(X) floor(X)

#include "testfunc1.c"
