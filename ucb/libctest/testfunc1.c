#ifdef ANSI_PROTOTYPES
void
NAME (TYPE z, TYPE x)
#else
void
NAME (z, x)
	TYPE          z;
	TYPE          x;
#endif
{
	*z = FUNC(*x);
}
