#include "libctest.h"

#define NAME testpows_
#define TYPE SINGLE

#if defined(__STDC__) && !defined(NO_FUNCF)
extern float powf(float,float);
#define FUNC(X,Y) powf(X,Y)
#else
extern double pow();
#define FUNC(X,Y) pow((double)(X),(double)(Y))
#endif

#include "testfunc2.c"
