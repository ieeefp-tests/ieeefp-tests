#include "libctest.h"

#define NAME testlog10s_
#define TYPE SINGLE

#if defined(__STDC__) && !defined(NO_FUNCF)
extern float log10f(float);
#define FUNC(X) log10f(X)
#else
extern double log10();
#define FUNC(X) log10((double)(X))
#endif

#include "testfunc1.c"
