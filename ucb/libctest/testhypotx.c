#include "libctest.h"

#define NAME testhypotx_
#define TYPE EXTENDED
extern long double hypotl(long double, long double);
#define FUNC(X,Y) hypotl(X,Y)

#include "testfunc2.c"
