#include "libctest.h"

#define NAME testsinhd_
#define TYPE DOUBLE
#ifdef ANSI_PROTOTYPES
extern double sinh(double);
#else
extern double sinh();
#endif
#define FUNC(X) sinh(X)

#include "testfunc1.c"
