#include "libctest.h"

#define NAME testacosx_
#define TYPE EXTENDED
extern long double acosl(long double);
#define FUNC(X) acosl(X)

#include "testfunc1.c"
