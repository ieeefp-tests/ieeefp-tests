#include "libctest.h"

#define NAME testcosd_
#define TYPE DOUBLE
#ifdef ANSI_PROTOTYPES
extern double cos(double);
#else
extern double cos();
#endif
#define FUNC(X) cos(X)

#include "testfunc1.c"
