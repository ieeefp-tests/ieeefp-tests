#include "libctest.h"

#define NAME testmodd_
#define TYPE DOUBLE
#ifdef ANSI_PROTOTYPES
extern double fmod(double,double);
#else
extern double fmod();
#endif
#define FUNC(X,Y) fmod(X,Y)

#include "testfunc2.c"
