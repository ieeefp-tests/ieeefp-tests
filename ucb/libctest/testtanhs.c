#include "libctest.h"

#define NAME testtanhs_
#define TYPE SINGLE

#if defined(__STDC__) && !defined(NO_FUNCF)
extern float tanhf(float);
#define FUNC(X) tanhf(X)
#else
extern double tanh();
#define FUNC(X) tanh((double)(X))
#endif

#include "testfunc1.c"
