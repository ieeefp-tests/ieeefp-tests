#ifdef ANSI_PROTOTYPES
void
NAME (TYPE z, TYPE x, TYPE y)
#else
void
NAME (z, x, y)
	TYPE          z;
	TYPE          x;
	TYPE          y;
#endif
{
	*z = *x * *y;
}
