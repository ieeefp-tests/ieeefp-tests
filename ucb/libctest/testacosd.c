#include "libctest.h"

#define NAME testacosd_
#define TYPE DOUBLE
#ifdef ANSI_PROTOTYPES
extern double acos(double);
#else
extern double acos();
#endif
#define FUNC(X) acos(X)

#include "testfunc1.c"
