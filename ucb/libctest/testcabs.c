#ifdef ANSI_PROTOTYPES
void
NAME (TYPE z, TYPECOMPLEX x)
#else
void
NAME (z, x)
	TYPE          z;
	TYPECOMPLEX x;
#endif
{
	*z = FUNC(*x);
}
