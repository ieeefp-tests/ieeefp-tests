#include "libctest.h"

#define NAME testmods_
#define TYPE SINGLE

#if defined(__STDC__) && !defined(NO_FUNCF)
extern float fmodf(float,float);
#define FUNC(X,Y) fmodf(X,Y)
#else
extern double fmod();
#define FUNC(X,Y) fmod((double)(X),(double)(Y))
#endif

#include "testfunc2.c"
