#include "libctest.h"

#define NAME testtanhd_
#define TYPE DOUBLE
#ifdef ANSI_PROTOTYPES
extern double tanh(double);
#else
extern double tanh();
#endif
#define FUNC(X) tanh(X)

#include "testfunc1.c"
