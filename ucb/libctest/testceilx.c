#include "libctest.h"

#define NAME testceilx_
#define TYPE EXTENDED
extern long double ceill(long double);
#define FUNC(X) ceill(X)

#include "testfunc1.c"
