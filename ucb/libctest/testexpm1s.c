#include "libctest.h"

#define NAME testexpm1s_
#define TYPE SINGLE

#if defined(__hpux) || defined(_AUX)

#if defined(__STDC__) && !defined(NO_FUNCF)
extern float expf(float);
#define FUNC(X) (expf(X)-1)
#else
extern double exp();
#define FUNC(X) (exp((double)(X))-1)
#endif

#else

#if defined(__STDC__) && !defined(NO_FUNCF)
extern float expm1f(float);
#define FUNC(X) expm1f(X)
#else
extern double expm1();
#define FUNC(X) expm1((double)(X))
#endif

#endif


#include "testfunc1.c"
