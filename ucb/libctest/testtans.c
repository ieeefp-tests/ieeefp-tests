#include "libctest.h"

#define NAME testtans_
#define TYPE SINGLE

#if defined(__STDC__) && !defined(NO_FUNCF)
extern float tanf(float);
#define FUNC(X) tanf(X)
#else
extern double tan();
#define FUNC(X) tan((double)(X))
#endif

#include "testfunc1.c"
