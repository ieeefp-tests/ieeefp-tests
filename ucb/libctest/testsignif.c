#ifdef ANSI_PROTOTYPES
void NAME ( INTEGER pn )
#else
void NAME ( pn ) INTEGER pn;
#endif

{				/* get number of significant bits */
	INTEGERTYPE            n;
#ifdef __STDC__
	volatile
#endif
	TYPE b, t;

	n = 0;
	b = 1;
	do {
		n++;
		b += b;
		t = b + 1;
		t -= b;
		t -= 1;
	} while (t == 0);
	*pn = n;
}
