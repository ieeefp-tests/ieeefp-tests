#include "libctest.h"

#define NAME testatan2s_
#define TYPE SINGLE

#if defined(__STDC__) && !defined(NO_FUNCF)
extern float atan2f(float,float);
#define FUNC(X,Y) atan2f(X,Y)
#else
extern double atan2();
#define FUNC(X,Y) atan2((double)(X),(double)(Y))
#endif

#include "testfunc2.c"
