#include "libctest.h"

#define NAME testhypots_
#define TYPE SINGLE

#if defined(__STDC__) && !defined(NO_FUNCF) && !defined(__hpux)
extern float hypotf(float,float);
#define FUNC(X,Y) hypotf(X,Y)
#else
extern double hypot();
#define FUNC(X,Y) hypot((double)(X),(double)(Y))
#endif

#include "testfunc2.c"
