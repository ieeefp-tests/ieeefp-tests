@echo off
if [%1] == [] goto msg
if exist RESULTS del RESULTS

echo ================================ >> RESULTS
echo repeatedly find "check"          >> RESULTS
echo ================================ >> RESULTS
echo.                                 >> RESULTS

echo ================================ >> RESULTS
echo float div test   check ========= >> RESULTS
echo ================================ >> RESULTS
echo.                                 >> RESULTS
fdivtest %1                           >> RESULTS
echo.                                 >> RESULTS

echo ================================ >> RESULTS
echo float mul test   check ========= >> RESULTS
echo ================================ >> RESULTS
echo.                                 >> RESULTS
fmultest %1                           >> RESULTS
echo.                                 >> RESULTS

echo ================================ >> RESULTS
echo float sqrt test  check ========= >> RESULTS
echo ================================ >> RESULTS
echo.                                 >> RESULTS
fsqrtest %1                           >> RESULTS
echo.                                 >> RESULTS

echo ================================ >> RESULTS
echo double div test  check ========= >> RESULTS
echo ================================ >> RESULTS
echo.                                 >> RESULTS
ddivtest %1                           >> RESULTS
echo.                                 >> RESULTS

echo ================================ >> RESULTS
echo double mul test  check ========= >> RESULTS
echo ================================ >> RESULTS
echo.                                 >> RESULTS
dmultest %1                           >> RESULTS
echo.                                 >> RESULTS

echo ================================ >> RESULTS
echo double sqrt test check ========= >> RESULTS
echo ================================ >> RESULTS
echo.                                 >> RESULTS
dsqrtest %1                           >> RESULTS
echo.                                 >> RESULTS

echo ================================ >> RESULTS
echo longdouble div test  check ===== >> RESULTS
echo ================================ >> RESULTS
echo.                                 >> RESULTS
wdivtest %1                           >> RESULTS
echo.                                 >> RESULTS

echo ================================ >> RESULTS
echo longdouble mul test  check ===== >> RESULTS
echo ================================ >> RESULTS
echo.                                 >> RESULTS
wmultest %1                           >> RESULTS
echo.                                 >> RESULTS

echo ================================ >> RESULTS
echo longdouble sqrt test check ===== >> RESULTS
echo ================================ >> RESULTS
echo.                                 >> RESULTS
wsqrtest %1                           >> RESULTS
echo.                                 >> RESULTS

echo ================================ >> RESULTS
echo done!            =============== >> RESULTS
echo ================================ >> RESULTS

goto done

:msg
echo ==================================
echo.
echo runall long_int_number_of_tests
echo.
echo results are stored in file RESULTS
echo.
echo ==================================

:done
