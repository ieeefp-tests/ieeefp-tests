/* @(#)s_scalbn.c 1.2 95/01/04 */
/*
 * ====================================================
 * Copyright (C) 1993 by Sun Microsystems, Inc. All rights reserved.
 *
 * Developed at SunPro, a Sun Microsystems, Inc. business.
 * Permission to use, copy, modify, and distribute this
 * software is freely granted, provided that this notice 
 * is preserved.
 * ====================================================
 */

/* 
 * scalbn (double x, int n)
 * scalbn(x,n) returns x* 2**n  computed by  exponent  
 * manipulation rather than by actually performing an 
 * exponentiation or a multiplication.
 */

#include "fdlibm.h"

#ifdef __STDC__
static const double
#else
static double
#endif
two54   =  1.80143985094819840000e+16, /* 0x43500000, 0x00000000 */
twom54  =  5.55111512312578270212e-17, /* 0x3C900000, 0x00000000 */
huge   = 1.0e+300,
tiny   = 1.0e-300;

#ifdef __STDC__
	double scalbn (double x, INT32 n)   /* DOS change */
#else
	double scalbn (x,n)
	double x; INT32 n;  /* DOS change */
#endif
{
	INT32  k,hx,lx;     /* DOS change */
	hx = __HI(x);
	lx = __LO(x);
#ifdef DOS
		  k = (hx&0x7ff00000L)>>20;		/* extract exponent */
#else
		  k = (hx&0x7ff00000)>>20;		/* extract exponent */
#endif
		  if (k==0) {				/* 0 or subnormal x */
#ifdef DOS
				if ((lx|(hx&0x7fffffffL))==0) return x; /* +-0 */
#else
				if ((lx|(hx&0x7fffffff))==0) return x; /* +-0 */
#endif
		 x *= two54;
		 hx = __HI(x);
#ifdef DOS
		 k = ((hx&0x7ff00000L)>>20) - 54;
				if (n< -50000L) return tiny*x; 	/*underflow*/
#else
		 k = ((hx&0x7ff00000)>>20) - 54;
				if (n< -50000) return tiny*x; 	/*underflow*/
#endif
		 }
		  if (k==0x7ff) return x+x;		/* NaN or Inf */
        k = k+n; 
        if (k >  0x7fe) return huge*copysign(huge,x); /* overflow  */
        if (k > 0) 				/* normal result */
#ifdef DOS
		 {__HI(x) = (hx&0x800fffffL)|(k<<20); return x;}
		  if (k <= -54)
				if (n > 50000L) 	/* in case integer overflow in n+k */
#else
		 {__HI(x) = (hx&0x800fffff)|(k<<20); return x;}
		  if (k <= -54)
				if (n > 50000) 	/* in case integer overflow in n+k */
#endif
		return huge*copysign(huge,x);	/*overflow*/
	    else return tiny*copysign(tiny,x); 	/*underflow*/
        k += 54;				/* subnormal result */
#ifdef DOS
		  __HI(x) = (hx&0x800fffffL)|(k<<20);
#else
		  __HI(x) = (hx&0x800fffff)|(k<<20);
#endif
        return x*twom54;
}
