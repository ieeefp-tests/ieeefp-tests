set -e
set -x

### specification section

# sample ucbtest invocation for GCC/F2C on linux Slackware 1.1.59

# temporary directory where everything is to happen

tmpdir=/tmp/linux

# source code directory

SRC=$HOME/ucb

# precision choices

precisions="DP SP"

# language choices

languages="c f"

# compilation options

make="make"
cpp="/lib/cpp -traditional"
cc=/usr/bin/cc
f77=/usr/bin/f77
cppopts="-DNO_FUNCF -DNTESTS=1000000 -DNREGIONS=16 -DQUIET -DNO_FAIL"
opts="-O"
ccopts="-ffloat-store"
ccldopts="-lm"  
f77opts=""
f77ldopts="-lm"  

### execution section

sh $SRC/ucbREADME/log.sh


if test -d $tmpdir ; then : ; else mkdir $tmpdir ; fi

cd $tmpdir

for language in $languages ; do

makedir=ucb${language}test

$make -f $SRC/$makedir/Makefile SRC=$SRC clean

for precision in $precisions ; do

$make -f $SRC/$makedir/Makefile SRC=$SRC PRECISION=${precision} \
CC="$cc" CCOPTS="$opts $ccopts $cppopts" CCLDOPTS="$ccldopts" \
F77="$f77" F77OPTS="$opts $f77opts" F77LDOPTS="$f77ldopts" \
CPP="$cpp" CPPOPTS="$cppopts" \
TIME=/usr/bin/time

done
done

set +x

#	results summary

echo 
if test "`ls $tmpdir/*.output.tmp 2>/dev/null | cat`" ; then
	echo output.tmp indicates problems!
	ls -rlt $tmpdir/*.output.tmp
	fail=fail
fi

echo
if grep UCBFAIL $tmpdir/*.output* 1>/dev/null 2>&1 ; then
	echo UCBFAIL indicates problems!
	grep UCBFAIL $tmpdir/*.output*
	fail=fail
fi

echo
nlang=`echo $languages | wc -w`
nprec=`echo $precisions | wc -w `
ntests=`expr 14 '*' $nlang '*' $nprec`
npass=`grep -l UCBPASS $tmpdir/*.output | wc -l`
if test $npass -lt $ntests ; then
	echo only $npass out of $ntests show UCBPASS!
	fail=fail
fi

if test $fail ; then : ; else
	echo GOOD NEWS - NO PROBLEMS NOTICED!
fi

sh $SRC/ucbREADME/log.sh

