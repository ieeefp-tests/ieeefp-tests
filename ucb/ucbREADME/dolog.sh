set -e
set -x
LOGDIR=$HOME/ucb/ucbREADME
cd /tmp
/bin/time sh $LOGDIR/$1.sh 2>&1 | tee -i $LOGDIR/$1.log.tmp
rm -f $LOGDIR/$1.log
fold < $LOGDIR/$1.log.tmp > $LOGDIR/$1.log
rm -f $LOGDIR/$1.log.tmp
echo DONE $1
