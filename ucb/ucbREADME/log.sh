set +e
set +x
echo
date
uname -a
echo WHO `whoami` HOST `hostname` ID `hostid` WD `pwd`
echo
