set -e
set -x

### specification section

# sample ucbtest invocation for GCC on SunOS 5, SPARC or x86
# if gcc is not in $PATH, do e.g. 
#	setenv gcc /opt/gcc263/bin/gcc 
# prior to entry

# temporary directory where everything is to happen

tmpdir=/tmp/gcc

# source code directory

SRC=$HOME/ucb

# precision choices

precisions="DP SP"

# language choices

languages="c"

# compilation options

make="make"
cpp="cpp"
if test $gcc ; then
	cc=$gcc
else
	cc=gcc
fi
cppopts="-DNO_FUNCF -DNTESTS=1000000 -DNREGIONS=16 -DQUIET -DNO_FAIL -Dsunos_version=530"
opts="-O -ffloat-store"
ccopts=""
ccldopts="-lm"  

### execution section

sh $SRC/ucbREADME/log.sh


if test -d $tmpdir ; then : ; else mkdir $tmpdir ; fi

cd $tmpdir

for language in $languages ; do

makedir=ucb${language}test

$make -f $SRC/$makedir/Makefile SRC=$SRC clean

for precision in $precisions ; do

$make -f $SRC/$makedir/Makefile SRC=$SRC PRECISION=${precision} \
CC="$cc" CCOPTS="$opts $ccopts $cppopts" CCLDOPTS="$ccldopts" \
F77="$f77" F77OPTS="$opts $f77opts" F77LDOPTS="$f77ldopts" \
CPP="$cpp" CPPOPTS="$cppopts"

done
done

set +x

#	results summary

echo 
if test "`ls $tmpdir/*.output.tmp 2>/dev/null | cat`" ; then
	echo output.tmp indicates problems!
	ls -rlt $tmpdir/*.output.tmp
	fail=fail
fi

echo
if grep UCBFAIL $tmpdir/*.output* 1>/dev/null 2>&1 ; then
	echo UCBFAIL indicates problems!
	grep UCBFAIL $tmpdir/*.output*
	fail=fail
fi

echo
nlang=`echo $languages | wc -w`
nprec=`echo $precisions | wc -w `
ntests=`expr 14 '*' $nlang '*' $nprec`
npass=`grep -l UCBPASS $tmpdir/*.output | wc -l`
if test $npass -lt $ntests ; then
	echo only $npass out of $ntests show UCBPASS!
	fail=fail
fi

if test $fail ; then : ; else
	echo GOOD NEWS - NO PROBLEMS NOTICED!
fi

sh $SRC/ucbREADME/log.sh


