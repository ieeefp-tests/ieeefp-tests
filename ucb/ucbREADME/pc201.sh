set -e
set -x

### specification section

# sample ucbtest invocation for SunOS 5.x for x86 with Sun PC2.0.1 compilers


# temporary directory where everything is to happen

tmpdir=/tmp/pc201

# source code directory

SRC=$HOME/ucb

# precision choices

precisions="DP SP QP"

# language choices

languages="c f"

# compilation options

make="make"
cpp="cpp"
cc=/opt/SUNWspro/bin/cc
f77=/opt/SUNWspro/bin/f77
cppopts="-DSUN_IEEE -DSUN_MATH -Dsunos_version=540 -Dsunpro_version=201 -DNTESTS=1000000 -DNREGIONS=16 -DQUIET -DNO_FAIL"
opts="-O /tmp/sqrt.il"
ccopts="-xlibmieee -xpentium -fsingle"
ccldopts="-lm"  
f77opts="-pentium"
f77ldopts=""

cat <<EOF > /tmp/sqrt.il
        .inline sqrt,0
        fldl    (%esp)
        fsqrt
        .end 
        .inline sqrtf,0
        flds    (%esp)
        fsqrt
        .end 
        .inline sqrtl,0
        fldt     (%esp)
        fsqrt
        .end 
EOF

### execution section

sh $SRC/ucbREADME/log.sh


if test -d $tmpdir ; then : ; else mkdir $tmpdir ; fi

cd $tmpdir

ncombo=0

for language in $languages ; do

makedir=ucb${language}test

$make -f $SRC/$makedir/Makefile SRC=$SRC clean

for precision in $precisions ; do

if test $language = f && test $precision = QP ; then : ; else

ncombo=`expr $ncombo + 1`

$make -f $SRC/$makedir/Makefile SRC=$SRC PRECISION=${precision} \
CC="$cc" CCOPTS="$opts $ccopts $cppopts" CCLDOPTS="$ccldopts" \
F77="$f77" F77OPTS="$opts $f77opts" F77LDOPTS="$f77ldopts" \
CPP="$cpp" CPPOPTS="$cppopts"

fi

done
done

set +x

#	results summary

echo 
if test "`ls $tmpdir/*.output.tmp 2>/dev/null | cat`" ; then
	echo output.tmp indicates problems!
	ls -rlt $tmpdir/*.output.tmp
	fail=fail
fi

echo
if grep UCBFAIL $tmpdir/*.output* 1>/dev/null 2>&1 ; then
	echo UCBFAIL indicates problems!
	grep UCBFAIL $tmpdir/*.output*
	fail=fail
fi

echo
ntests=`expr 14 '*' $ncombo`
npass=`grep -l UCBPASS $tmpdir/*.output | wc -l`
if test $npass -lt $ntests ; then
	echo only $npass out of $ntests show UCBPASS!
	fail=fail
fi

if test $fail ; then : ; else
	echo GOOD NEWS - NO PROBLEMS NOTICED!
fi

sh $SRC/ucbREADME/log.sh


