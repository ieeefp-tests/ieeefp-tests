set -e
set -x

### specification section

# sample ucbtest invocation for SunOS 5.x for SPARC with Apogee 3.0 compilers

# temporary directory where everything is to happen

tmpdir=/tmp/ap30

# source code directory

SRC=$HOME/ucb

# precision choices

precisions="DP SP"

# language choices

languages="c f"

# compilation options

make="make"
cpp=/usr/ccs/lib/cpp
cc=/set/lang/valid4/apogee30/AC3.0.sol2/apcc
f77=/set/lang/valid4/apogee30/AC3.0.sol2/apf77
cppopts="-Dsunos_version=530 -DNTESTS=1000000 -DNREGIONS=16 -DQUIET -DNO_FUNCF"
opts="-O -cg92"
ccopts="-Dlint"
ccldopts="-lm"  
f77opts=""
f77ldopts=""

### execution section
 
sh $SRC/ucbREADME/log.sh
 
if test -d $tmpdir ; then : ; else mkdir $tmpdir ; fi

cd $tmpdir

for language in $languages ; do

makedir=ucb${language}test

$make -f $SRC/$makedir/Makefile SRC=$SRC clean

for precision in $precisions ; do

$make -f $SRC/$makedir/Makefile SRC=$SRC PRECISION=${precision} \
CC="$cc" CCOPTS="$opts $ccopts $cppopts" CCLDOPTS="$ccldopts" \
F77="$f77" F77OPTS="$opts $f77opts" F77LDOPTS="$f77ldopts" \
CPP="$cpp" CPPOPTS="$cppopts"

done
done

set +x

#	results summary

echo 
if test "`ls $tmpdir/*.output.tmp 2>/dev/null | cat`" ; then
	echo output.tmp indicates problems!
	ls -rlt $tmpdir/*.output.tmp
	fail=fail
fi

echo
if grep UCBFAIL $tmpdir/*.output* 1>/dev/null 2>&1 ; then
	echo UCBFAIL indicates problems!
	grep UCBFAIL $tmpdir/*.output*
	fail=fail
fi

echo
nlang=`echo $languages | wc -w`
nprec=`echo $precisions | wc -w `
ntests=`expr 14 '*' $nlang '*' $nprec`
npass=`grep -l UCBPASS $tmpdir/*.output | wc -l`
if test $npass -lt $ntests ; then
	echo only $npass out of $ntests show UCBPASS!
	fail=fail
fi

if test $fail ; then : ; else
	echo GOOD NEWS - NO PROBLEMS NOTICED!
fi

sh $SRC/ucbREADME/log.sh

