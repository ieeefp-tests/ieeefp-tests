set -e
set -x

### specification section

# sample ucbtest invocation for generic OS, cc, f77

# temporary directory where everything is to happen

tmpdir=/tmp/generic	# define as you like

# source code directory

SRC=$HOME/ucb		# where the source code is

# precision choices

precisions="DP SP"	# add QP if long double and real*16 available

# language choices

languages="c f"		# delete f if Fortran not available

# compilation options

make="make"	# try make -k or gmake if this doesn't work
time="time"	# try /bin/time or /usr/bin/time if this doesn't work
cpp="cpp"	# try /lib/cpp or /usr/lib/cpp or gcc -E if this doesn't work
cc="cc"		# try /bin/cc or /usr/bin/cc or gcc if this doesn't work
f77="f77"	# try /usr/bin/f77 if this doesn't work
cppopts="-DNTESTS=1000 -DNREGIONS=16 -DQUIET"
		# change 1000 to 1000000 after script debugging done
opts="-O"
ccopts=""
ccldopts="-lm"  
f77opts=""
f77ldopts=""

### execution section

sh $SRC/ucbREADME/log.sh

if test -d $tmpdir ; then : ; else mkdir $tmpdir ; fi

cd $tmpdir

ncombo=0

for language in $languages ; do

makedir=ucb${language}test

#$make -f $SRC/$makedir/Makefile SRC=$SRC clean
		# uncomment make clean after script debugging done

for precision in $precisions ; do

ncombo=`expr $ncombo + 1`

$make -f $SRC/$makedir/Makefile SRC=$SRC PRECISION=${precision} \
CC="$cc" CCOPTS="$opts $ccopts $cppopts" CCLDOPTS="$ccldopts" \
F77="$f77" F77OPTS="$opts $f77opts" F77LDOPTS="$f77ldopts" \
CPP="$cpp" CPPOPTS="$cppopts" \
TIME="$time"

done
done

set +x

#	results summary

echo 
if test "`ls $tmpdir/*.output.tmp 2>/dev/null | cat`" ; then
	echo output.tmp indicates problems!
	ls -rlt $tmpdir/*.output.tmp
	fail=fail
fi

echo
if grep UCBFAIL $tmpdir/*.output* 1>/dev/null 2>&1 ; then
	echo UCBFAIL indicates problems!
	grep UCBFAIL $tmpdir/*.output*
	fail=fail
fi

echo
ntests=`expr 14 '*' $ncombo`
npass=`grep -l UCBPASS $tmpdir/*.output | wc -l`
if test $npass -lt $ntests ; then
	echo only $npass out of $ntests show UCBPASS!
	fail=fail
fi

if test $fail ; then : ; else
	echo GOOD NEWS - NO PROBLEMS NOTICED!
fi

sh $SRC/ucbREADME/log.sh

