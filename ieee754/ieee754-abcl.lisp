(in-package "IEEE754-INTERNALS")

(defvar ieeefp-tests:*float-types* (list :single :double))

(defvar ieeefp-tests:*rounding-modes* (list :nearest))

(defun make-single-float (x)
  (sys:make-single-float x))

(defun make-double-float (x)
  (sys:make-double-float x))

(defun single-float-bits (x)
  (declare (type single-float x))
  (ldb (byte 32 0) (sys:single-float-bits x)))

(defun double-float-bits (x)
  (declare (type double-float x))
  (ldb (byte 64 0)
       (logior (ash (sys:double-float-high-bits x) 32)
	       (sys:double-float-low-bits x))))

(defun set-floating-point-modes (&rest args &key traps accrued-exceptions
				 current-exceptions rounding-mode precision)
  (declare (ignore args traps accrued-exceptions current-exceptions
                   rounding-mode precision))
  ;; Not supported.
  )

(macrolet
  ((def (x &body body)
     `(defun ,x (x y)
        (declare (type float x y))
        ,@body)))
  (def = (cl:= x y))
  (def ?<> (not (= x y)))
  (def > (cl:> x y))
  (def >= (cl:>= x y))
  (def < (cl:< x y))
  (def <= (cl:<= x y))
  (def ? (or (sys:float-nan-p x) (sys:float-nan-p y)))
  (def <> (or (< x y) (> x y)))
  (def <=> (or (< x y) (= x y) (> x y)))
  (def ?> (or (? x y) (> x y)))
  (def ?>= (or (? x y) (>= x y)))
  (def ?< (or (? x y) (< x y)))
  (def ?<= (or (? x y) (<= x y)))
  (def ?= (or (? x y) (= x y))))
