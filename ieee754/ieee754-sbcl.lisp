(in-package "IEEE754-INTERNALS")

(defvar ieeefp-tests:*test-exceptions* t)

(defun make-single-float (x)
  (declare (type (or (unsigned-byte 32) (signed-byte 32)) x))
  (typecase x
    ((signed-byte 32) (sb-kernel:make-single-float x))
    (t (sb-kernel:make-single-float (dpb x (byte 32 0) -1)))))

(defun make-double-float (x)
  (declare (type (or (unsigned-byte 64) (signed-byte 64)) x))
  (typecase x
    ((signed-byte 64) (sb-kernel:make-double-float
		       (ldb (byte 32 32) x) (ldb (byte 32 0) x)))
    (t (sb-kernel:make-double-float
	(dpb (ldb (byte 32 32) x) (byte 32 0) -1) (ldb (byte 32 0) x)))))

(defun single-float-bits (x)
  (declare (type single-float x))
  (ldb (byte 32 0) (sb-kernel:single-float-bits x)))

(defun double-float-bits (x)
  (declare (type double-float x))
  (ldb (byte 64 0)
       (logior (ash (sb-kernel:double-float-high-bits x) 32)
	       (sb-kernel:double-float-low-bits x))))

(defun set-floating-point-modes (&rest args &key traps accrued-exceptions
				 current-exceptions rounding-mode precision)
  (declare (ignore traps accrued-exceptions current-exceptions rounding-mode
		   precision))
  (apply #'sb-int:set-floating-point-modes args))

(defun get-floating-point-modes ()
  (sb-int:get-floating-point-modes))

(macrolet
    ((def (x &body body)
	 `(defun ,x (x y)
	   (declare (type float x y))
	   ,@body)))
  (def = (cl:= x y))
  (def ?<> (not (= x y)))
  (def > (cl:> x y))
  (def >= (cl:>= x y))
  (def < (cl:< x y))
  (def <= (cl:<= x y))
  (def ? (or (sb-ext:float-nan-p x) (sb-ext:float-nan-p y)))
  (def <> (or (< x y) (> x y)))
  (def <=> (or (< x y) (= x y) (> x y)))
  (def ?> (or (? x y) (> x y)))
  (def ?>= (or (? x y) (>= x y)))
  (def ?< (or (? x y) (< x y)))
  (def ?<= (or (? x y) (<= x y)))
  (def ?= (or (? x y) (= x y))))
