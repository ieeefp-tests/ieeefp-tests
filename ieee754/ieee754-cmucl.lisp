(in-package "IEEE754-INTERNALS")

(defun make-single-float (x)
  (declare (type (or (unsigned-byte 32) (signed-byte 32)) x))
  (typecase x
    ((signed-byte 32) (kernel:make-single-float x))
    (t (kernel:make-single-float (dpb x (byte 32 0) -1)))))

(defun make-double-float (x)
  (declare (type (or (unsigned-byte 64) (signed-byte 64)) x))
  (typecase x
    ((signed-byte 64) (kernel:make-double-float
		       (ldb (byte 32 32) x) (ldb (byte 32 0) x)))
    (t (kernel:make-double-float
	(dpb (ldb (byte 32 32) x) (byte 32 0) -1) (ldb (byte 32 0) x)))))

(defun single-float-bits (x)
  (declare (type single-float x))
  (ldb (byte 32 0) (kernel:single-float-bits x)))

(defun double-float-bits (x)
  (declare (type double-float x))
  (ldb (byte 64 0)
       (logior (ash (kernel:double-float-high-bits x) 32)
	       (kernel:double-float-low-bits x))))

(defun set-floating-point-modes (&rest args &key traps accrued-exceptions
				 current-exceptions rounding-mode precision)
  (declare (ignore traps accrued-exceptions current-exceptions rounding-mode
		   precision))
  (apply #'ext:set-floating-point-modes args))

(defun get-floating-point-modes ()
  (ext:get-floating-point-modes))


;;; IEEE754 recommended functions

(defun copysign (x y)
  "Copy the sign of Y to X and return the result"
  (float-sign y x))

(defun scalb (x n)
  "Compute x*2^n, without roundoff"
  (scale-float x n))

(defun finitep (x)
  "Returns non-NIL if X is a finite value"
  ;; What are we supposed to do when X is a NaN?
  (not (ext:float-infinity-p x)))

(defun nanp (x)
  "Returns non-NIL if X is a NaN"
  (ext:float-nan-p x))

(defun logb (x)
  "Return the unbiased exponent of X, except for the following:

   NaN             NaN
   +infinity       +infinity
   0               -infinity, signaling division by zero

   Also, 0 < scalb(x, -logb(x)) < 2, when x is positive and finite; it
   is less than 1 only when x is denormalized."
  (cond ((nanp x)
	 x)
	((not (finitep x))
	 x)
	((zerop x)
	 ;; Need to signal division by zero, if enabled; otherwise,
	 ;; return -infinity.
	 (/ -1 x))
	(t
	 ;; Finite x.  DECODE-FLOAT is basically what we want, except
	 ;; it's one too big.  And it's also too small for
	 ;; denormalized numbers.  We need to clip at the least
	 ;; exponent for normalized floats.
	 (multiple-value-bind (f e s)
	     (decode-float x)
	   (declare (ignore f s))
	   (max (1- e)
		(etypecase x
		  (single-float -126)
		  (double-float -1022)))))))

(defun nextafter (x y)
  "The next float after X in the direction of Y, with the
following exceptions:

  X = Y                 returns X, unchanged
  X or Y is quiet NaN   returns the NaN

  Overflow is signaled if nextafter would overflow; underflow is
signaled if nextafter would underflow.  In both cases, inexact
is signaled."

  ;; What are we supposed to do if x or y is a signaling NaN?
  (cond ((= x y)
	 x)
	((nanp x)
	 x)
	((nanp y)
	 y)
	(t
	 (multiple-value-bind (f e s)
	     (integer-decode-float x)
	   (if (>= y x)
	       (incf f)
	       (decf f))
	   (* s (scale-float (float f x) e))))))

(macrolet
    ((def (x &body body)
	 `(defun ,x (x y)
	   (declare (type float x y))
	   ,@body)))
  (def = (cl:= x y))
  (def ?<> (not (= x y)))
  (def > (cl:> x y))
  (def >= (cl:>= x y))
  (def < (cl:< x y))
  (def <= (cl:<= x y))
  (def ? (or (ext:float-nan-p x) (ext:float-nan-p y)))
  (def <> (or (< x y) (> x y)))
  (def <=> (or (< x y) (= x y) (> x y)))
  (def ?> (or (? x y) (> x y)))
  (def ?>= (or (? x y) (>= x y)))
  (def ?< (or (? x y) (< x y)))
  (def ?<= (or (? x y) (<= x y)))
  (def ?= (or (? x y) (= x y))))
