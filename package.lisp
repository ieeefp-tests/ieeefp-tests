(defpackage "IEEE754"
  (:use "CL")
  (:shadow "=" ">" "<" ">=" "<=")
  (:export "MAKE-SINGLE-FLOAT" "MAKE-DOUBLE-FLOAT"
	   "SINGLE-FLOAT-BITS" "DOUBLE-FLOAT-BITS"
	   "GET-FLOATING-POINT-MODES" "SET-FLOATING-POINT-MODES"

	   ;; IEEE754 Table 4
	   "=" "?<>" ">" ">=" "<" "<=" "?"
	   "<>"
	   "<=>" "?>" "?>=" "?<" "?<=" "?="
	   ))

(defpackage "IEEE754-INTERNALS"
  (:shadowing-import-from "IEEE754" "=" ">" "<" ">=" "<=")
  (:use "CL" "IEEE754"))

(defpackage "IEEEFP-TESTS"
  (:shadowing-import-from "CL" "=" ">" "<" ">=" "<=")
  (:use "CL" "IEEE754" "SPLIT-SEQUENCE")
  (:export "*FLOAT-TYPES*" "*ROUNDING-MODES*" "*TEST-EXCEPTIONS*"))
