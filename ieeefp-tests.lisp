(in-package "IEEEFP-TESTS")

(defvar *float-types*
  (list :single :double))

(defvar *rounding-modes*
  (list :nearest :zero :positive-infinity :negative-infinity))

(defvar *test-exceptions* nil)

;; So we can run log10 tests
(defun log10 (x)
  (log x (float 10 x)))

;; So we can run hypot tests and make it test Lisp's ABS function on
;; complex values.
(defun hypot (x y)
  (abs (complex x y)))

(defclass test-vector ()
  ((line :initarg :line :accessor line)
   (fun-name :initarg :fun-name :accessor fun-name)
   (lisp-fun-name :accessor lisp-fun-name)
   (fun-arity :accessor fun-arity)
   (precision :initarg :precision :accessor precision)
   (rounding-mode :initarg :rounding-mode :accessor rounding-mode)
   (test :initarg :test :accessor test)
   (exceptions :initarg :exceptions :accessor exceptions)
   (fun-args :accessor fun-args)
   (expected-answer :accessor expected-answer)))

(defmethod initialize-instance :after ((vector test-vector)
				       &key args-and-expected-answer)
  (ecase (fun-name vector)
    ((log exp sin cos tan sinh cosh tanh asin acos atan sqrt fabs floor ceil log10 trunc)
     (assert (= (length args-and-expected-answer) 2))
     (setf (fun-arity vector) 1))
    ((add sub mul div pow atan2 hypot)
     (assert (= (length args-and-expected-answer) 3))
     (setf (fun-arity vector) 2))
    ((ieee754:=)
     (assert (= (length args-and-expected-answer) 2))
     (setf (fun-arity vector) 2)))
  (case (test vector)
    ((:yea :nay)
     (setf (fun-args vector) args-and-expected-answer
	   (expected-answer vector) nil))
    (t 
     (setf (fun-args vector) (butlast args-and-expected-answer))
     (setf (expected-answer vector) (car (last args-and-expected-answer)))))
  (setf (exceptions vector) (sort (exceptions vector) #'string<))
  ;; FUN-NAME is currently partially overloaded with 2 meanings: 1. It
  ;; is the name of the test. 2. It is the name of the Lisp function
  ;; to use.
  (setf (fun-name vector)
	(case (fun-name vector)
	  ((fabs) 'abs)
	  ((floor) 'ffloor)
	  ((ceil) 'fceiling)
	  ((trunc) 'ftruncate)
	  ((add) '+)
	  ((sub) '-)
	  ((mul) '*)
	  ((div) '/)
	  ((pow) 'expt)
	  (t (fun-name vector))))
  ;; Figure out the Lisp function we need to call to test.  Mostly
  ;; redundant, except for the atan2 tests.  Can't use a fun-name of
  ;; atan2 because there's no atan2 Lisp function.  And can't change
  ;; fun-name from atan2 to atan because then all the test names will
  ;; be atan, overwriting the tests for the single arg atan.
  (setf (lisp-fun-name vector)
	(case (fun-name vector)
	  ((atan2) 'atan)
	  (t (fun-name vector)))))

(defparameter *special-values* nil)

(macrolet ((def (name bits)
	       `(progn
		 (defconstant ,name ,bits)
		 (push (cons ,bits ',name) *special-values*))))
  (def +quiet-double-float-nan-mask+ #X7FF8000000000000)
  (def +quiet-single-float-nan-mask+ #X7FC00000)
  ;; NaN is supposed to ignore the sign, but the tests use both
  ;; positive and negative NaNs, so define them here.
  (def +trapping-positive-double-float-nan+ #x7FF0000000000001)
  (def +trapping-negative-double-float-nan+ #xFFF0000000000001)
  (def +single-float-positive-infinity+ #x7F800000)
  (def +single-float-negative-infinity+ #xFF800000)
  (def +double-float-positive-infinity+ #x7FF0000000000000)
  (def +double-float-negative-infinity+ #xFFF0000000000000)
  (def +most-positive-double-float+ #X7FEFFFFFFFFFFFFF)
  (def +most-positive-single-float+ #x7F7FFFFF)
  (def +least-positive-double-float+ 1)
  (def +least-positive-single-float+ 1)
  (def +least-negative-double-float+ #x8000000000000001)
  (def +least-negative-single-float+ #x80000001)
  (def +least-positive-normalized-double-float+ #x10000000000000)
  (def +least-positive-normalized-single-float+ #x800000)
  (def +least-negative-normalized-double-float+ #x8010000000000000)
  (def +least-negative-normalized-single-float+ #x80800000)
  (def +1d0+ #x3FF0000000000000)
  (def +1f0+ #x3F800000)
  (def +negative-0d0+ #x8000000000000000)
  (def +negative-0f0+ #x80000000))

(defun maybe-replace-special-value (x)
  ;; Look at x and replace it with named constants, if possible.
  (let ((value (assoc x *special-values* :test #'=)))
    (if value
	(cdr value)
	x)))

(defvar *ieeefp-test-root*
  (if (find-package "ASDF")
      (funcall (intern "SYSTEM-RELATIVE-PATHNAME" (find-package "ASDF"))
	       :ieeefp-tests ".")
      *load-truename*))

(defun vector-pathname (function-name file-name)
  (let ((directory (case function-name
		     ((trunc ieee754:=) '(:relative "ucb-patches" "ucblib"))
		     (t '(:relative "ucb" "ucblib")))))
    (merge-pathnames
     (make-pathname :directory directory
		    :name file-name
		    :type "input")
     *ieeefp-test-root*)))

(defun process-vector-file (function-name precision)
  (let* ((file-name (format nil "~(~A~)~(~A~)" function-name
			    (char (symbol-name precision) 0)))
	 (length (length file-name))
	 (input-file (vector-pathname function-name file-name))
	 tests)

  (with-open-file (in input-file)
    (do ((line (read-line in nil nil) (read-line in nil nil)))
	((null line) (nreverse tests))
      (when (eql (mismatch line file-name) length)
	(let ((split (split-sequence:split-sequence #\Space line
						    :remove-empty-subseqs t)))
	  (let (rounding-mode test exceptions args-and-expected-answer)
	    (setf rounding-mode (ecase (char (second split) 0)
				  (#\n :nearest)
				  (#\z :zero)
				  (#\p :positive-infinity)
				  (#\m :negative-infinity)))
	    (setf test (intern (string-upcase (third split))
			       "KEYWORD"))
	    (dotimes (i (length (fourth split)))
	      (ecase (char (fourth split) i)
		(#\o (push :overflow exceptions))
		(#\u (push :underflow exceptions))
		(#\d (push :divide-by-zero exceptions))
		(#\x (push :inexact exceptions))
		(#\v (push :invalid exceptions))
		(#\- (setf exceptions nil))
		;; FIXME: what does #\? mean?  Not really a problem
		;; until we start testing exceptions.  I think it
		;; means that the following exception may or may not
		;; be present.
		(#\? (push 'maybe exceptions))))
	    (setf args-and-expected-answer
		  (loop
		   for x on (nthcdr 4 split) by (ecase precision
						  (:single #'cdr)
						  (:double #'cddr)
						  (:quad #'cddddr))
		   collect
		   (maybe-replace-special-value
		    (ecase precision
		     (:single (parse-integer (car x) :radix 16))
		     (:double (+ (ash (parse-integer (car x) :radix 16) 32)
				 (parse-integer (cadr x) :radix 16)))
		     (:quad (+ (ash (parse-integer (car x) :radix 16) 96)
			       (ash (parse-integer (cadr x) :radix 16) 64)
			       (ash (parse-integer (caddr x) :radix 16) 32)
			       (parse-integer (cadddr x) :radix 16)))))))
	    (push (make-instance 'test-vector
				 :line line
				 :fun-name function-name
				 :precision precision
				 :rounding-mode rounding-mode
				 :test test
				 :exceptions (nreverse exceptions)
				 :args-and-expected-answer args-and-expected-answer)
		  tests))))))))

(defvar *test-counter* 0)

(set-pprint-dispatch '(cons (member rt:deftest))
		     (lambda (stream list)
		       (funcall
			(formatter "~:<~^~W~^ ~@_~:I~W~^ ~3I~@:_~W~^ ~1I~@:_~W~:>")
			stream list)))

(defun make-result-test-form (vector)
  (case (test vector)
    ((:yea) `(not (not result)))
    ((:nay) `(not result))
    (t `(if (complexp result)
	 t
	 ,(ecase (precision vector)
		 (:single (make-single-result-test-form vector))
		 (:double (make-double-result-test-form vector)))))))

(defun make-double-result-test-form (vector)
  `(let ((result-bits (double-float-bits result)))
    ,(ecase (test vector)
       (:eq '(maybe-replace-special-value result-bits))
       (:uo `(= (logand +quiet-double-float-nan-mask+ result-bits)
		+quiet-double-float-nan-mask+))
       ((:vn :nb)
       `(<= (abs (- ,(expected-answer vector) result-bits))
	    ,(if (eq (test vector) :vn) 3 10)))
       ((:gt :ge :le :lt)
	`(let ((k (logxor ,(expected-answer vector) result-bits)))
	  ;; when they have different signs...
	  (if (logbitp 63 k)
	       ;; ... the result should have the appropriate sign
	       ,(case (test vector)
		  ((:ge :gt) '(not (logbitp 63 result-bits)))
		  ((:le :lt) '(logbitp 63 result-bits)))
	      (if (logbitp 63 result-bits)
		  (,(case (test vector)
		      (:ge '<=)
		      (:gt '<)
		      (:lt '>)
		      (:le '>=))
		    (logand result-bits #x7fffffffffffffff)
		    (logand ,(expected-answer vector) #x7fffffffffffffff))
		  (,(case (test vector)
		      (:ge '>=)
		      (:gt '>)
		      (:lt '<)
		      (:le '<=))
		    (logand result-bits #x7fffffffffffffff)
		    (logand ,(expected-answer vector) #x7fffffffffffffff)))))))))

(defun make-single-result-test-form (vector)
  `(let ((result-bits (single-float-bits result)))
    ,(ecase (test vector)
       (:eq '(maybe-replace-special-value result-bits))
       (:uo `(= (logand result-bits +quiet-single-float-nan-mask+)
		+quiet-single-float-nan-mask+))
       ((:vn :nb)
	`(<= (abs (- ,(expected-answer vector) result-bits))
	     ,(if (eq (test vector) :vn) 3 10)))
       ((:gt :ge :le :lt)
	`(let ((k (logxor ,(expected-answer vector) result-bits)))
	  ;; when they have different signs...
	  (if (logbitp 31 k)
	       ;; ... the result should have the appropriate sign
	       ,(case (test vector)
		  ((:ge :gt) '(not (logbitp 31 result-bits)))
		  ((:le :lt) '(logbitp 31 result-bits)))
	      (if (logbitp 31 result-bits)
		  (,(case (test vector)
		      (:ge '<=)
		      (:gt '<)
		      (:lt '>)
		      (:le '>=))
		    (logand result-bits #x7fffffff)
		    (logand ,(expected-answer vector) #x7fffffff))
		  (,(case (test vector)
		      (:ge '>=)
		      (:gt '>)
		      (:lt '<)
		      (:le '<=))
		    (logand result-bits #x7fffffff)
		    (logand ,(expected-answer vector) #x7fffffff)))))))))

(defun make-expected-result-form (vector)
  (if (eq (test vector) :eq)
      (expected-answer vector)
      t))

(defun make-test-name (vector type)
  (intern (format nil "~@:(~A~)-~@:(~A~)-~@:(~A~).~D"
		  (precision vector)
		  (fun-name vector)
		  type
		  *test-counter*)))

(defun set-up-fpcw-state (rounding-mode)
  (set-floating-point-modes
   :traps nil :accrued-exceptions nil
   :current-exceptions nil :rounding-mode rounding-mode))

(defun clear-fpcw-exceptions ()
  (set-floating-point-modes
   :accrued-exceptions nil :current-exceptions nil))

(defun get-accrued-exceptions ()
  (getf (get-floating-point-modes) :accrued-exceptions))

(defun emit-double-value-tests (vector stream)
  (when (member (rounding-mode vector) *rounding-modes*)
    (pprint
     `(rt:deftest ,(make-test-name vector 'value)
                  (progn
		    ,(line vector)
                    (set-up-fpcw-state ,(rounding-mode vector))
                    (let ((result
                           (,(lisp-fun-name vector)
			     ,@(mapcar (lambda (x)
					 `(make-double-float ,x))
				       (fun-args vector)))))
                      ,(make-result-test-form vector)))
                  ,(make-expected-result-form vector))
     stream)))

(defun emit-single-value-tests (vector stream)
  (when (member (rounding-mode vector) *rounding-modes*)
    (pprint
     `(rt:deftest ,(make-test-name vector 'value)
                  (progn
		    ,(line vector)
                    (set-up-fpcw-state ,(rounding-mode vector))
                    (let ((result
                           (,(lisp-fun-name vector)
			     ,@(mapcar (lambda (x)
					 `(make-single-float ,x))
				       (fun-args vector)))))
                      ,(make-result-test-form vector)))
                  ,(make-expected-result-form vector))
     stream)))

(defun emit-double-exceptions-tests (vector stream)
  (when (and (member (rounding-mode vector) *rounding-modes*)
	     *test-exceptions*)
    (pprint
     (let* ((maybes (loop for x on (exceptions vector)
			  if (eq (car x) 'maybe)
			    collect (cadr x)))
	    (definites (sort
			(set-difference (remove 'maybe (exceptions vector))
					maybes)
			#'string<)))
       `(rt:deftest ,(make-test-name vector 'exceptions)
                    (block nil
		      ,(line vector)
	  	      (set-up-fpcw-state ,(rounding-mode vector))
		      (let ((answer
			     (,(lisp-fun-name vector)
			       ,@(mapcar (lambda (x)
					   `(make-double-float ,x))
					 (fun-args vector))))
			    (result
			     (sort
			      (remove-if-not
			       (lambda (x)
				 (member x
					 '(:invalid :underflow :overflow
					   :divide-by-zero :inexact)))
			       (set-difference (get-accrued-exceptions)
					       ',maybes))
			      #'string<)))
			(if (complexp answer)
			    ',definites
			    result)))
	            ,definites))
     stream)))

(defun emit-single-exceptions-tests (vector stream)
  (when (and (member (rounding-mode vector) *rounding-modes*)
	     *test-exceptions*)
    (pprint
     (let* ((maybes (loop for x on (exceptions vector)
			  if (eq (car x) 'maybe)
			    collect (cadr x)))
	    (definites (sort
			(set-difference (remove 'maybe (exceptions vector))
					maybes)
			#'string<)))
       `(rt:deftest ,(make-test-name vector 'exceptions)
                    (block nil
		      ,(line vector)
	  	      (set-up-fpcw-state ,(rounding-mode vector))
		      (let ((answer
			     (,(lisp-fun-name vector)
			       ,@(mapcar (lambda (x)
					   `(make-single-float ,x))
					 (fun-args vector))))
			    (result
			     (sort
			      (remove-if-not
			       (lambda (x)
				 (member x
					 '(:invalid :underflow :overflow
					   :divide-by-zero :inexact)))
			       (set-difference (get-accrued-exceptions)
					       ',maybes))
			      #'string<)))
			(if (complexp answer)
			    ',definites
			    result)))
	            ,definites))
     stream)))

(defmethod emit-tests-from-one-vector ((vector test-vector) stream)
  (let ((*print-case* :downcase))
    (with-standard-io-syntax
      (ecase (precision vector)
	(:single
	 (emit-single-value-tests vector stream)
	 (emit-single-exceptions-tests vector stream))
	(:double
	 (emit-double-value-tests vector stream)
	 (emit-double-exceptions-tests vector stream))
	))))

(defun make-one-test-file (fun-name precision)
  (with-open-file (s (format nil "/tmp/~(~A~)-~(~A~).lisp"
			     fun-name precision)
		     :direction :output :if-exists :supersede)
    (format *trace-output* "; creating ~S~%" (namestring s))
    (format s "(in-package \"IEEEFP-TESTS\")~2%")
    (setf *test-counter* 0)
    (dolist (v (process-vector-file fun-name precision))
      (incf *test-counter*)
      (emit-tests-from-one-vector v s))
    (pathname s)))

(defparameter *test-files* nil)

(dolist (fun '(log sin cos tan sinh cosh tanh asin acos
	       atan sqrt fabs floor ceil add sub mul div pow
	       atan2 log10 hypot trunc ieee754:=))
  (dolist (type *float-types*)
    (pushnew (make-one-test-file fun type) *test-files* :test #'equal)))

(defvar *revision* "$Revision: 1.11 $")

(defun format-date (stream arg colonp atp)
  (declare (ignore colonp atp))
  (multiple-value-bind (s m h da mo yr dow dst tz)
      (decode-universal-time arg)
    (declare (ignore dow))
    (let* ((tz (+ (if dst 1 0) tz)))
      (multiple-value-bind (tzh tzm)
	  (truncate tz)
	(let ((tzmm (truncate tzm 1/60)))
	  (format stream "~2,'0D-~2,'0D-~2,'0DT~2,'0D:~2,'0D:~2,'0D~:[+~;-~]~2,'0D:~2,'0D"
		  yr mo da h m s (minusp tzh) tzh tzmm))))))

(defun report (&optional (stream *standard-output*))
  (let ((*standard-output* stream))
    (format t ";;;; IEEEFP-TESTS results for ~A ~A~%;;;~%"
	    (lisp-implementation-type) (lisp-implementation-version))
    (format t ";;; Machine: ~A ~A (~A)~%"
	    (machine-type) (machine-version) (machine-instance))
    ;; KLUDGE: no way of querying for libm version...
    (format t ";;; Software: ~A ~A~%"
	    (software-type) (software-version))
    (format t ";;; Report generated: ~/ieeefp-tests::format-date/~%"
	    (get-universal-time))
    (let ((revision (subseq *revision* 11 (1- (length *revision*)))))
      (format t ";;; using ieeefp-tests.lisp version ~A~%" revision))
    (let ((failures (rt:pending-tests)))
      (format t ";;;~%;;; ~D out of ~D tests failed.~%;;; Failures:~%(~%"
	      (length failures)
	      ;; KLUDGE: unexported symbol
	      (length (cdr rt::*entries*)))
      (with-standard-io-syntax
	(format t "~{~A~%~})~%" (sort (copy-list failures) #'string<))))))
